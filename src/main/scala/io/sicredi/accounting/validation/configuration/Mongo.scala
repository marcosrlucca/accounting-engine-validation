package io.sicredi.accounting.validation.configuration

import reactivemongo.api.MongoConnectionOptions
import reactivemongo.api.collections.bson.BSONCollection
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class MongoProperties(servers: Seq[String],
                           authDBName: String,
                           password: String,
                           dataDBName: String,
                           closureDateCollection: String,
                           systemParameterCollection: String,
                           movementEventParameterCollection: String,
                           companyParameterCollection: String,
                           balanceEventParameterCollection: String)

case class Mongo(mongoProperties: MongoProperties) {

  private lazy val driver = new reactivemongo.api.MongoDriver

  private lazy val servers: Seq[String] = mongoProperties.servers
  private lazy val authDBName: String = mongoProperties.authDBName
  private lazy val password: String = mongoProperties.password
  private lazy val credentials = MongoConnectionOptions.Credential(user = DB_USERNAME, password = Some(password))
  private lazy val conOpts = MongoConnectionOptions(credentials = Map((authDBName, credentials)))

  private lazy val connection = driver.connection(servers, options = conOpts)

  private lazy val dataBaseName = mongoProperties.dataDBName

  private lazy val closureDateCollection = mongoProperties.closureDateCollection
  private lazy val systemParameterCollection = mongoProperties.systemParameterCollection
  private lazy val movementEventParameterCollection = mongoProperties.movementEventParameterCollection
  private lazy val balanceEventParameterCollection = mongoProperties.balanceEventParameterCollection

  def getCompanyCollection: Future[BSONCollection] =
    connection.database(dataBaseName)
      .map(_.collection(mongoProperties.companyParameterCollection))

  def getMovementEventParameterCollection: Future[BSONCollection] =
    connection.database(dataBaseName)
      .map(_.collection(movementEventParameterCollection))

  def getBalanceEventParameterCollection: Future[BSONCollection] =
    connection.database(dataBaseName)
      .map(_.collection(balanceEventParameterCollection))

  def getSystemParameterCollection: Future[BSONCollection] =
    connection.database(dataBaseName)
      .map(_.collection(systemParameterCollection))

  def getClosureDateCollection: Future[BSONCollection] =
    connection.database(dataBaseName)
      .map(_.collection(closureDateCollection))

  def getCollectionByCollectionName(name: String): Future[BSONCollection] =
    connection.database(dataBaseName)
      .map(_.collection(name))

}

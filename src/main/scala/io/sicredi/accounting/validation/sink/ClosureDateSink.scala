package io.sicredi.accounting.validation.sink

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.client.ClosureDateAPI
import io.sicredi.accounting.validation.domain.ClosureDateParameter.ClosureDate
import io.sicredi.accounting.validation.repository.ClosureDateRepository
import io.sicredi.accounting.validation.service.ClosureDateService
import org.apache.flink.streaming.api.functions.sink.{RichSinkFunction, SinkFunction}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class ClosureDateSink(service: ClosureDateService, repository: ClosureDateRepository, closureDateAPI: ClosureDateAPI) extends RichSinkFunction[Either[String, ClosureDate]] with StrictLogging {

  override def invoke(maybeClosureDate: Either[String, ClosureDate], context: SinkFunction.Context[_]): Unit = {
    logger.debug("starting closure date sink")

    if (maybeClosureDate.isRight) {
      val futureResult = for {
        externalApiResult: Either[String, Seq[ClosureDate]] <- closureDateAPI.getAllClosureDate

        resultDB <- service.insertAll(externalApiResult.right.get)
      } yield resultDB

      futureResult onComplete {
        case Success(value) =>
          logger.debug("cleaning the cache")
          service.clearCache()

        case Failure(exception) =>
          logger.error(exception.getLocalizedMessage)
      }
    } else {
      logger.debug("closure date not parsed")
    }

  }

}

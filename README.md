Accounting Engine Validation
===
This stream processor gets data from accounting-validation topic that contains
accounting-related data and sent it to the Success or Error topic.

This project is based on: Scala, Flink, Kafka, Mongo

### Compile

`$sbt compile`

### Run

`$sbt run`

### Run (with auto compiling)

`$sbt ~run`

##Build proccess
`$./sbt -Dsbt.log.noformat=true -Dconfig.resource=dev.conf assembly`

##Starting jar
`$ java -jar accounting-engine-validation.jar`

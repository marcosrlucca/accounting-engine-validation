package io.sicredi.accounting.validation.service

import com.github.benmanes.caffeine.cache
import com.github.benmanes.caffeine.cache.Caffeine
import io.sicredi.accounting.validation.domain.ClosureDateParameter.ClosureDate
import io.sicredi.accounting.validation.domain.CompanyParameter.Company
import io.sicredi.accounting.validation.domain.EventParameter.{BalanceEventParameter, MovementEventParameter}
import scalacache.caffeine.CaffeineCache
import scalacache.Entry
import scala.concurrent.Future

object Cache {

  import scalacache.Cache

  val underlyingBalanceCaffeineCache: cache.Cache[String, Entry[BalanceCache]] = Caffeine.newBuilder().maximumSize(maximumSizeCache).build[String, Entry[BalanceCache]]

  val customisedBalanceRawCache: CaffeineCache[BalanceCache] = CaffeineCache(underlyingBalanceCaffeineCache)

  val underlyingMovementCaffeineCache: cache.Cache[String, Entry[MovementCache]] = Caffeine.newBuilder().maximumSize(maximumSizeCache).build[String, Entry[MovementCache]]

  val customisedMovementRawCache: CaffeineCache[MovementCache] = CaffeineCache(underlyingMovementCaffeineCache)

  val closureDate: ClosureDateCache = ClosureDateCache(CaffeineCache[Future[Seq[ClosureDate]]])

  val companyParameterCache: CompanyParameterCache = CompanyParameterCache(CaffeineCache[Future[Seq[Company]]])

  val movementEventParameter: MovementEventParameterCache = MovementEventParameterCache(CaffeineCache[Future[Seq[MovementEventParameter]]])

  val balanceEventParameter: BalanceEventParameterCache = BalanceEventParameterCache(CaffeineCache[Future[Seq[BalanceEventParameter]]])

  case class ClosureDateCache(cache: Cache[Future[Seq[ClosureDate]]])

  case class CompanyParameterCache(cache: Cache[Future[Seq[Company]]])

  case class MovementEventParameterCache(cache: Cache[Future[Seq[MovementEventParameter]]])

  case class BalanceEventParameterCache(cache: Cache[Future[Seq[BalanceEventParameter]]])

  case class MovementCache(uniqueIndex: Option[String], baseDate: Option[String], var dateReversalOcurred: Option[String])

  case class BalanceCache(uniqueIndex: Option[String], baseDate: Option[String])

}

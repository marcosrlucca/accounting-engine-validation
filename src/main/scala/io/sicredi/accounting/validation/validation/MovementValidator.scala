package io.sicredi.accounting.validation.validation

import io.sicredi.accounting.validation.domain.EventParameter.{EventParameter, MovementEventParameter}
import io.sicredi.accounting.validation.domain.Raw.{MovementRaw, RawProcess, RawProcessFailure}
import io.sicredi.accounting.validation.helper.ValidationHelper
import io.sicredi.accounting.validation.service.MovementEventParameterService
import org.apache.flink.streaming.api.scala.async.{AsyncFunction, ResultFuture}
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

class MovementValidator(service: MovementEventParameterService) extends RawValidator with AsyncFunction[RawProcess, Either[Some[RawProcessFailure], Some[RawProcess]]] {

  override def asyncInvoke(incomingEvent: RawProcess, resultFuture: ResultFuture[Either[Some[RawProcessFailure], Some[RawProcess]]]): Unit = {
    logger.debug("starting movement validator")

    service.getAll onComplete {
      case Success(eventParameters) =>
        val rulesFromIncomingEvent = incomingEvent.payloadParsed.get.rules.get

        val result = eventParameters
          .map(eventPar => isSameEventCodeOrProductCode(incomingEvent, eventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isSameSystem(incomingEvent, resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isRulesDefined(resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isSameSizeRules(rulesFromIncomingEvent, resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isValueDefined(resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isValuesSameSize(incomingEvent, resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isValuesDifferentThanZero(incomingEvent, resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isValuesHigherThanZero(incomingEvent, resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isValueWithTwoDecimalPlaces(incomingEvent, resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isDateValid(incomingEvent, resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isOptionsPresent(resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isScriptsPresent(resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isOptionsValid(incomingEvent.payloadParsed.get.asInstanceOf[MovementRaw], resultValidationEventPar))
          .filter(resultValidationEventPar => resultValidationEventPar.isRight)
          .map(resultValidationEventPar => isScriptsFromMovementAndRulesFromRawValid(incomingEvent.payloadParsed.get.asInstanceOf[MovementRaw], resultValidationEventPar))
          .exists(resultValidationEventPar => resultValidationEventPar.isRight)

        if (result) {
          logger.debug("movement validator result is right")
          resultFuture.complete(Iterable(Right(Some(incomingEvent))))
        } else {
          logger.debug("movement validator result is left")
          resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = Some(errors.get mkString ","), failuredRaw = Some(incomingEvent.payloadParsed.get), failuredPayload = incomingEvent.payload, topicPostingTimestamp = incomingEvent.topicPostingTimestamp)))))
        }

      case Failure(exception) =>
        logger.error(exception.getLocalizedMessage)
        resultFuture.completeExceptionally(exception)
    }
  }

  override def isScriptsPresent(resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    val validated = resultValidationEventPar.right.get.asInstanceOf[MovementEventParameter].scripts.get.exists(roadmap => roadmap.options.isDefined && roadmap.options.get.nonEmpty && roadmap.options.get.exists(opt => opt.option.isDefined))

    if (!validated) {
      errors.foreach(e => e(SCRIPTS_NOT_EXISTS) = SCRIPTS_NOT_EXISTS_TEXT)
      Left(SCRIPTS_NOT_EXISTS_TEXT)
    } else {
      Right(resultValidationEventPar.right.get.asInstanceOf[MovementEventParameter])
    }
  }

  override def isValueWithTwoDecimalPlaces(incomingEvent: RawProcess, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    incomingEvent.payloadParsed
      .filter(raw => raw.isInstanceOf[MovementRaw])
      .map(raw => raw.asInstanceOf[MovementRaw])
      .map(movementRaw => {
        errors.foreach(e => e.clear())

        val notValidated = movementRaw.values.get.exists(v => isTwoDecimalPlaces(v))

        if (notValidated) {
          errors.foreach(e => e(VALUE_TWO_DECIMAL_PLACES) = VALUE_TWO_DECIMAL_PLACES_TEXT)
          Left(VALUE_TWO_DECIMAL_PLACES_TEXT)
        } else {
          Right(resultValidationEventPar.right.get.asInstanceOf[MovementEventParameter])
        }

      })
      .getOrElse(resultValidationEventPar)
  }

  override def isScriptsPresent(eventPar: EventParameter): Either[String, EventParameter] = {
    val movementParameter = eventPar.asInstanceOf[MovementEventParameter]
    val validated = movementParameter.scripts.get.exists(roadmap => roadmap.options.isDefined && roadmap.options.get.nonEmpty && roadmap.options.get.exists(opt => opt.option.isDefined))

    if (!validated) {
      errors.foreach(e => e(SCRIPTS_NOT_EXISTS) = SCRIPTS_NOT_EXISTS_TEXT)
      Left(SCRIPTS_NOT_EXISTS_TEXT)
    } else {
      Right(eventPar)
    }
  }

  private def isScriptsFromMovementAndRulesFromRawValid(raw: MovementRaw, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    val scriptsIsValid = ValidationHelper.isScriptMovementAndRulesFromRawValid(resultValidationEventPar.right.get.asInstanceOf[MovementEventParameter], raw, errors)

    if (scriptsIsValid) {
      resultValidationEventPar
    } else {
      errors.foreach(e => e(SCRIPTS_NOT_VALID) = SCRIPTS_NOT_VALID_TEXT)
      Left(SCRIPTS_NOT_VALID_TEXT)
    }
  }

  private def isValueDefined(resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    val validated = resultValidationEventPar.right.get.asInstanceOf[MovementEventParameter].values.isDefined

    if (!validated) {
      errors.foreach(e => e(VALUE_NOT_DEFINED) = VALUE_NOT_DEFINED_TEXT)
      Left(VALUE_NOT_DEFINED_TEXT)
    } else {
      Right(resultValidationEventPar.right.get.asInstanceOf[MovementEventParameter])
    }
  }

  private def isValuesSameSize(valuesFromIncomingEvent: Seq[BigDecimal], eventPar: MovementEventParameter): Either[String, MovementEventParameter] = {
    val values = eventPar.asInstanceOf[MovementEventParameter].values
    val validated = values.isDefined && values.get.size == valuesFromIncomingEvent.size

    if (!validated) {
      errors.foreach(e => e(VALUE_DIFFERENT_SIZE) = VALUE_DIFFERENT_SIZE_TEXT)
      Left(VALUE_DIFFERENT_SIZE_TEXT)
    } else {
      Right(eventPar)
    }
  }

  private def isValuesHigherThanZero(incomingEvent: RawProcess, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    incomingEvent.payloadParsed
      .filter(raw => raw.isInstanceOf[MovementRaw])
      .map(raw => raw.asInstanceOf[MovementRaw])
      .map(movementRaw => {
        errors.foreach(e => e.clear())

        val notValidated = movementRaw.values.get.exists(v => v < 0)

        if (notValidated) {
          errors.foreach(e => e(VALUE_LESS_ZERO) = VALUE_LESS_ZERO_TEXT)
          Left(VALUE_NOT_DEFINED_TEXT)
        } else {
          Right(resultValidationEventPar.right.get.asInstanceOf[MovementEventParameter])
        }
      })
      .getOrElse(resultValidationEventPar)
  }

  private def isValuesDifferentThanZero(incomingEvent: RawProcess, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    incomingEvent.payloadParsed
      .filter(raw => raw.isInstanceOf[MovementRaw])
      .map(raw => raw.asInstanceOf[MovementRaw])
      .map(movementRaw => {
        errors.foreach(e => e.clear())

        val validated = movementRaw.values.get.exists(v => v != 0)

        if (!validated) {
          errors.foreach(e => e(VALUE_EQUAL_ZERO) = VALUE_EQUAL_ZERO_TEXT)
          Left(VALUE_NOT_DEFINED_TEXT)
        } else {
          Right(resultValidationEventPar.right.get.asInstanceOf[MovementEventParameter])
        }
      })
      .getOrElse(resultValidationEventPar)
  }

  private def isValuesSameSize(incomingEvent: RawProcess, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    incomingEvent.payloadParsed
      .filter(raw => raw.isInstanceOf[MovementRaw])
      .map(raw => raw.asInstanceOf[MovementRaw])
      .map(movementRaw => {
        errors.foreach(e => e.clear())

        isValuesSameSize(movementRaw.values.get, resultValidationEventPar.right.get.asInstanceOf[MovementEventParameter])
      })
      .getOrElse(resultValidationEventPar)
  }
}
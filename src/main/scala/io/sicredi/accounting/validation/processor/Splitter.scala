package io.sicredi.accounting.validation.processor

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.Raw.{MovementRaw, RawProcess, RawProcessFailure}

class Splitter extends Serializable with StrictLogging {

  def splitByResultOfParsing(result: Either[Some[RawProcessFailure], Some[RawProcess]]): Seq[String] = {
    logger.debug("splitting by result of parsing")

    result match {
      case Right(_) => Seq(PARSED)
      case Left(_) => Seq(NOT_PARSED)
    }
  }

  def splitByResultOfValidation(result: Either[Some[RawProcessFailure], Some[RawProcess]]): Seq[String] = {
    logger.debug("splitting by result of validation")

    result match {
      case Right(_) => Seq(VALIDATED)
      case Left(_) => Seq(NOT_VALIDATED)
    }
  }

  def splitByMovementOrBalance(validatedEvent: RawProcess): Seq[String] = {
    logger.debug("splitting by movement or balance")

    if (validatedEvent.payloadParsed.get.isInstanceOf[MovementRaw])
      Seq(MOVEMENT)
    else
      Seq(BALANCE)
  }

}

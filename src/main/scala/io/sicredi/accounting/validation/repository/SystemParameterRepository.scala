package io.sicredi.accounting.validation.repository

import io.sicredi.accounting.validation.configuration.Mongo
import io.sicredi.accounting.validation.domain.SystemParameter.SystemParameterView
import reactivemongo.api.Cursor
import reactivemongo.bson.BSONDocument
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}

class SystemParameterRepository(mongoConfiguration: Mongo) {

  def findAll(): Future[Seq[SystemParameterView]] = {
    val query = BSONDocument.empty

    Try(
      mongoConfiguration.getSystemParameterCollection
        .flatMap(_.find(query)
          .cursor[SystemParameterView]()
          .collect[Seq](-1, Cursor.FailOnError[Seq[SystemParameterView]]()))
    ) match {
      case Success(rs) => rs
      case Failure(_) => Future(Seq.empty)
    }
  }

}

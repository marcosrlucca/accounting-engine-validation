package io.sicredi.accounting.validation.repository

import io.sicredi.accounting.validation.configuration.Mongo
import io.sicredi.accounting.validation.domain.Raw.{Balance, BalanceRaw}
import io.sicredi.accounting.validation.helper.mapper.RawMapper
import reactivemongo.api.Cursor
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class IncomingBalanceRepository(mongoConfiguration: Mongo) extends IncomingRawRepository[BalanceRaw](mongoConfiguration) {

  override def findOneBy(incomingRaw: BalanceRaw): Future[Option[BalanceRaw]] = {
    val collectionName = getCollectionName(incomingRaw).get
    val eventualCollection = mongoConfiguration.getCollectionByCollectionName(collectionName.toLowerCase)
    val query = getClauseWhere(incomingRaw._unique_index.get)

    Try(eventualCollection
      .flatMap(_.find(query).one[Balance])
      .map(balance => RawMapper.mapToBalanceRaw(balance))) match {
      case Success(rs) => rs
      case Failure(_) => Future(None)
    }
  }

  override def findAllBy(incomingRaw: BalanceRaw): Future[Option[mutable.Map[String, BalanceRaw]]] = {
    getCollectionName(incomingRaw)
      .map(collectionName => mongoConfiguration.getCollectionByCollectionName(collectionName.toLowerCase))
      .map(col => {
        val query = getClauseWhere(incomingRaw._unique_index.get)

        Try(col
          .flatMap(_.find(query)
            .cursor[Balance]()
            .collect[Seq](-1, Cursor.FailOnError[Seq[Balance]]()))
          .map(balances => balances
            .flatMap(b => RawMapper.mapToBalanceRaw(Some(b))))
          .map(seqBalance => seqBalance.map(s => s._unique_index.get -> s).toMap)
          .map(immutableMap => scala.collection.mutable.Map(immutableMap.toSeq: _*))
          .map(mutableMap => Some(mutableMap))
        ) match {
          case Success(rs: Future[Some[mutable.Map[String, BalanceRaw]]]) => rs
          case Failure(_) => Future(None)
        }
      })
      .getOrElse(Future(None))
  }

}

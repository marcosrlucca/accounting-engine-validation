package io.sicredi.accounting.validation.repository

import io.sicredi.accounting.validation.configuration.Mongo
import io.sicredi.accounting.validation.domain.CompanyParameter.Company
import reactivemongo.api.Cursor
import reactivemongo.bson.document
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class CompanyParameterRepository(mongoConfiguration: Mongo) extends Serializable {

  def findAll(): Future[Seq[Company]] = {

    val query = document()

    Try(mongoConfiguration.getCompanyCollection.flatMap(_.find(query)
      .cursor[Company]()
      .collect[Seq](-1, Cursor.FailOnError[Seq[Company]]()))) match {
      case Success(rs) => rs
      case Failure(_) => Future(Seq.empty)
    }

  }

}

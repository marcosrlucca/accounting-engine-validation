package io.sicredi.accounting.validation.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.typesafe.scalalogging.StrictLogging
import io.sicredi.services.platform.client.core.{Protocol, ServerConfiguration}
import io.sicredi.services.platform.vault.client._
import io.sicredi.services.platform.vault.client.credentials.{LoginRequest, LoginToken, VaultCredentials, VaultCredentialsFactory}
import okhttp3.OkHttpClient
import scala.util.{Failure, Success}

case class VaultProperties(okHttpClient: OkHttpClient, vaultAddress: String, vaultPort: Int, vaultRole: String, vaultToken: String)

case class Vault(vaultProperties: VaultProperties) extends StrictLogging {

  private lazy val serverConfiguration = new ServerConfiguration(
    Protocol.HTTPS,
    vaultProperties.vaultAddress,
    vaultProperties.vaultPort)

  private lazy val loginRequest = new LoginRequest(
    vaultProperties.vaultRole,
    vaultProperties.vaultToken)

  private lazy val mapper = new ObjectMapper
  private lazy val client: VaultClient = VaultClientFatory.createVaultClient(serverConfiguration, mapper, vaultProperties.okHttpClient)
  private lazy val credentialsClient: VaultCredentials = VaultCredentialsFactory.createVaultCredentials(serverConfiguration, mapper, vaultProperties.okHttpClient)
  private lazy val loginToken: LoginToken = credentialsClient.credentials(loginRequest)

  def getSecret(root: String, key: String): String = {
    val readRequest = new SecretReadRequest(new Secret(root, key))

    retry(client.read(loginToken, readRequest).getSecretKeyValues.get(0).getValue, 5) match {
      case Success(value) => value
      case Failure(f) =>
        logger.error(s"""Unable to fetch value $root$key from Vault! ${f.getMessage}""")

        throw new RuntimeException(s"""Unable to fetch value $root$key from Vault!""")
    }
  }

}

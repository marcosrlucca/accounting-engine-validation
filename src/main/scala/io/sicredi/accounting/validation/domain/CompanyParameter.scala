package io.sicredi.accounting.validation.domain

import play.api.libs.json.{Json, OWrites, Reads}

object CompanyParameter {

  implicit val readsCompany: Reads[Company] = Json.reads[Company]
  implicit val writesCompany: OWrites[Company] = Json.writes[Company]

  case class Company(code: Option[String],
                     socialName: Option[String],
                     fantasyName: Option[String],
                     units: Option[Seq[String]])

}

package io.sicredi.accounting.validation.helper

import io.sicredi.accounting.validation.domain.EventParameter._
import io.sicredi.accounting.validation.domain.Raw.{BalanceRaw, MovementRaw}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers, PrivateMethodTester}
import reactivemongo.bson.BSONDateTime

class ValidationHelperSpec extends FlatSpec with Matchers with MockFactory with PrivateMethodTester {

  "The method isSameOption" should "return true" in {
    val result = ValidationHelper.isSameOption("*", RuleOption(Some("*"), None, None))

    assert(result)
  }

  "The method isSameOption" should "return false" in {
    val result = ValidationHelper.isSameOption("999", RuleOption(Some("*"), None, None))

    assert(!result)
  }

  "The method isSameSizeRules" should "return true" in {
    val parameter = MovementEventParameter(rules = Some(Seq(Rule(None, None, None), Rule(None, None, None))))
    val result = ValidationHelper.isSameSizeRules(Seq("*", "999"), parameter)

    assert(result)
  }

  "The method isSameSizeRules" should "return false" in {
    val parameter = MovementEventParameter(rules = Some(Seq(Rule(None, None, None), Rule(None, None, None))))
    val result = ValidationHelper.isSameSizeRules(Seq("*", "1001", "999"), parameter)

    assert(!result)
  }

  "The method isDateEventParameterValid" should "return false when incomingRawDateString is empty" in {
    val result = ValidationHelper.isDateEventParameterValid("", null)
    assert(!result)
  }

  "The method isDateEventParameterValid" should "return false when incomingRawDateString is invalid" in {
    val result = ValidationHelper.isDateEventParameterValid("abc", null)
    assert(!result)
  }

  "The method isDateEventParameterValid" should "return false when startValidity is invalid" in {
    val date = "2018-02-12T08:32:41.746-02:00[America/Sao_Paulo]"
    val parameter = MovementEventParameter(startValidity = Some(BSONDateTime(1549967561746L)))

    val result = ValidationHelper.isDateEventParameterValid(date, parameter)
    assert(!result)
  }

  "The method isDateEventParameterValid" should "return false when date is expired" in {
    val date = "2020-02-12T08:32:41.746-02:00[America/Sao_Paulo]"
    val parameter = MovementEventParameter(startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val result = ValidationHelper.isDateEventParameterValid(date, parameter)
    assert(!result)
  }

  "The method isDateEventParameterValid" should "return true when" in {
    val date = "2020-02-12T08:32:41.746-02:00[America/Sao_Paulo]"
    val parameter = MovementEventParameter(startValidity = Some(BSONDateTime(1518431561746L)))

    val result = ValidationHelper.isDateEventParameterValid(date, parameter)
    assert(result)
  }

  "The method isDateSystemParameterValid" should "return false when parameter systems is None" in {
    val parameter = MovementEventParameter(system = None, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      _unique_index = None,
      topicPostingTimestamp = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = None,
      values = None)

    val result = ValidationHelper.isDateSystemParameterValid(parameter, raw)

    assert(!result)
  }

  "The method isDateSystemParameterValid" should "return false when system raw is None" in {
    val date = "2020-02-12T08:32:41.746-02:00[America/Sao_Paulo]"
    val someSystem = Some(System(sapCode = Some("FD"), startValidity = Some(BSONDateTime(1518431561746L))))
    val parameter = MovementEventParameter(system = someSystem, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = None,
      creditUnit = None,
      debitUnit = None,
      rules = None,
      values = None)

    val result = ValidationHelper.isDateSystemParameterValid(parameter, raw)

    assert(!result)
  }

  "The method isDateSystemParameterValid" should "return true when" in {
    val date = "2020-02-12T08:32:41.746-02:00[America/Sao_Paulo]"
    val someSystem = Some(System(name = Some("FIDELIDADE"), startValidity = Some(BSONDateTime(1518431561746L))))
    val parameter = MovementEventParameter(system = someSystem, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FIDELIDADE"),
      creditUnit = None,
      debitUnit = None,
      rules = None,
      values = None)

    val result = ValidationHelper.isDateSystemParameterValid(parameter, raw)

    assert(result)
  }

  "The method isDateSystemParameterValid" should "return false when end date is invalid" in {
    val system = System(startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val result = ValidationHelper.isDateSystemParameterValid(system)

    assert(!result)
  }

  "The method isDateSystemParameterValid" should "return false when start date is invalid" in {
    val system = System(startValidity = Some(BSONDateTime(1865586761746L)), endValidity = Some(BSONDateTime(1518431561746L)))

    val result = ValidationHelper.isDateSystemParameterValid(system)

    assert(!result)
  }

  "The method isDateSystemParameterValid" should "return true when start date is valid" in {
    val system = System(startValidity = Some(BSONDateTime(1518431561746L)))

    val result = ValidationHelper.isDateSystemParameterValid(system)

    assert(result)
  }

  "The method isDateScriptParameterValid for BalanceScript" should "return false when end date is invalid" in {
    val script = BalanceScript(None, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)), None)

    val result = ValidationHelper.isDateScriptParameterValid(script)

    assert(!result)
  }

  "The method isDateScriptParameterValid for BalanceScript" should "return false when start date is invalid" in {
    val script = BalanceScript(None, startValidity = Some(BSONDateTime(1865586761746L)), endValidity = Some(BSONDateTime(1518431561746L)), None)

    val result = ValidationHelper.isDateScriptParameterValid(script)

    assert(!result)
  }

  "The method isDateScriptParameterValid for BalanceScript" should "return true" in {
    val script = BalanceScript(None, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None, None)

    val result = ValidationHelper.isDateScriptParameterValid(script)

    assert(result)
  }

  "The method isDateScriptParameterValid for MovementScript" should "return false when end date is invalid" in {
    val script = MovementScript(None, None, None, None, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)), None)

    val result = ValidationHelper.isDateScriptParameterValid(script)

    assert(!result)
  }

  "The method isDateScriptParameterValid for MovementScript" should "return false when start date is invalid" in {
    val script = MovementScript(None, None, None, None, startValidity = Some(BSONDateTime(1865586761746L)), endValidity = Some(BSONDateTime(1518431561746L)), None)

    val result = ValidationHelper.isDateScriptParameterValid(script)

    assert(!result)
  }

  "The method isDateScriptParameterValid for MovementScript" should "return true" in {
    val script = MovementScript(None, None, None, None, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None, None)

    val result = ValidationHelper.isDateScriptParameterValid(script)

    assert(result)
  }

  "The method isOptionValid" should "return false when end date is invalid" in {
    val script = RuleOption(startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val result = ValidationHelper.isOptionDateValid(script)

    assert(!result)
  }

  "The method isOptionValid" should "return false when start date is invalid" in {
    val script = RuleOption(startValidity = Some(BSONDateTime(1865586761746L)), endValidity = Some(BSONDateTime(1518431561746L)))

    val result = ValidationHelper.isOptionDateValid(script)

    assert(!result)
  }

  "The method isOptionValid" should "return true" in {
    val script = RuleOption(startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)

    val result = ValidationHelper.isOptionDateValid(script)

    assert(result)
  }

  "The method isRulesFromIncomingEventValid" should "return false when everything is None" in {
    val parameter = MovementEventParameter(system = None, startValidity = None, endValidity = None)
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = None,
      creditUnit = None,
      debitUnit = None,
      rules = None,
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(!result)
  }

  "The method isRulesFromIncomingEventValid" should "return true when" in {
    val ruleA = Rule(description = None, options = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val someOptions = Some(Seq(ruleA))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*")),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(result)
  }

  "The method isRulesFromIncomingEventValid" should "return false when the parameter options array is bigger than" in {
    val ruleA = Rule(description = None, options = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("1001"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val ruleB = Rule(description = None, options = Some(Seq(
      RuleOption(Some("123"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("134"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("111"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val someOptions = Some(Seq(ruleA, ruleB))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "1113")),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(!result)
  }

  "The method isRulesFromIncomingEventValid" should "return false when the parameter options does not have the correct option" in {
    val ruleA = Rule(description = None, options = Some(Seq(
      RuleOption(Some("1212*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val someOptions = Some(Seq(ruleA))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "999")),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(!result)
  }

  "The method isRulesFromIncomingEventValid" should "return false when the parameter options array is empty" in {
    val ruleA = Rule(description = None, options = Some(Seq()))

    val someOptions = Some(Seq(ruleA))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "999")),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(!result)
  }

  "The method isRulesFromIncomingEventValid" should "return false when the incoming event options array is empty" in {
    val ruleA = Rule(description = None, options = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val someOptions = Some(Seq(ruleA))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq()),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(!result)
  }

  "The method isRulesFromIncomingEventValid" should "return false when the incoming event options array is lower than" in {
    val ruleA = Rule(description = None, options = Some(Seq(
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val someOptions = Some(Seq(ruleA))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "1")),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(!result)
  }

  "The method isRulesFromIncomingEventValid" should "return false when the incoming event options array is bigger than" in {
    val ruleA = Rule(description = None, options = Some(Seq(
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val ruleB = Rule(description = None, options = Some(Seq(
      RuleOption(Some("1"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val ruleC = Rule(description = None, options = Some(Seq(
      RuleOption(Some("3"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("4"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val ruleD = Rule(description = None, options = Some(Seq(
      RuleOption(Some("5"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("6"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val someOptions = Some(Seq(ruleA, ruleB, ruleC, ruleD))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("999", "1", "3")),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(!result)
  }

  "The method isRulesFromIncomingEventValid" should "return false when the incoming event options array is not equal" in {
    val ruleA = Rule(description = None, options = Some(Seq(
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val ruleB = Rule(description = None, options = Some(Seq(
      RuleOption(Some("1"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val ruleC = Rule(description = None, options = Some(Seq(
      RuleOption(Some("3"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("4"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val ruleD = Rule(description = None, options = Some(Seq(
      RuleOption(Some("5"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("6"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val someOptions = Some(Seq(ruleA, ruleB, ruleC, ruleD))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("1", "999", "2", "a")),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(!result)
  }

  "The method isRulesFromIncomingEventValid" should "return true when the there are a lot of rules" in {
    val ruleA = Rule(description = None, options = Some(Seq(
      RuleOption(Some("1"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("3"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("6"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val ruleB = Rule(description = None, options = Some(Seq(
      RuleOption(Some("4"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("6"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val ruleC = Rule(description = None, options = Some(Seq(
      RuleOption(Some("9"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("28"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val someOptions = Some(Seq(ruleA, ruleB, ruleC))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("1", "999", "28")),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(result)
  }

  "The method isRulesFromIncomingEventValid" should "return false when there is one rule" in {
    val ruleA = Rule(description = None, options = Some(Seq(
      RuleOption(Some("1"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("3"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    )))

    val someOptions = Some(Seq(ruleA))

    val parameter = MovementEventParameter(rules = someOptions, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))
    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("2")),
      values = None)

    val result = ValidationHelper.isRulesFromIncomingEventValid(raw, parameter)

    assert(result)
  }

  "The method isScriptsFromMovementValid" should "return false when the parameter script list is empty" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val parameter = MovementEventParameter(scripts = None, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "999")),
      values = None)

    val result = ValidationHelper.isScriptMovementAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromMovementValid" should "return false when the incoming event options array is less than" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = MovementEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "999")),
      values = None)

    val result = ValidationHelper.isScriptMovementAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromMovementValid" should "return false when the incoming event options array is bigger than" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = MovementEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "999", "2", "3")),
      values = None)

    val result = ValidationHelper.isScriptMovementAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromMovementValid" should "return false when the parameter options array is bigger than" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = MovementEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      dateReversalOcurred = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "999", "2", "3")),
      values = None)

    val result = ValidationHelper.isScriptMovementAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromMovementValid" should "return false when the parameter options array is lower than" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = MovementEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "999", "2", "3")),
      values = None)

    val result = ValidationHelper.isScriptMovementAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromMovementValid" should "return false when the parameter options array is equal to" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = MovementEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("2", "1")),
      values = None)

    val result = ValidationHelper.isScriptMovementAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromMovementValid" should "return false when there are a lot of scripts, but none correct" in {
    val someOptionsA = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val someOptionsB = Some(Seq(
      RuleOption(Some("3"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("4"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val someOptionsC = Some(Seq(
      RuleOption(Some("7"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("8"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsA),
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsB),
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsC)
    ))

    val parameter = MovementEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*1", "2")),
      values = None)

    val result = ValidationHelper.isScriptMovementAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromMovementValid" should "return true when there are a lot of scripts" in {
    val someOptionsA = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val someOptionsB = Some(Seq(
      RuleOption(Some("3"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("4"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val someOptionsC = Some(Seq(
      RuleOption(Some("7"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("8"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsA),
      MovementScript(debitAccount = Some("123"),
        creditAccount = Some("123"),
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = Some(BSONDateTime(1518431561746L)),
        endValidity = None,
        options = someOptionsB),
      MovementScript(debitAccount = None,
        creditAccount = None,
        sapCreditTransaction = None,
        sapDebitTransaction = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsC)
    ))

    val parameter = MovementEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("3", "4")),
      values = None)

    val result = ValidationHelper.isScriptMovementAndRulesFromRawValid(parameter, raw, None)

    assert(result)
  }

  "The method isScriptsFromBalanceValid" should "return false when the incoming event options array is less than" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = BalanceEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = BalanceRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      operationDate = None,
      document = None,
      company = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      unit = None,
      rules = Some(Seq("*", "999")),
      value = None)

    val result = ValidationHelper.isScriptBalanceAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromBalanceValid" should "return false when the incoming event options array is bigger than" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = BalanceEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = MovementRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      originalEventBaseDateForReversal = None,
      operationDate = None,
      document = None,
      company = None,
      reversal = None,
      dateReversalOcurred = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      creditUnit = None,
      debitUnit = None,
      rules = Some(Seq("*", "999", "2", "3")),
      values = None)

    val result = ValidationHelper.isScriptBalanceAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromBalanceValid" should "return false when the parameter options array is bigger than" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("999"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = BalanceEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = BalanceRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      operationDate = None,
      document = None,
      company = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      unit = None,
      rules = Some(Seq("*", "999", "2", "3")),
      value = None)

    val result = ValidationHelper.isScriptBalanceAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromBalanceValid" should "return false when the parameter options array is lower than" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = BalanceEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = BalanceRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      operationDate = None,
      document = None,
      company = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      unit = None,
      rules = Some(Seq("*", "999", "2", "3")),
      value = None)

    val result = ValidationHelper.isScriptBalanceAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromBalanceValid" should "return false when the parameter options array is equal to" in {
    val someOptions = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptions)
    ))

    val parameter = BalanceEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = BalanceRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      operationDate = None,
      document = None,
      company = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      unit = None,
      rules = Some(Seq("2", "1")),
      value = None)

    val result = ValidationHelper.isScriptBalanceAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromBalanceValid" should "return false when there are a lot of scripts, but none correct" in {
    val someOptionsA = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val someOptionsB = Some(Seq(
      RuleOption(Some("3"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("4"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val someOptionsC = Some(Seq(
      RuleOption(Some("7"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("8"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsA),
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsB),
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsC)
    ))

    val parameter = BalanceEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = BalanceRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      operationDate = None,
      document = None,
      company = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      unit = None,
      rules = Some(Seq("*1", "2")),
      value = None)

    val result = ValidationHelper.isScriptBalanceAndRulesFromRawValid(parameter, raw, None)

    assert(!result)
  }

  "The method isScriptsFromBalanceValid" should "return true when there are a lot of scripts" in {
    val someOptionsA = Some(Seq(
      RuleOption(Some("*"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("2"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val someOptionsB = Some(Seq(
      RuleOption(Some("3"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("4"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val someOptionsC = Some(Seq(
      RuleOption(Some("7"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None),
      RuleOption(Some("8"), startValidity = Some(BSONDateTime(1518431561746L)), endValidity = None)
    ))

    val script = Some(Seq(
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsA),
      BalanceScript(account = Some("123"),
        startValidity = Some(BSONDateTime(1518431561746L)),
        endValidity = None,
        options = someOptionsB),
      BalanceScript(account = None,
        startValidity = None,
        endValidity = None,
        options = someOptionsC)
    ))

    val parameter = BalanceEventParameter(scripts = script, startValidity = Some(BSONDateTime(1518431561746L)), endValidity = Some(BSONDateTime(1549967561746L)))

    val raw = BalanceRaw(_id = None,
      topicPostingTimestamp = None,
      _unique_index = None,
      baseDate = None,
      operationDate = None,
      document = None,
      company = None,
      accountingReferenceCode = None,
      productReferenceCode = None,
      transactionDetail = None,
      transactionId = None,
      system = Some("FD"),
      unit = None,
      rules = Some(Seq("3", "4")),
      value = None)

    val result = ValidationHelper.isScriptBalanceAndRulesFromRawValid(parameter, raw, None)

    assert(result)
  }
}

package io.sicredi.accounting.validation.processor

import io.sicredi.accounting.validation.domain.Raw
import io.sicredi.accounting.validation.domain.Raw.{BalanceRaw, Event, MovementRaw, RawProcess, RawProcessFailure}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers, PrivateMethodTester}
import reactivemongo.bson.BSONObjectID

class FlinkProcessorSpec extends FlatSpec with Matchers with MockFactory with PrivateMethodTester {

  "The normalizeSuccessOrFailureRawProcess" should "return a Balance failured payload in string format" in {
    val privateNormalizeSuccessOrFailureRawProcess = PrivateMethod[String]('normalizeSuccessOrFailureRawProcess)

    val rawProcess: Either[Some[RawProcessFailure], Some[RawProcess]] =
      Left(Some(
        RawProcessFailure(
          rawProcessError = Some("Deu algum erro no processo"),
          failuredRaw = Some(BalanceRaw(
            Some(BSONObjectID.generate()), None, None, Some("2018-11-30 00:00:00+00:00"), None, Some("53125746000"), Some("0106"), Some("10-40-101"), None,
            Some("1549628030699537858"), Some("D791097"), Some("RD"), Some("0116"), Some(Seq("999", "*")), Some(99.88)
          )),
          failuredPayload = Some(MessagesBucket.payloadBalanceError),
          topicPostingTimestamp = None)
      ))

    val result: String = FlinkProcessor invokePrivate privateNormalizeSuccessOrFailureRawProcess(rawProcess)

    assert(result.contains("rawProcessError"))
    assert(result.contains("BalanceRaw"))
    assert(result.contains("failuredRaw"))
    assert(result.contains("failuredPayload"))
  }

  "The normalizeSuccessOrFailureRawProcess" should "return a Movement failured payload in string format" in {
    val privateNormalizeSuccessOrFailureRawProcess = PrivateMethod[String]('normalizeSuccessOrFailureRawProcess)

    val rawProcess: Either[Some[RawProcessFailure], Some[RawProcess]] =
      Left(Some(
        RawProcessFailure(
          rawProcessError = Some("Deu algum erro no processo"),
          failuredRaw = Some(MovementRaw(
            Some(BSONObjectID.generate()), None, None, Some("2018-11-30 00:00:00+00:00"), None, Some("2018-11-30 00:00:00+00:00"), Some("53125746000"), Some("0106"), Some(false), None, Some("10-40-101"), None,
            Some("1549628030699537858"), Some("5c48caf00154540b46b839b1"), Some("RD"), Some("0116"), Some("0116"), Some(Seq("999", "*")), Some(Seq(100, 99.88))
          )),
          failuredPayload = Some(MessagesBucket.payloadBalanceError),
          topicPostingTimestamp = None)
      ))

    val result: String = FlinkProcessor invokePrivate privateNormalizeSuccessOrFailureRawProcess(rawProcess)

    assert(result.contains("rawProcessError"))
    assert(result.contains("MovementRaw"))
    assert(result.contains("failuredRaw"))
    assert(result.contains("failuredPayload"))
  }

  "The normalizeSuccessOrFailureRawProcess" should "return a payload of Balance in string format" in {
    val privateNormalizeSuccessOrFailureRawProcess = PrivateMethod[String]('normalizeSuccessOrFailureRawProcess)

    val rawProcess: Either[Some[RawProcessFailure], Some[RawProcess]] = Right(Some(RawProcess(
      Some(
        BalanceRaw(
          Some(BSONObjectID.generate()), None, None, Some("2018-11-30 00:00:00+00:00"), None, Some("53125746000"), Some("0106"), Some("10-40-101"), None,
          Some("1549628030699537858"), Some("5c48caf00154540b46b839b1"), Some("RD"), Some("0116"), Some(Seq("999", "*")), Some(99.88)
        )
      ), None, None, None, None)))

    val result: String = FlinkProcessor invokePrivate privateNormalizeSuccessOrFailureRawProcess(rawProcess)

    assert(result.contains("class"))
    assert(result.contains("BalanceRaw"))
    assert(result.contains("data"))
  }

  "The normalizeSuccessOrFailureRawProcess" should "return a payload of Movement in string format" in {
    val privateNormalizeSuccessOrFailureRawProcess = PrivateMethod[String]('normalizeSuccessOrFailureRawProcess)

    val rawProcess: Either[Some[RawProcessFailure], Some[RawProcess]] = Right(Some(RawProcess(
      Some(
        MovementRaw(
          Some(BSONObjectID.generate()), None, None, Some("2018-11-30 00:00:00+00:00"), None, Some("2018-11-30 00:00:00+00:00"), Some("53125746000"), Some("0106"), Some(false), None, Some("10-40-101"), None,
          Some("1549628030699537858"), Some("5c48caf00154540b46b839b1"), Some("RD"), Some("0116"), Some("0116"), Some(Seq("999", "*")), Some(Seq(100, 99.88))
        )
      ), None, None, None, None)))

    val result: String = FlinkProcessor invokePrivate privateNormalizeSuccessOrFailureRawProcess(rawProcess)

    assert(result.contains("class"))
    assert(result.contains("MovementRaw"))
    assert(result.contains("data"))
  }

  "The isRightNotEmpty" should "return true when a RawProcess is sent" in {
    val privateIsRightNotEmpty = PrivateMethod[Boolean]('isRightNotEmpty)

    val rawProcess: Either[Some[RawProcessFailure], Some[RawProcess]] = Right(Some(RawProcess(
      Some(MovementRaw(None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None)),
      None, None, None, None)))

    val result = FlinkProcessor invokePrivate privateIsRightNotEmpty(rawProcess)

    assert(result)
  }

  "The isRightNotEmpty" should "return false when a RawProcessFailure is sent" in {
    val privateIsRightNotEmpty = PrivateMethod[Boolean]('isRightNotEmpty)

    val rawProcessFailure: Either[Some[RawProcessFailure], Some[RawProcess]] = Left(Some(Raw.RawProcessFailure(None, None, None, None, None, None)))

    val result = FlinkProcessor invokePrivate privateIsRightNotEmpty(rawProcessFailure)

    assert(!result)
  }

  "The decodeFromRaw" should "return left side when a Balance error payload is sent" in {
    val privateDecodeFromRaw = PrivateMethod[Either[Some[RawProcessFailure], Some[RawProcess]]]('decodeFromRaw)

    val event = Event(MessagesBucket.payloadBalanceError, 0L)
    val result = FlinkProcessor invokePrivate privateDecodeFromRaw(event)

    assert(result.isLeft)
  }

  "The decodeFromRaw" should "return left side when a Movement error payload is sent" in {
    val privateDecodeFromRaw = PrivateMethod[Either[Some[RawProcessFailure], Some[RawProcess]]]('decodeFromRaw)

    val event = Event(MessagesBucket.payloadMovementError, 0L)
    val result = FlinkProcessor invokePrivate privateDecodeFromRaw(event)

    assert(result.isLeft)
  }

  "The filterPayload" should "return false when baseDate is not found" in {
    val privateFilterPayload = PrivateMethod[Boolean]('filterPayload)
    val event = Event(MessagesBucket.payloadLessBaseDate, 0L)
    val result = FlinkProcessor invokePrivate privateFilterPayload(event)

    assert(!result)
  }

  "The filterPayload" should "return false when eventCode is not found" in {
    val privateFilterPayload = PrivateMethod[Boolean]('filterPayload)
    val event = Event(MessagesBucket.payloadLessEventCode, 0L)
    val result = FlinkProcessor invokePrivate privateFilterPayload(event)

    assert(!result)
  }

  "The filterPayload" should "return false when eventId is not found" in {
    val privateFilterPayload = PrivateMethod[Boolean]('filterPayload)
    val event = Event(MessagesBucket.payloadLessEventId, 0L)
    val result = FlinkProcessor invokePrivate privateFilterPayload(event)

    assert(!result)
  }

  "The filterPayload" should "return false when originId is not found" in {
    val privateFilterPayload = PrivateMethod[Boolean]('filterPayload)
    val event = Event(MessagesBucket.payloadLessOriginId, 0L)
    val result = FlinkProcessor invokePrivate privateFilterPayload(event)

    assert(!result)
  }

  "The filterPayload" should "return false when transactionId is not found" in {
    val privateFilterPayload = PrivateMethod[Boolean]('filterPayload)
    val event = Event(MessagesBucket.payloadLessTransactionId, 0L)
    val result = FlinkProcessor invokePrivate privateFilterPayload(event)

    assert(!result)
  }

  "The filterPayload" should "return false when system is not found" in {
    val privateFilterPayload = PrivateMethod[Boolean]('filterPayload)
    val event = Event(MessagesBucket.payloadLessSystem, 0L)
    val result = FlinkProcessor invokePrivate privateFilterPayload(event)

    assert(!result)
  }

  "The filterPayload" should "return false when rules is not found" in {
    val privateFilterPayload = PrivateMethod[Boolean]('filterPayload)
    val event = Event(MessagesBucket.payloadLessRules, 0L)
    val result = FlinkProcessor invokePrivate privateFilterPayload(event)

    assert(!result)
  }

}

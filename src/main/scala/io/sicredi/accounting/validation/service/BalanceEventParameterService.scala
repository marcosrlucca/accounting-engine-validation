package io.sicredi.accounting.validation.service

import io.sicredi.accounting.validation.domain.EventParameter.BalanceEventParameter
import io.sicredi.accounting.validation.repository.BalanceEventParameterRepository
import scalacache.Cache
import scalacache.modes.sync._
import scala.concurrent.Future
import scala.language.{implicitConversions, postfixOps}

class BalanceEventParameterService(balanceEventRepository: BalanceEventParameterRepository) extends EventParameterService with Serializable {

  private lazy val balanceEventParameters: Cache[Future[Seq[BalanceEventParameter]]] = Cache.balanceEventParameter.cache

  override def getAll: Future[Seq[BalanceEventParameter]] = {
    val maybeEventParametersView = balanceEventParameters
      .get(BalanceEventParametersLabel)
      .getOrElse(balanceEventParameters.cachingForMemoize(BalanceEventParametersLabel)(Some(globalCacheDuration))(balanceEventRepository.findAll()))

    maybeEventParametersView
  }

}

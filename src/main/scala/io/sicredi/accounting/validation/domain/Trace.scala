package io.sicredi.accounting.validation.domain

import play.api.libs.json.{Json, OWrites, Reads}
import reactivemongo.bson.BSONDateTime

object Trace {

  implicit val readsBSONDateTime: Reads[BSONDateTime] = Json.reads[BSONDateTime]
  implicit val writesBSONDateTime: OWrites[BSONDateTime] = Json.writes[BSONDateTime]

  implicit val readsTraceLogging: Reads[TraceLogging] = Json.reads[TraceLogging]
  implicit val writesTraceLogging: OWrites[TraceLogging] = Json.writes[TraceLogging]

  case class TraceLogging(eventType: Option[String],
                          company: Option[String],
                          system: Option[String],
                          baseDate: Option[BSONDateTime],
                          var collectionName: Option[String] = None,
                          uniqueIndex: Option[String],
                          timestampIn: Option[BSONDateTime],
                          var timestampOut: Option[BSONDateTime] = None,
                          status: Option[String],
                          applicationName: Option[String]) extends Serializable

}

package io.sicredi.accounting.validation.repository

import io.sicredi.accounting.validation.configuration.Mongo
import io.sicredi.accounting.validation.helper.mapper.RawMapper
import io.sicredi.accounting.validation.domain.Raw.{Movement, MovementRaw}
import reactivemongo.api.Cursor
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class IncomingMovementRepository(mongoConfiguration: Mongo) extends IncomingRawRepository[MovementRaw](mongoConfiguration) {

  override def findOneBy(incomingRaw: MovementRaw): Future[Option[MovementRaw]] = {
    getCollectionName(incomingRaw)
      .map(collectionName => mongoConfiguration.getCollectionByCollectionName(collectionName.toLowerCase))
      .map(col => {
        val query = getClauseWhere(incomingRaw._unique_index.get)

        Try(col
          .flatMap(_.find(query).one[Movement])
          .map(movement => RawMapper.mapToMovementRaw(movement))) match {
          case Success(rs) => rs
          case Failure(_) => Future(None)
        }
      })
      .getOrElse(Future(None))
  }

  override def findAllBy(incomingRaw: MovementRaw): Future[Option[mutable.Map[String, MovementRaw]]] = {
    getCollectionName(incomingRaw)
      .map(collectionName => mongoConfiguration.getCollectionByCollectionName(collectionName.toLowerCase))
      .map(col => {
        val query = getClauseWhere(incomingRaw._unique_index.get)

        Try(col
          .flatMap(_.find(query)
            .cursor[Movement]()
            .collect[Seq](-1, Cursor.FailOnError[Seq[Movement]]()))
          .map(movements => movements
            .flatMap(m => RawMapper.mapToMovementRaw(Some(m))))
          .map(seqMovement => seqMovement.map(s => s._unique_index.get -> s).toMap)
          .map(immutableMap => scala.collection.mutable.Map(immutableMap.toSeq: _*))
          .map(mutableMap => Some(mutableMap))
        ) match {
          case Success(rs: Future[Some[mutable.Map[String, MovementRaw]]]) => rs
          case Failure(_) => Future(None)
        }
      })
      .getOrElse(Future(None))
  }

  def findOneReversalBy(incomingRaw: MovementRaw): Future[Option[MovementRaw]] = {
    val query = getClauseWhere(incomingRaw._unique_index.get)

    Try(mongoConfiguration.getCollectionByCollectionName(REVERSAL_WAITING_COLLECTION)
      .flatMap(_.find(query).one[Movement])
      .map(movement => RawMapper.mapToMovementRaw(movement))) match {
      case Success(rs) =>
        rs

      case Failure(e) =>
        Future(None)
    }
  }

}

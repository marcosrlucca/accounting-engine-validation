package io.sicredi.accounting.validation.helper

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId, ZonedDateTime}
import org.joda.time.DateTime
import reactivemongo.bson.BSONDateTime

object DateConversionHelper {

  private lazy val FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")

  def parseToZonedDateTime(maybeObjToConvert: Option[BSONDateTime]): Option[ZonedDateTime] = {
    maybeObjToConvert.map(objToConvert => {
      val dt: DateTime = new DateTime(objToConvert.value)
      val instant: Instant = Instant.ofEpochMilli(dt.getMillis)
      val zone: ZoneId = ZoneId.of(dt.getZone.getID, ZoneId.SHORT_IDS)
      ZonedDateTime.ofInstant(instant, zone)
    })
  }

  def parseToBSONDateTime(maybeObjToConvert: Option[ZonedDateTime]): Option[BSONDateTime] = {
    maybeObjToConvert.map(date => BSONDateTime.apply(date.toInstant.toEpochMilli))
  }

  def parseToDateString(maybeObjToConvert: Option[ZonedDateTime]): Option[String] = {
    if(maybeObjToConvert != null && maybeObjToConvert.nonEmpty) {
      Some(FORMATTER.format(maybeObjToConvert.get))
    } else {
      Option.empty
    }
  }
}

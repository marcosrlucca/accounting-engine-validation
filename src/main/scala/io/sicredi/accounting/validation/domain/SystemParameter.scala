package io.sicredi.accounting.validation.domain

import play.api.libs.json.{Json, OWrites, Reads}
import reactivemongo.bson.{BSONDateTime, BSONObjectID}

object SystemParameter {

  implicit val readsBsonDateTime: Reads[BSONDateTime] = Json.reads[BSONDateTime]
  implicit val writesBsonDateTime: OWrites[BSONDateTime] = Json.writes[BSONDateTime]

  implicit val readsBSONObjectID: Reads[BSONObjectID] = Json.reads[BSONObjectID]
  implicit val writesBSONObjectID: OWrites[BSONObjectID] = Json.writes[BSONObjectID]

  implicit val readsParameterSystemView: Reads[SystemParameterView] = Json.reads[SystemParameterView]
  implicit val writesParameterSystemView: OWrites[SystemParameterView] = Json.writes[SystemParameterView]

  case class SystemParameterView(_id: Option[BSONObjectID],
                                 name: Option[String],
                                 description: Option[String],
                                 sapCode: Option[String],
                                 startValidity: Option[BSONDateTime],
                                 endValidity: Option[BSONDateTime])

}
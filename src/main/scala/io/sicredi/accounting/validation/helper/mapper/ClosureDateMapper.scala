package io.sicredi.accounting.validation.helper.mapper

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.ClosureDateParameter.ClosureDate
import play.api.libs.json.{JsPath, JsSuccess, Json}
import scala.util.{Failure, Success, Try}

object ClosureDateMapper extends StrictLogging {

  def convertToDTO(message: String): Either[String, ClosureDate] = {
    Try(Json.parse(message)) match {
      case Success(value) =>
        Json.fromJson[ClosureDate](value) match {
          case JsSuccess(decodeResult: ClosureDate, _: JsPath) => {
            logger.info(s"Received and was able to parse the $decodeResult")
            Right(decodeResult)
          }
          case _ => {
            logger.error(s"There was an error parsing the following closure date data: $message")
            Left(s"There was an error parsing the following closure date data: $message")
          }
        }

      case Failure(_) =>
        logger.error(s"There was an error parsing the following json: $message")
        Left(s"There was an error parsing the following json: $message")
    }
  }

}

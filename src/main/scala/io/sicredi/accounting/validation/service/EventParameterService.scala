package io.sicredi.accounting.validation.service

import io.sicredi.accounting.validation.domain.EventParameter.EventParameter
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

abstract class EventParameterService {

  def getAll: Future[Seq[EventParameter]]

  def getByEventCodeAndSystem(code: String, system: String): Future[Seq[EventParameter]] = {
    getAll
      .map(events => events
        .filter(event => event.code.exists(c => c.equalsIgnoreCase(code)))
        .filter(event => event.system.exists(s => s.name.get.equalsIgnoreCase(system))))
  }

}

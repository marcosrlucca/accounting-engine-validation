package io.sicredi.accounting.validation.helper.mapper

import java.time.format.DateTimeFormatter
import java.time._
import io.sicredi.accounting.validation.domain.Raw.{Balance, BalanceRaw, Movement, MovementRaw, RawProcessFailure, RawProcessOutcome}
import io.sicredi.accounting.validation.helper.DateConversionHelper

object RawMapper {

  def mapToMovementRaw(movement: Option[Movement]): Option[MovementRaw] = {
    movement.map(m => MovementRaw(
      _id = m._id,
      _unique_index = m._unique_index,
      topicPostingTimestamp = m.topicPostingTimestamp,
      baseDate = DateConversionHelper.parseToZonedDateTime(m.baseDate).map(d => d.toString),
      originalEventBaseDateForReversal = DateConversionHelper.parseToZonedDateTime(m.originalEventBaseDateForReversal).map(d => d.toString),
      operationDate = DateConversionHelper.parseToZonedDateTime(m.operationDate).map(d => d.toString),
      document = m.document,
      company = m.company,
      reversal = m.reversal,
      dateReversalOcurred = DateConversionHelper.parseToZonedDateTime(m.dateReversalOcurred).map(d => d.toString),
      accountingReferenceCode = m.accountingReferenceCode,
      productReferenceCode = m.productReferenceCode,
      transactionDetail = m.transactionDetail,
      transactionId = m.transactionId,
      system = m.system,
      creditUnit = m.creditUnit,
      debitUnit = m.debitUnit,
      rules = m.rules,
      values = m.values
    ))
  }

  def mapToMovement(raw: Option[MovementRaw]): Option[Movement] = {
    raw.map(r => Movement(
      _id = r._id,
      _unique_index = r._unique_index,
      topicPostingTimestamp = r.topicPostingTimestamp,
      baseDate = r.baseDate.map(b => ZonedDateTime.parse(b)).flatMap(z => DateConversionHelper.parseToBSONDateTime(Some(z))),
      originalEventBaseDateForReversal = r.originalEventBaseDateForReversal.map(b => ZonedDateTime.parse(b)).flatMap(z => DateConversionHelper.parseToBSONDateTime(Some(z))),
      operationDate = r.operationDate.map(b => ZonedDateTime.parse(b)).flatMap(z => DateConversionHelper.parseToBSONDateTime(Some(z))),
      dateReversalOcurred = r.dateReversalOcurred.map(b => ZonedDateTime.parse(b)).flatMap(z => DateConversionHelper.parseToBSONDateTime(Some(z))),
      document = r.document,
      company = r.company,
      reversal = r.reversal,
      accountingReferenceCode = r.accountingReferenceCode,
      productReferenceCode = r.productReferenceCode,
      transactionDetail = r.transactionDetail,
      transactionId = r.transactionId,
      system = r.system,
      creditUnit = r.creditUnit,
      debitUnit = r.debitUnit,
      rules = r.rules,
      values = r.values
    ))
  }

  def mapToBalanceRaw(balance: Option[Balance]): Option[BalanceRaw] = {
    balance.map(b => BalanceRaw(
      _id = b._id,
      _unique_index = b._unique_index,
      topicPostingTimestamp = b.topicPostingTimestamp,
      baseDate = DateConversionHelper.parseToZonedDateTime(b.baseDate).map(d => d.toString),
      operationDate = DateConversionHelper.parseToZonedDateTime(b.operationDate).map(d => d.toString),
      document = b.document,
      company = b.company,
      accountingReferenceCode = b.accountingReferenceCode,
      productReferenceCode = b.productReferenceCode,
      transactionDetail = b.transactionDetail,
      transactionId = b.transactionId,
      system = b.system,
      unit = b.unit,
      rules = b.rules,
      value = b.value
    ))
  }

  def mapToBalance(raw: Option[BalanceRaw]): Option[Balance] = {
    raw.map(r => Balance(
      _id = r._id,
      _unique_index = r._unique_index,
      topicPostingTimestamp = r.topicPostingTimestamp,
      baseDate = r.baseDate.map(b => ZonedDateTime.parse(b)).flatMap(z => DateConversionHelper.parseToBSONDateTime(Some(z))),
      operationDate = r.operationDate.map(o => ZonedDateTime.parse(o)).flatMap(z => DateConversionHelper.parseToBSONDateTime(Some(z))),
      document = r.document,
      company = r.company,
      accountingReferenceCode = r.accountingReferenceCode,
      productReferenceCode = r.productReferenceCode,
      transactionDetail = r.transactionDetail,
      transactionId = r.transactionId,
      system = r.system,
      unit = r.unit,
      rules = r.rules,
      value = r.value
    ))
  }

  def mapToRawProcessOutcome(rawProcessFailure: Option[RawProcessFailure]): Option[RawProcessOutcome] = {
    rawProcessFailure.flatMap(r => r.failuredRaw
      .flatMap(raw => raw.topicPostingTimestamp.map(t => {
        val topicPostingDate = Some(OffsetDateTime.ofInstant(Instant.ofEpochMilli(t), ZoneId.of("America/Sao_Paulo")).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))

        RawProcessOutcome(
          system = raw.system,
          transactionId = raw.transactionId,
          transactionDetail = raw.transactionDetail,
          topicPostingTimestamp = topicPostingDate,
          errorDescription = r.rawProcessError,
          payload = r.failuredPayload
        )
      })
      ))
  }

}

package io.sicredi.accounting.validation.configuration

import com.typesafe.config.{ConfigFactory, Config => TypesafeConfig}
import com.typesafe.scalalogging.StrictLogging
import io.sicredi.services.platform.client.core.{Protocol, ServerConfiguration}
import io.sicredi.services.platform.consul.client.{ConsulKVClientFactory, KVFetchRequest}
import okhttp3.OkHttpClient
import scala.util.{Failure, Success}

case class ConsulProperties(okHttpClient: OkHttpClient, name: String, aclToken: String, url: String, port: Int)

case class Consul(okHttpClient: OkHttpClient) extends StrictLogging {

  val storedConfigs: TypesafeConfig = fetch()

  private def fetch(): TypesafeConfig = {

    logger.info("Configuration fetch started...")

    val bootstrapConf = loadBootstrapConf()

    val bootstrapConsulConf = bootstrapConf.getConfig("consul")

    val consulConf = fetchConsulConf(
      ConsulProperties(
        okHttpClient = okHttpClient,
        name = bootstrapConsulConf.getString("name"),
        aclToken = bootstrapConsulConf.getString("acl-token"),
        url = bootstrapConsulConf.getString("url"),
        port = bootstrapConsulConf.getInt("port")
      )
    )

    val conf = consulConf.withFallback(bootstrapConf).resolve()

    logger.info("Configuration successfully fetched!")

    conf
  }

  // query consul http api to fetch application configuration with retry of 3
  private def fetchConsulConf(prop: ConsulProperties): TypesafeConfig = {
    logger.info("Querying consul at with configured uri")

    val serverConfiguration = new ServerConfiguration(Protocol.HTTP, prop.url.replaceAll("http://", ""), prop.port)
    val consulClient = ConsulKVClientFactory.createConsulKVClient(serverConfiguration, prop.okHttpClient)
    logger.info(s"Configured parameters [port]: ${prop.port}, [url]: ${prop.url}, [application-name]: ${prop.name}")

    retry(consulClient.get(new KVFetchRequest(prop.name, prop.aclToken)), 3) match {
      case Success(a) => ConfigFactory.parseString(a)
      case Failure(f) =>
        logger.error(f.getMessage)
        throw new RuntimeException("Unable to fetch application configuration from consul!")
    }
  }

}
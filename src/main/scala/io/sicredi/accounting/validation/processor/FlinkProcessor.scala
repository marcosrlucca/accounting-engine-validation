package io.sicredi.accounting.validation.processor

import java.util.concurrent.TimeUnit
import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.client.ClosureDateAPI
import io.sicredi.accounting.validation.validation._
import io.sicredi.accounting.validation.configuration.{Kafka, Mongo}
import io.sicredi.accounting.validation.domain.Raw._
import io.sicredi.accounting.validation.helper.MD5Helper
import io.sicredi.accounting.validation.helper.mapper.ProcessorMapper._
import io.sicredi.accounting.validation.helper.mapper.ClosureDateMapper
import io.sicredi.accounting.validation.repository._
import io.sicredi.accounting.validation.service._
import io.sicredi.accounting.validation.sink.ClosureDateSink
import org.apache.flink.api.common.JobExecutionResult
import org.apache.flink.streaming.api.scala._

object FlinkProcessor extends StrictLogging {

  private lazy val splitter: Splitter = new Splitter

  def process(kafka: Kafka, mongo: Mongo, closureDateAPI: ClosureDateAPI): JobExecutionResult = {
    lazy val balanceEventParameterRepository = new BalanceEventParameterRepository(mongo)
    lazy val companyParameterRepository = new CompanyParameterRepository(mongo)
    lazy val closureDateRepository = new ClosureDateRepository(mongo)
    lazy val incomingBalanceRepository = new IncomingBalanceRepository(mongo)
    lazy val incomingMovementRepository = new IncomingMovementRepository(mongo)
    lazy val movementEventParameterRepository = new MovementEventParameterRepository(mongo)

    lazy val incomingMovementRawService = new IncomingMovementRawService(incomingMovementRepository)
    lazy val incomingBalanceRawService = new IncomingBalanceRawService(incomingBalanceRepository)
    lazy val balanceCacheService = new BalanceCacheService
    lazy val movementCacheService = new MovementCacheService
    lazy val movementEventParameterService = new MovementEventParameterService(movementEventParameterRepository)
    lazy val companyParameterService = new CompanyParameterService(companyParameterRepository)
    lazy val balanceEventParameterService = new BalanceEventParameterService(balanceEventParameterRepository)
    lazy val closureDateService = new ClosureDateService(closureDateRepository, closureDateAPI)

    lazy val movementValidator = new MovementValidator(movementEventParameterService)
    lazy val balanceValidator = new BalanceValidator(balanceEventParameterService)
    lazy val companyValidator = new CompanyValidator(companyParameterService)
    lazy val closureDateValidator = new ClosureDateValidator(closureDateService)

    lazy val closureDateSink = new ClosureDateSink(closureDateService, closureDateRepository, closureDateAPI)

    logger.debug("starting closure date consumer from kafka")

    val environment = kafka.getEnvironment

    processClosureDate(kafka, closureDateSink, environment)

    logger.debug("starting engine events consumer from kafka")

    processIncomingEvents(kafka, environment, balanceCacheService, movementCacheService, incomingMovementRawService, incomingBalanceRawService, movementValidator, balanceValidator, companyValidator, closureDateValidator)

    environment.execute(ACCOUNTING_ENGINE_VALIDATION)
  }

  private def processClosureDate(kafka: Kafka, closureDateSink: => ClosureDateSink, environment: StreamExecutionEnvironment) = {
    environment.addSource(kafka.getClosureDateConsumer)
      .filter(message => filterNotEmpty(message))
      .name(FILTER_CLOSURE_DATE_CONSUMER_NON_EMPTY)
      .uid(FILTER_CLOSURE_DATE_CONSUMER_NON_EMPTY)
      .map(message => ClosureDateMapper.convertToDTO(message))
      .name(DECODE_CLOSURE_DATE_CONSUMER_FROM_RAW)
      .uid(DECODE_CLOSURE_DATE_CONSUMER_FROM_RAW)
      .addSink(closureDateSink)
  }

  private def processIncomingEvents(kafka: Kafka, environment: StreamExecutionEnvironment, balanceCacheService: => BalanceCacheService, movementCacheService: => MovementCacheService, incomingMovementRawService: => IncomingMovementRawService, incomingBalanceRawService: => IncomingBalanceRawService, movementValidator: => MovementValidator, balanceValidator: => BalanceValidator, companyValidator: => CompanyValidator, closureDateValidator: => ClosureDateValidator) = {
    val decodingStream = getDecodingStream(kafka, environment)

    val mandatoryValidationStream = getMandatoryValidationStream(decodingStream)

    val mandatoryValidationRightStream = getMandatoryValidationRightStream(mandatoryValidationStream)

    val closureDateValidationStream = getClosureDateValidationStream(closureDateValidator, mandatoryValidationRightStream)

    val closureDateValidationRightStream = getClosureDateValidationRightStream(closureDateValidationStream)

    val companyValidationStream = getCompanyValidationStream(companyValidator, closureDateValidationRightStream)

    val movementOrBalanceStream = getMovementOrBalanceStream(companyValidationStream)

    val movementStream = getMovementStream(movementOrBalanceStream)

    val balanceStream = getBalanceStream(movementOrBalanceStream)

    val movementValidationStream = getMovementValidationStream(movementValidator, movementStream)

    val balanceValidationStream = getBalanceValidationStream(balanceValidator, balanceStream)

    val balanceValidationCacheProcessingStream = getBalanceValidationCacheProcessingStream(balanceCacheService, balanceValidationStream)

    val movementValidationCacheProcessingStream = getMovementValidationCacheProcessingStream(movementCacheService, movementValidationStream)

    val incomingBalanceRawStream = getIncomingBalanceRawStream(incomingBalanceRawService, balanceValidationCacheProcessingStream)

    val incomingMovementRawStream = getIncomingMovementRawStream(incomingMovementRawService, movementValidationCacheProcessingStream)

    val incomingBalanceRawRightStream = getIncomingBalanceRawRightStream(incomingBalanceRawStream)

    val incomingMovementRawRightStream = getIncomingMovementRawRightStream(incomingMovementRawStream)

    val failuredUnionStream = unionFailure(decodingStream, mandatoryValidationStream, closureDateValidationStream, companyValidationStream, movementValidationStream, balanceValidationStream, incomingBalanceRawStream, incomingMovementRawStream)

    val successUnionStream = unionSuccess(incomingBalanceRawRightStream, incomingMovementRawRightStream)

    val errorWithMD5Stream = createMD5ForError(failuredUnionStream)

    producesToOutcome(kafka, failuredUnionStream)

    producesToError(kafka, errorWithMD5Stream)

    producesSuccessToProcessing(kafka, successUnionStream)

    producesFailureToProcessing(kafka, errorWithMD5Stream)

    tracesErrorLogging(kafka, errorWithMD5Stream)

    tracesSuccessLogging(kafka, successUnionStream)
  }

  private def createMD5ForError(failuredUnionStream: DataStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    failuredUnionStream
      .map(process => {
        logger.debug("creating hash id for failure")
        process.left.get.get.uniqueEventIdDB = MD5Helper.createHashId(process.left.get.get)
        process
      })
      .name(FAILURE_CREATE_UNIQUE_INDEX)
      .uid(FAILURE_CREATE_UNIQUE_INDEX)
  }

  private def producesSuccessToProcessing(kafka: Kafka, successUnionStream: DataStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    successUnionStream
      .map(rawProcessed => normalizeSuccessOrFailureRawProcess(rawProcessed))
      .print()
//      .addSink(kafka.getProducerToProcessing)
//      .name(SINK_SUCCESS)
//      .uid(SINK_SUCCESS)
  }

  private def unionSuccess(incomingBalanceRawRightStream: DataStream[Either[Some[RawProcessFailure], Some[RawProcess]]], incomingMovementRawRightStream: DataStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    incomingBalanceRawRightStream
      .union(incomingMovementRawRightStream)
  }

  private def producesToError(kafka: Kafka, failuredUnionStream: DataStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    failuredUnionStream
      .map(rawProcessed => normalizeSuccessOrFailureRawProcess(rawProcessed))
      .name(NORMALIZE_FAILURE)
      .uid(NORMALIZE_FAILURE)
      .printToErr()
//      .addSink(kafka.getProducerToError)
//      .name(SINK_ERROR)
//      .uid(SINK_ERROR)
  }

  private def producesToOutcome(kafka: Kafka, failuredUnionStream: DataStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    failuredUnionStream
      .map(rawProcessed => normalizeOutcomeRawProcess(rawProcessed))
      .name(NORMALIZE_OUTCOME)
      .uid(NORMALIZE_OUTCOME)
      .addSink(kafka.getProducerToOutcome)
      .name(SINK_OUTCOME)
      .uid(SINK_OUTCOME)
  }

  private def tracesSuccessLogging(kafka: Kafka, successUnionStream: DataStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    successUnionStream
      .map(rawProcessed => normalizeSuccessOrFailureTraceLogging(rawProcessed))
      .addSink(kafka.getProducerToProcessing)
      .name(SINK_SUCCESS_TRACE_LOGGING)
      .uid(SINK_SUCCESS_TRACE_LOGGING)
  }

  private def producesFailureToProcessing(kafka: Kafka, failuredUnionStream: DataStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    failuredUnionStream
      .map(rawProcessed => normalizeFailureForProcessing(rawProcessed.left.get.get))
      .name(NORMALIZE_FAILURE_PROCESSING)
      .uid(NORMALIZE_FAILURE_PROCESSING)
      .addSink(kafka.getProducerToProcessing)
      .name(SINK_ERROR_FAILURE_PROCESSING)
      .uid(SINK_ERROR_FAILURE_PROCESSING)
  }

  private def tracesErrorLogging(kafka: Kafka, failuredUnionStream: DataStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    failuredUnionStream
      .map(rawProcessed => normalizeSuccessOrFailureTraceLogging(rawProcessed))
      .name(NORMALIZE_FAILURE_TRACE_LOGGING)
      .uid(NORMALIZE_FAILURE_TRACE_LOGGING)
      .addSink(kafka.getProducerToProcessing)
      .name(SINK_ERROR_TRACE_LOGGING)
      .uid(SINK_ERROR_TRACE_LOGGING)
  }

  private def unionFailure(decodingStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]], mandatoryValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]], closureDateValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]], companyValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]], movementValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]], balanceValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]], incomingBalanceRawStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]], incomingMovementRawStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    decodingStream
      .select(NOT_PARSED)
      .union(mandatoryValidationStream select NOT_VALIDATED)
      .union(closureDateValidationStream select NOT_VALIDATED)
      .union(companyValidationStream select NOT_VALIDATED)
      .union(movementValidationStream select NOT_VALIDATED)
      .union(balanceValidationStream select NOT_VALIDATED)
      .union(incomingBalanceRawStream select NOT_VALIDATED)
      .union(incomingMovementRawStream select NOT_VALIDATED)
  }

  private def getIncomingMovementRawRightStream(incomingMovementRawStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    incomingMovementRawStream
      .select(VALIDATED)
      .filter(incomingEvent => isRightNotEmpty(incomingEvent))
      .name(INCOMING_MOVEMENT_FILTER_RIGHT)
      .uid(INCOMING_MOVEMENT_FILTER_RIGHT)
  }

  private def getIncomingBalanceRawRightStream(incomingBalanceRawStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    incomingBalanceRawStream
      .select(VALIDATED)
      .filter(incomingEvent => isRightNotEmpty(incomingEvent))
      .name(INCOMING_BALANCE_FILTER_RIGHT)
      .uid(INCOMING_BALANCE_FILTER_RIGHT)
  }

  private def getIncomingMovementRawStream(incomingMovementRawService: => IncomingMovementRawService, movementValidationCacheProcessingStream: DataStream[(Boolean, Boolean, Option[String], RawProcess)]) = {
    AsyncDataStream.unorderedWait(movementValidationCacheProcessingStream, incomingMovementRawService, GlobalTimeOutAsyncProcessor, TimeUnit.SECONDS, AsyncCapacity)
      .split(incomingEvent => splitter.splitByResultOfValidation(incomingEvent))
  }

  private def getIncomingBalanceRawStream(incomingBalanceRawService: => IncomingBalanceRawService, balanceValidationCacheProcessingStream: DataStream[(Boolean, Option[String], RawProcess)]) = {
    AsyncDataStream.unorderedWait(balanceValidationCacheProcessingStream, incomingBalanceRawService, GlobalTimeOutAsyncProcessor, TimeUnit.SECONDS, AsyncCapacity)
      .split(incomingEvent => splitter.splitByResultOfValidation(incomingEvent))
  }

  private def getMovementValidationCacheProcessingStream(movementCacheService: => MovementCacheService, movementValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    movementValidationStream
      .select(VALIDATED)
      .filter(incomingEvent => isRightNotEmpty(incomingEvent))
      .name(MOVEMENT_VALIDATION_FILTER_RIGHT)
      .uid(MOVEMENT_VALIDATION_FILTER_RIGHT)
      .map(parsedData => parsedData.right.get.get)
      .name(MOVEMENT_VALIDATION_RIGHT)
      .uid(MOVEMENT_VALIDATION_RIGHT)
      .map(process => {
        logger.debug("creating hash id for movement")

        val uniqueEventId = MD5Helper.createHashId(process.payloadParsed.get.asInstanceOf[MovementRaw])
        process.payloadParsed.get._unique_index = uniqueEventId
        process.uniqueEventIdDB = uniqueEventId

        process
      })
      .name(MOVEMENT_CREATE_UNIQUE_INDEX)
      .uid(MOVEMENT_CREATE_UNIQUE_INDEX)
      .map(process => movementCacheService.process(process))
      .name(MOVEMENT_CACHE_SERVICE)
      .uid(MOVEMENT_CACHE_SERVICE)
  }

  private def getBalanceValidationCacheProcessingStream(balanceCacheService: => BalanceCacheService, balanceValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    balanceValidationStream
      .select(VALIDATED)
      .filter(incomingEvent => isRightNotEmpty(incomingEvent))
      .name(BALANCE_VALIDATION_FILTER_RIGHT)
      .uid(BALANCE_VALIDATION_FILTER_RIGHT)
      .map(parsedData => parsedData.right.get.get)
      .name(BALANCE_VALIDATION_RIGHT)
      .uid(BALANCE_VALIDATION_RIGHT)
      .map(process => {
        logger.debug("creating hash id for balance")

        val uniqueEventId = MD5Helper.createHashId(process.payloadParsed.get.asInstanceOf[BalanceRaw])
        process.payloadParsed.get._unique_index = uniqueEventId
        process.uniqueEventIdDB = uniqueEventId
        process
      })
      .name(BALANCE_CREATE_UNIQUE_INDEX)
      .uid(BALANCE_CREATE_UNIQUE_INDEX)
      .map(process => balanceCacheService.process(process))
      .name(BALANCE_CACHE_SERVICE)
      .uid(BALANCE_CACHE_SERVICE)
  }

  private def getBalanceValidationStream(balanceValidator: => BalanceValidator, balanceStream: DataStream[RawProcess]) = {
    AsyncDataStream.unorderedWait(balanceStream, balanceValidator, GlobalTimeOutAsyncProcessor, TimeUnit.SECONDS, AsyncCapacity)
      .split(incomingEvent => splitter.splitByResultOfValidation(incomingEvent))
  }

  private def getMovementValidationStream(movementValidator: => MovementValidator, movementStream: DataStream[RawProcess]) = {
    AsyncDataStream.unorderedWait(movementStream, movementValidator, GlobalTimeOutAsyncProcessor, TimeUnit.SECONDS, AsyncCapacity)
      .split(incomingEvent => splitter.splitByResultOfValidation(incomingEvent))
  }

  private def getBalanceStream(movementOrBalanceStream: SplitStream[RawProcess]) = {
    movementOrBalanceStream
      .select(BALANCE)
  }

  private def getMovementStream(movementOrBalanceStream: SplitStream[RawProcess]) = {
    movementOrBalanceStream
      .select(MOVEMENT)
  }

  private def getMovementOrBalanceStream(companyValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    companyValidationStream
      .select(VALIDATED)
      .filter(incomingEvent => isRightNotEmpty(incomingEvent))
      .name(COMPANY_VALIDATION_FILTER_RIGHT)
      .uid(COMPANY_VALIDATION_FILTER_RIGHT)
      .map(incomingEvent => incomingEvent.right.get.get)
      .name(COMPANY_VALIDATION_RIGHT)
      .uid(COMPANY_VALIDATION_RIGHT)
      .split(eventValidated => splitter.splitByMovementOrBalance(eventValidated))
  }

  private def getCompanyValidationStream(companyValidator: => CompanyValidator, closureDateValidationRightStream: DataStream[RawProcess]) = {
    AsyncDataStream.unorderedWait(closureDateValidationRightStream, companyValidator, GlobalTimeOutAsyncProcessor, TimeUnit.SECONDS, AsyncCapacity)
      .split(incomingEvent => splitter.splitByResultOfValidation(incomingEvent))
  }

  private def getClosureDateValidationStream(closureDateValidator: => ClosureDateValidator, mandatoryValidationRightStream: DataStream[RawProcess]) = {
    AsyncDataStream.unorderedWait(mandatoryValidationRightStream, closureDateValidator, GlobalTimeOutAsyncProcessor, TimeUnit.SECONDS, AsyncCapacity)
      .split(incomingEvent => splitter.splitByResultOfValidation(incomingEvent))
  }

  private def getClosureDateValidationRightStream(closureDateValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    closureDateValidationStream
      .select(VALIDATED)
      .filter(incomingEvent => isRightNotEmpty(incomingEvent))
      .name(CLOSURE_DATE_FILTER_RIGHT)
      .uid(CLOSURE_DATE_FILTER_RIGHT)
      .map(incomingEvent => incomingEvent.right.get.get)
      .name(CLOSURE_DATE_RIGHT)
      .uid(CLOSURE_DATE_RIGHT)
  }

  private def getMandatoryValidationRightStream(mandatoryValidationStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    mandatoryValidationStream
      .select(VALIDATED)
      .filter(incomingEvent => isRightNotEmpty(incomingEvent))
      .name(MANDATORY_VALIDATION_FILTER_RIGHT)
      .uid(MANDATORY_VALIDATION_FILTER_RIGHT)
      .map(incomingEvent => incomingEvent.right.get.get)
      .name(MANDATORY_VALIDATION_RIGHT)
      .uid(MANDATORY_VALIDATION_RIGHT)
  }

  private def getMandatoryValidationStream(decodingStream: SplitStream[Either[Some[RawProcessFailure], Some[RawProcess]]]) = {
    decodingStream
      .select(PARSED)
      .filter(parsedData => isRightNotEmpty(parsedData))
      .name(DECODING_STREAM_FILTER_RIGHT)
      .uid(DECODING_STREAM_FILTER_RIGHT)
      .map(parsedData => parsedData.right.get.get)
      .name(DECODING_STREAM_RIGHT)
      .uid(DECODING_STREAM_RIGHT)
      .map(parsedEvent => MandatoryFieldsValidator.validateSizeAndTypeAndMandatory(parsedEvent))
      .name(VALIDATE_SIZE)
      .uid(VALIDATE_SIZE)
      .split(incomingEvent => splitter.splitByResultOfValidation(incomingEvent))
  }

  private def getDecodingStream(kafka: Kafka, environment: StreamExecutionEnvironment) = {
    environment.addSource(kafka.getConsumer)
      .filter(event => filterNotEmpty(event))
      .name(FILTER_NON_EMPTY)
      .uid(FILTER_NON_EMPTY)
      .map(payload => decodeFromRaw(payload))
      .name(DECODE_FROM_RAW)
      .uid(DECODE_FROM_RAW)
      .split(d => splitter.splitByResultOfParsing(d))
  }

  private def filterNotEmpty(event: Event) = {
    event != null && event.timestamp != 0 && event.message != null && !event.message.trim.isEmpty
  }

  private def filterNotEmpty(message: String) = {
    message != null && message.trim != null && message.trim.length > 0
  }

  private def isRightNotEmpty(parsedData: Either[Some[RawProcessFailure], Some[RawProcess]]) = {
    parsedData.isRight && parsedData.right.get.isDefined && parsedData.right.get.get.payloadParsed.isDefined
  }

  private def filterPayload(event: Event): Boolean = {
    logger.debug("filtering payload")

    val message = event.message

    val validated = message.contains(""""baseDate":""") &&
      message.contains(""""company":""") &&
      (message.contains(""""accountingReferenceCode":""") || message.contains(""""productReferenceCode":""")) &&
      message.contains(""""transactionId":""") &&
      message.contains(""""system":""") &&
      message.contains(""""rules":""")

    if (!validated)
      logger.info("The following payload is not valid to parse: " + message)

    validated
  }

}

package io.sicredi.accounting.validation

package object processor {

  val GlobalTimeOutAsyncProcessor = 10
  val AsyncCapacity = 2

  val PARSED = "Parsed"
  val NOT_PARSED = "NotParsed"

  val VALIDATED = "Validated"
  val NOT_VALIDATED = "NotValidated"

  val SUCCESS = "Success"
  val FAILURE = "Failure"

  val MOVEMENT = "Movement"
  val BALANCE = "Balance"

  val FILTER_CLOSURE_DATE_CONSUMER_NON_EMPTY = "filter-closure-date-consumer-non-empty"
  val DECODE_CLOSURE_DATE_CONSUMER_FROM_RAW = "decode-closure-date-consumer-from-raw"

  val FILTER_NON_EMPTY = "filter-non-empty"
  val FILTER_PAYLOAD = "filter-payload"
  val DECODE_FROM_RAW = "decode-from-raw"
  val DECODING_STREAM_FILTER_RIGHT = "decoding-stream-filter-right"
  val DECODING_STREAM_RIGHT = "decoding-stream-right"
  val VALIDATE_SIZE = "validate-size-type"
  val MANDATORY_VALIDATION_FILTER_RIGHT = "mandatory-validation-filter-right"
  val MANDATORY_VALIDATION_RIGHT = "mandatory-validation-right"
  val CLOSURE_DATE_FILTER_RIGHT = "closure-date-filter-right"
  val CLOSURE_DATE_RIGHT = "closure-date-right"
  val COMPANY_VALIDATION = "company-validation"
  val COMPANY_VALIDATION_FILTER_RIGHT = "company-validation-filter-right"
  val COMPANY_VALIDATION_RIGHT = "company-validation-right"
  val BALANCE_VALIDATION_FILTER_RIGHT = "balance-validation-filter-right"
  val BALANCE_VALIDATION_RIGHT = "balance-validation-right"
  val BALANCE_CREATE_UNIQUE_INDEX = "balance-create-unique-index"
  val MOVEMENT_VALIDATION_FILTER_RIGHT = "movement-validation-filter-right"
  val MOVEMENT_VALIDATION_RIGHT = "movement-validation-right"
  val MOVEMENT_CREATE_UNIQUE_INDEX = "movement-create-unique-index"
  val FAILURE_CREATE_UNIQUE_INDEX = "failure-create-unique-index"
  val INCOMING_BALANCE_FILTER_RIGHT = "incoming-balance-filter-right"
  val INCOMING_MOVEMENT_FILTER_RIGHT = "incoming-movement-filter-right"
  val MOVEMENT_CACHE_SERVICE = "movement-cache-service"
  val BALANCE_CACHE_SERVICE = "balance-cache-service"
  val NORMALIZE_FAILURE = "normalize-failure"
  val NORMALIZE_FAILURE_TRACE_LOGGING = "normalize-failure-trace-logging"
  val NORMALIZE_FAILURE_PROCESSING = "normalize-failure-processing"
  val NORMALIZE_OUTCOME = "normalize-failure-outcome"
  val SINK_ERROR = "sink-error"
  val SINK_ERROR_TRACE_LOGGING = "sink-error-trace-logging"
  val SINK_ERROR_FAILURE_PROCESSING = "sink-error-failure-processing"
  val SINK_OUTCOME = "sink-outcome"
  val SINK_SUCCESS = "sink-success"
  val SINK_SUCCESS_TRACE_LOGGING = "sink-success-trace-logging"
  val ACCOUNTING_ENGINE_VALIDATION = "accounting-engine-validation"

}

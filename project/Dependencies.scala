import sbt._

object Dependencies {

  object Version {
    val flink = "1.6.1"
    val kafka = "1.1.0"
    val logback = "1.2.3"
    val http4s = "0.20.0"
    val slf4j = "1.7.25"
    val scalaLogging = "3.9.0"
    val scalactic = "3.0.5"
    val caffeine = "0.24.3"
    val scalamock = "4.1.0"
    val consulClient = "1.0.1"
    val reactiveMongo = "0.16.1"
    val vaultClient = "1.0.0"
    val play = "2.6.13"
  }

  lazy val all: Seq[ModuleID] = flink ++ cache ++ http4s ++ playJSON ++ logging ++ test ++ consul ++ mongo ++ vault

  private val http4s = Seq(
    "org.http4s" %% "http4s-blaze-client",
    "org.http4s" %% "http4s-dsl"
  ).map(_ % Version.http4s)

  val flink = Seq(
    "org.apache.flink" %% "flink-scala" % Version.flink % "provided",
    "org.apache.flink" %% "flink-streaming-scala" % Version.flink % "provided",
    "org.apache.flink" %% "flink-clients" % Version.flink,
    "org.apache.flink" %% "flink-connector-kafka-0.11" % Version.flink
  )

  val cache = Seq(
    "com.github.cb372" %% "scalacache-caffeine" % Version.caffeine,
    "com.github.cb372" %% "scalacache-cats-effect" % Version.caffeine
  )

  val playJSON = Seq("com.typesafe.play" %% "play-json" % Version.play)

  val logging = Seq(
    "ch.qos.logback" % "logback-core" % Version.logback % "provided",
    "ch.qos.logback" % "logback-classic" % Version.logback % "provided",
    "org.slf4j" % "log4j-over-slf4j" % Version.slf4j % "provided",
    "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  )

  val test = Seq("org.scalactic" %% "scalactic" % Version.scalactic,
    "org.scalatest" %% "scalatest" % Version.scalactic % Test,
    "org.scalamock" %% "scalamock" % Version.scalamock % Test)

  val consul = Seq("io.sicredi.services.platform" % "platform-consul-thin-client" % Version.consulClient)

  val mongo = Seq("org.reactivemongo" %% "reactivemongo" % Version.reactiveMongo)

  val vault = Seq("io.sicredi.services.platform" % "platform-vault-thin-client" % Version.vaultClient)
}

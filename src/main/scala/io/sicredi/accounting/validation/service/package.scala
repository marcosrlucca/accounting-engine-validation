package io.sicredi.accounting.validation

import scala.concurrent.duration._

package object service {

  val BALANCE = "BALANCE"
  val MOVEMENT = "MOVEMENT"

  val FLAG_ORIGINAL_EVENT = "[WaitingForTheOriginal=false]"
  val FLAG_WAITING_FOR_THE_ORIGINAL = "[WaitingForTheOriginal=true]"

  val CacheMovementRawLabel = "cacheMovementRawLabel"
  val CacheBalanceRawLabel = "cacheBalanceRawLabel"
  val AllCompaniesLabel = "allCompanies"
  val ClosureDateLabel = "closureDate"
  val MovementEventParametersLabel = "movementEventParameters"
  val BalanceEventParametersLabel = "balanceEventParameters"

  val globalCacheDuration: FiniteDuration = 15 minutes
  val cacheToCheckIfAlreadyExists: FiniteDuration = 15 seconds

  val maximumSizeCache = 500

}

package io.sicredi.accounting.validation.helper.mapper

import java.time.{Instant, ZonedDateTime}
import java.util.TimeZone
import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.Raw.{BalanceRaw, Event, MovementRaw, Raw, RawProcess, RawProcessFailure, RawProcessOutcome}
import io.sicredi.accounting.validation.domain.Trace.TraceLogging
import play.api.libs.json.{JsError, JsPath, JsSuccess, Json}
import io.sicredi.accounting.validation.processor.VALIDATED
import io.sicredi.accounting.validation.processor.NOT_VALIDATED
import io.sicredi.accounting.validation.processor.NOT_PARSED
import io.sicredi.accounting.validation.domain.APPLICATION_NAME
import io.sicredi.accounting.validation.helper.DateConversionHelper
import io.sicredi.accounting.validation.repository.{NOT_PARSED => COLLECTION_NOT_PARSED, NOT_VALIDATED => COLLECTION_NOT_VALIDATED}
import scala.util.{Failure, Success, Try}
import io.sicredi.accounting.validation.repository.getCollectionName

object ProcessorMapper extends StrictLogging {

  val MOVEMENT = "movement"
  val BALANCE = "balance"

  def decodeFromRaw(event: Event): Either[Some[RawProcessFailure], Some[RawProcess]] = {
    logger.debug("decoding from raw")

    val timestamp = getTimestamp(event)

    Try(
      if (event.message.contains("values")) {
        Json.fromJson[MovementRaw](Json.parse(event.message)) match {
          case JsSuccess(decodedResult: Raw, _: JsPath) =>
            decodedResult.topicPostingTimestamp = timestamp

            Right(Some(RawProcess(payloadParsed = Some(decodedResult), payload = Some(event.message), topicPostingTimestamp = timestamp)))

          case JsError(_) =>
            Left(Some(RawProcessFailure(rawProcessError = Some("Error on parsing JSON"), failuredRaw = None, failuredPayload = Some(event.message), topicPostingTimestamp = timestamp)))
        }
      } else {
        Json.fromJson[BalanceRaw](Json.parse(event.message)) match {
          case JsSuccess(decodedResult: Raw, _: JsPath) =>
            decodedResult.topicPostingTimestamp = timestamp

            Right(Some(RawProcess(payloadParsed = Some(decodedResult), payload = Some(event.message), topicPostingTimestamp = timestamp)))

          case JsError(_) =>
            Left(Some(RawProcessFailure(rawProcessError = Some("Error on parsing JSON"), failuredRaw = None, failuredPayload = Some(event.message), topicPostingTimestamp = timestamp)))
        }
      }
    ) match {
      case Success(result) =>
        result

      case Failure(ex) =>
        Left(Some(RawProcessFailure(rawProcessError = Some(ex.getLocalizedMessage), failuredRaw = None, failuredPayload = Some(event.message), topicPostingTimestamp = timestamp)))
    }
  }

  private def getTimestamp(event: Event): Option[Long] = {
    var timestamp: Option[Long] = None

    if (event.timestamp == 0) timestamp = None
    else timestamp = Some(event.timestamp)

    timestamp
  }

  def normalizeOutcomeRawProcess(rawProcessed: Either[Some[RawProcessFailure], Some[RawProcess]]): String = {
    logger.debug("normalizing outcome")

    val empty = ""

    if (rawProcessed.isLeft && rawProcessed.left.get.get.failuredRaw.isDefined) {
      RawMapper.mapToRawProcessOutcome(rawProcessed.left.get).map(outcome => Json.toJson[RawProcessOutcome](outcome).toString()).getOrElse(empty)
    }
    else
      empty
  }

  def normalizeSuccessOrFailureRawProcess(rawProcessed: Either[Some[RawProcessFailure], Some[RawProcess]]): String = {
    logger.debug("normalizing success or failure raw process")

    if (rawProcessed.isRight && rawProcessed.right.get.get.payloadParsed.isDefined) {
      val rawProcess = rawProcessed.right.get.get
      val value: Raw = rawProcess.payloadParsed.get
      rawProcess.collectionName = getCollectionName(value)

      Json.toJson[RawProcess](rawProcess).toString()
    } else {
      val left = rawProcessed.left.get.get

      left.collectionName = if (left.failuredRaw.isDefined) Some(COLLECTION_NOT_VALIDATED) else Some(COLLECTION_NOT_PARSED)

      Json.toJson[RawProcessFailure](left).toString()
    }
  }

  def normalizeFailureForProcessing(failure: RawProcessFailure): String = {
    logger.debug("normalizing success or failure raw process")

    failure.collectionName = if (failure.failuredRaw.isDefined) Some(COLLECTION_NOT_VALIDATED) else Some(COLLECTION_NOT_PARSED)

    Json.toJson[RawProcessFailure](failure).toString()
  }

  def normalizeSuccessOrFailureTraceLogging(rawProcessed: Either[Some[RawProcessFailure], Some[RawProcess]]): String = {
    logger.debug("normalizing success or failure raw process for trace logging")

    if (rawProcessed.isRight && rawProcessed.right.get.get.payloadParsed.isDefined)
      Json.toJson[TraceLogging](parseToTraceLogging(rawProcessed.right.get.get)).toString
    else
      Json.toJson[TraceLogging](parseToTraceLogging(rawProcessed.left.get.get)).toString
  }

  private def parseToTraceLogging(process: RawProcess): TraceLogging = {
    val parsed = process.payloadParsed.get
    val company = if (process.payloadParsed.isDefined) process.payloadParsed.get.company else None
    val system = if (process.payloadParsed.isDefined) process.payloadParsed.get.system else None
    val collectionName = getCollectionName(parsed)

    val inputTime = process.topicPostingTimestamp
      .map(_.toString)
      .map(value => value.toLong)
      .map(value => ZonedDateTime.ofInstant(Instant.ofEpochMilli(value), TimeZone.getDefault.toZoneId))
      .flatMap(value => DateConversionHelper.parseToBSONDateTime(Some(value)))

    val baseDate =
      if (process.payloadParsed.isDefined)
        process.payloadParsed.get.baseDate
          .flatMap(value =>
            Try(ZonedDateTime.parse(value)) match {
              case Success(v) => DateConversionHelper.parseToBSONDateTime(Some(v))
              case Failure(_) => None
            })
      else
        None

    val eventType = if (process.payload.exists(a => a.contains("values"))) Some(MOVEMENT) else Some(BALANCE)

    TraceLogging(
      eventType = eventType,
      company = company,
      system = system,
      baseDate = baseDate,
      collectionName = collectionName,
      uniqueIndex = parsed._unique_index,
      timestampIn = inputTime,
      timestampOut = getOutputTime,
      status = Some(VALIDATED),
      applicationName = Some(APPLICATION_NAME)
    )
  }

  private def parseToTraceLogging(failure: RawProcessFailure) = {
    val isParsed = failure.failuredRaw.isDefined
    val company = if (failure.failuredRaw.isDefined) failure.failuredRaw.get.company else None
    val system = if (failure.failuredRaw.isDefined) failure.failuredRaw.get.system else None

    val baseDate =
      if (failure.failuredRaw.isDefined)
        failure.failuredRaw.get.baseDate
          .flatMap(value =>
            Try(ZonedDateTime.parse(value)) match {
              case Success(v) => DateConversionHelper.parseToBSONDateTime(Some(v))
              case Failure(_) => None
            })
      else
        None

    val inputTime = failure.topicPostingTimestamp
      .map(_.toString)
      .map(value => value.toLong)
      .map(value => ZonedDateTime.ofInstant(Instant.ofEpochMilli(value), TimeZone.getDefault.toZoneId))
      .flatMap(value => DateConversionHelper.parseToBSONDateTime(Some(value)))

    val eventType = if (failure.failuredPayload.exists(a => a.contains("values"))) Some(MOVEMENT) else Some(BALANCE)

    TraceLogging(
      eventType = eventType,
      company = company,
      system = system,
      baseDate = baseDate,
      collectionName = if (isParsed) Some(COLLECTION_NOT_VALIDATED) else Some(COLLECTION_NOT_PARSED),
      uniqueIndex = failure.uniqueEventIdDB,
      timestampIn = inputTime,
      timestampOut = getOutputTime,
      status = if (isParsed) Some(NOT_VALIDATED) else Some(NOT_PARSED),
      applicationName = Some(APPLICATION_NAME)
    )
  }

  private def getOutputTime = {
    DateConversionHelper.parseToBSONDateTime(Some(ZonedDateTime.now()))
  }

}

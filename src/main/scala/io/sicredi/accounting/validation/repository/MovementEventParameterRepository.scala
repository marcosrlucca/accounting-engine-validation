package io.sicredi.accounting.validation.repository

import io.sicredi.accounting.validation.configuration.Mongo
import io.sicredi.accounting.validation.domain.EventParameter.MovementEventParameter
import reactivemongo.api.Cursor
import reactivemongo.bson.document
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class MovementEventParameterRepository(mongoConfiguration: Mongo) extends EventParameterRepository with Serializable {

  override def findAll(): Future[Seq[MovementEventParameter]] = {

    val query = document()

    Try(mongoConfiguration.getMovementEventParameterCollection.flatMap(_.find(query)
      .cursor[MovementEventParameter]()
      .collect[Seq](-1, Cursor.FailOnError[Seq[MovementEventParameter]]()))) match {
      case Success(rs) => rs
      case Failure(_) => Future(Seq.empty)
    }

  }

}

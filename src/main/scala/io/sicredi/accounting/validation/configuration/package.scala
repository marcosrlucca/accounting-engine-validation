package io.sicredi.accounting.validation

import com.typesafe.config.{ConfigFactory, Config => TypesafeConfig}
import scala.util.Try
import scala.util.control.{Exception => ControlException}

package object configuration {

  val KafkaServerLocalAccess = "kafka.server-local-access"
  val CONSUL_SERVER = "CONSUL_SERVER"
  val DB_USERNAME = "app_accounting"

  def loadBootstrapConf(): TypesafeConfig = ConfigFactory.load("bootstrap.conf")

  def retry[T](fn: => T, n: Int): Try[T] = {
    ControlException.catching(classOf[Exception])
      .withTry(fn)
      .recoverWith({ case _ if n > 1 => retry(fn, n - 1) })
  }

}

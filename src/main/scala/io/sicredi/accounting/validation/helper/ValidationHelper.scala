package io.sicredi.accounting.validation.helper

import java.time.ZonedDateTime
import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.{EventParameter, Raw}
import io.sicredi.accounting.validation.domain.EventParameter.{BalanceEventParameter, BalanceScript, EventParameter, MovementEventParameter, MovementScript, RuleOption}
import io.sicredi.accounting.validation.domain.Raw.{BalanceRaw, MovementRaw, Raw}
import io.sicredi.accounting.validation.validation._
import scala.collection.mutable
import scala.util.{Failure, Success, Try}

object ValidationHelper extends StrictLogging {

  def isSameOption(currentOptionFromIncomingEvent: String, option: RuleOption): Boolean = {
    option.option.get.equalsIgnoreCase(currentOptionFromIncomingEvent)
  }

  def isSameSizeRules(rulesFromIncomingEvent: Seq[String], eventPar: EventParameter): Boolean = {
    eventPar.rules.isDefined && eventPar.rules.get.size == rulesFromIncomingEvent.size
  }

  def isDateEventParameterValid(incomingRawDateString: String, eventParameter: EventParameter): Boolean = {
    Try(ZonedDateTime.parse(incomingRawDateString)) match {

      case Success(rawDate) =>
        DateConversionHelper.parseToZonedDateTime(eventParameter.startValidity) match {

          case Some(startDateParameter) =>
            DateConversionHelper.parseToZonedDateTime(eventParameter.endValidity) match {

              case Some(endDateParameter) => startDateParameter.isBefore(rawDate) && endDateParameter.isAfter(rawDate)

              case None => startDateParameter.isBefore(rawDate)
            }

          case None => logger.error("Error on parsing event parameter start validity")
            false
        }

      case Failure(exception) => logger.error(exception.getMessage)
        false
    }
  }

  def isDateSystemParameterValid(eventPar: EventParameter, incomingRaw: Raw): Boolean = {
    eventPar.system
      .exists(s => s.name
        .exists(sName => incomingRaw.system
          .exists(sRaw => sName.equalsIgnoreCase(sRaw))) && ValidationHelper.isDateSystemParameterValid(s))
  }

  def isDateSystemParameterValid(sys: EventParameter.System): Boolean = {
    DateConversionHelper.parseToZonedDateTime(sys.startValidity) match {

      case Some(startDateSysParameter) =>
        DateConversionHelper.parseToZonedDateTime(sys.endValidity) match {

          case Some(endDateSysParameter) => startDateSysParameter.isBefore(ZonedDateTime.now()) && endDateSysParameter.isAfter(ZonedDateTime.now())

          case None => startDateSysParameter.isBefore(ZonedDateTime.now())
        }

      case None => logger.error("Error on parsing event parameter start validity")
        false
    }
  }

  def isDateScriptParameterValid(scriptParameter: BalanceScript): Boolean = {
    DateConversionHelper.parseToZonedDateTime(scriptParameter.startValidity) match {

      case Some(startDateSysParameter) =>
        DateConversionHelper.parseToZonedDateTime(scriptParameter.endValidity) match {

          case Some(endDateSysParameter) => startDateSysParameter.isBefore(ZonedDateTime.now()) && endDateSysParameter.isAfter(ZonedDateTime.now())

          case None => startDateSysParameter.isBefore(ZonedDateTime.now())
        }

      case None => logger.error("Error on parsing script parameter start validity")
        false
    }
  }

  def isDateScriptParameterValid(scriptParameter: MovementScript): Boolean = {
    DateConversionHelper.parseToZonedDateTime(scriptParameter.startValidity) match {

      case Some(startDateSysParameter) =>
        DateConversionHelper.parseToZonedDateTime(scriptParameter.endValidity) match {

          case Some(endDateSysParameter) => startDateSysParameter.isBefore(ZonedDateTime.now()) && endDateSysParameter.isAfter(ZonedDateTime.now())

          case None => startDateSysParameter.isBefore(ZonedDateTime.now())
        }

      case None => logger.error("Error on parsing script parameter start validity")
        false
    }
  }

  def isOptionDateValid(option: RuleOption): Boolean = {
    DateConversionHelper.parseToZonedDateTime(option.startValidity) match {

      case Some(startDateOption) =>
        DateConversionHelper.parseToZonedDateTime(option.endValidity) match {

          case Some(endDateOption) => startDateOption.isBefore(ZonedDateTime.now()) && endDateOption.isAfter(ZonedDateTime.now())

          case None => startDateOption.isBefore(ZonedDateTime.now()) || startDateOption.isEqual(ZonedDateTime.now())
        }

      case None => logger.error("Error on parsing event parameter start validity")
        false
    }
  }

  def isRulesFromIncomingEventValid(raw: Raw, event: EventParameter): Boolean = {
    event.rules.exists(rulesFromEventPar => {
      var rulesFromIncomingEventIsValid = false

      for {
        i <- rulesFromEventPar.indices
        if rulesFromEventPar.length == raw.rules.get.length
        if (rulesFromIncomingEventIsValid && i > 0) || (i == 0)
      } {
        val currentOptionFromIncomingEvent = raw.rules.get(i)

        rulesFromIncomingEventIsValid =
          rulesFromEventPar(i).description.exists(a => a.equalsIgnoreCase(COMPANY_DESCRIPTION)) || //valida empresa somente no método isScriptParameterAndRulesFromRawValid()
            rulesFromEventPar(i).options.exists(options => options.exists(option => isSameOption(currentOptionFromIncomingEvent, option) && isOptionDateValid(option)))
      }

      rulesFromIncomingEventIsValid
    })
  }

  def isScriptBalanceAndRulesFromRawValid(eventDB: BalanceEventParameter, raw: Raw, errors: Option[mutable.Map[String, String]]): Boolean = {
    eventDB.scripts
      .filter(scriptsFromEventPar => scriptsFromEventPar
        .exists(script => script.options
          .exists(parOptions => raw.rules
            .exists(incomingRules => incomingRules.length == parOptions.length)))) //primeiro, vê se o tamanho das opções do script e do raw combinam
      .exists(scriptsFromEventPar => {

      val maybePositionCompany = findCompanyPositionInParRules(eventDB)

      //a partir daqui, verifica se o conteudo dos options e do raw combinam

      var allOptionsValidNotFound = true

      for {
        i <- scriptsFromEventPar.indices
        if allOptionsValidNotFound //se não encontrou todas as opções válidas, segue procurando
      } {
        var optionValid = true

        for {
          j <- scriptsFromEventPar(i).options.get.indices
          if optionValid //se as opções são válidas até a última posição do array, então é porque encontrou o script correto
        } {
          val currentOptionFromIncomingEvent = raw.rules.get(j)
          val currentOptionFromEventParameter = scriptsFromEventPar(i).options.get(j)

          if (maybePositionCompany.isDefined) { //deve validar empresa?
            maybePositionCompany
              .filter(positionCompany => positionCompany == j) //se a posição da empresa do campo parameter rules é a mesma posição do parameter options atual, valida os dados de empresa enviado pelo produto
              .map(positionCompany => {

              val allowedCompaniesInParRules: Seq[String] =
                eventDB.rules.flatMap(rules => rules(positionCompany).options.map(options => options.flatMap(option => option.option.map(desc => desc)))).getOrElse(Seq.empty) //lista que contem somente o codigo das empresas permitidas

              //se contém empresa para validar
              if (allowedCompaniesInParRules.nonEmpty) {
                val currentOptionScript = currentOptionFromEventParameter.option.get

                optionValid =
                  (
                    allowedCompaniesInParRules.contains(currentOptionScript) ||
                      allowedCompaniesInParRules.contains(ANY_CODE) ||
                      (allowedCompaniesInParRules.contains(ANY_AND_ONLY_COOPERATIVE) && !currentOptionScript.equalsIgnoreCase(BANK_748))
                    ) &&
                    !raw.company.get.equalsIgnoreCase(ANY_CODE) &&
                    (
                      (currentOptionScript.equalsIgnoreCase(ANY_CODE) && !currentOptionFromIncomingEvent.equalsIgnoreCase(ANY_CODE)) ||
                        (currentOptionScript.equalsIgnoreCase(ANY_AND_ONLY_COOPERATIVE) && !currentOptionFromIncomingEvent.equalsIgnoreCase(BANK_748)) ||
                        ((currentOptionScript.equalsIgnoreCase(BANK_748) || currentOptionScript.equalsIgnoreCase(BANK_0748)) && currentOptionFromIncomingEvent.equalsIgnoreCase(BANK_748))
                      ) &&
                    (
                      (currentOptionFromIncomingEvent.equalsIgnoreCase(BANK_748) && raw.company.get.equalsIgnoreCase(BANK_748)) ||
                        (currentOptionFromIncomingEvent.equalsIgnoreCase(ANY_AND_ONLY_COOPERATIVE) && !raw.company.get.equalsIgnoreCase(BANK_0748) && !raw.company.get.equalsIgnoreCase(BANK_748))
                      ) &&
                    (
                      isOptionDateValid(currentOptionFromEventParameter) &&
                        scriptsFromEventPar(i).account.isDefined &&
                        ValidationHelper.isDateScriptParameterValid(scriptsFromEventPar(i))
                      )

                if (!optionValid) errors.foreach(e => e(COMPANY_IN_RULES_NOT_VALID) = COMPANY_IN_RULES_NOT_VALID_TEXT)

                //tenta validar o script de novo caso possua zero a esquerda da empresa
                //mas, remove o zero antes de tentar enviando uma cópia do raw para validação
                if (!optionValid && (currentOptionFromIncomingEvent.startsWith(ZERO) || raw.company.get.startsWith(ZERO)))
                  optionValid = isScriptBalanceAndRulesFromRawValid(eventDB, removeZeroFromCompany(raw, positionCompany), errors)
              }

              optionValid
            })
              .getOrElse(optionValid = isOptionValidFromBalanceScript(scriptsFromEventPar, i, currentOptionFromIncomingEvent, currentOptionFromEventParameter)) //deve validar empresa, mas não é o momento de validar a empresa (j não é o indice de empresa no laço for)
          } else { //não deve validar empresa
            optionValid = isOptionValidFromBalanceScript(scriptsFromEventPar, i, currentOptionFromIncomingEvent, currentOptionFromEventParameter)
          }
        }

        //se optionValid é false, então uma das opções é inválida
        //se optionValid é true, então todas as opções são válidas
        allOptionsValidNotFound = !optionValid
      }

      !allOptionsValidNotFound

    })
  }

  def isScriptMovementAndRulesFromRawValid(eventDB: MovementEventParameter, raw: Raw, errors: Option[mutable.Map[String, String]]): Boolean = {
    eventDB.scripts
      .filter(scriptsFromEventPar => scriptsFromEventPar
        .exists(script => script.options
          .exists(parOptions => raw.rules
            .exists(incomingRules => incomingRules.length == parOptions.length)))) //primeiro, vê se o tamanho das opções do script e do raw combinam
      .exists(scriptsFromEventPar => {

      val maybePositionCompany = findCompanyPositionInParRules(eventDB)

      //a partir daqui, verifica se o conteudo dos options e do raw combinam

      var allOptionsValidNotFound = true

      for {
        i <- scriptsFromEventPar.indices
        if allOptionsValidNotFound //se não encontrou todas as opções válidas, segue procurando
      } {
        var optionValid = true

        for {
          j <- scriptsFromEventPar(i).options.get.indices
          if optionValid //se as opções são válidas até a última posição do array, então é porque encontrou o script correto
        } {
          val currentOptionFromIncomingEvent = raw.rules.get(j)
          val currentOptionFromEventParameter = scriptsFromEventPar(i).options.get(j)

          if (maybePositionCompany.isDefined) { //deve validar empresa?
            maybePositionCompany
              .filter(positionCompany => positionCompany == j) //se a posição da empresa do campo parameter rules é a mesma posição do parameter options atual, valida os dados de empresa enviado pelo produto
              .map(positionCompany => {

              val allowedCompaniesInParRules: Seq[String] =
                eventDB.rules.flatMap(rules => rules(positionCompany).options.map(options => options.flatMap(option => option.option.map(desc => desc)))).getOrElse(Seq.empty) //lista que contem somente o codigo das empresas permitidas

              //se contém empresa para validar
              if (allowedCompaniesInParRules.nonEmpty) {
                val currentOptionScript = currentOptionFromEventParameter.option.get

                optionValid =
                  (
                    allowedCompaniesInParRules.contains(currentOptionScript) ||
                      allowedCompaniesInParRules.contains(ANY_CODE) ||
                      (allowedCompaniesInParRules.contains(ANY_AND_ONLY_COOPERATIVE) && !currentOptionScript.equalsIgnoreCase(BANK_748))
                    ) &&
                    !raw.company.get.equalsIgnoreCase(ANY_CODE) &&
                    (
                      (currentOptionScript.equalsIgnoreCase(ANY_CODE) && !currentOptionFromIncomingEvent.equalsIgnoreCase(ANY_CODE)) ||
                        (currentOptionScript.equalsIgnoreCase(ANY_AND_ONLY_COOPERATIVE) && !currentOptionFromIncomingEvent.equalsIgnoreCase(BANK_748)) ||
                        ((currentOptionScript.equalsIgnoreCase(BANK_748) || currentOptionScript.equalsIgnoreCase(BANK_0748)) && currentOptionFromIncomingEvent.equalsIgnoreCase(BANK_748))
                      ) &&
                    (
                      (currentOptionFromIncomingEvent.equalsIgnoreCase(BANK_748) && raw.company.get.equalsIgnoreCase(BANK_748)) ||
                        (currentOptionFromIncomingEvent.equalsIgnoreCase(ANY_AND_ONLY_COOPERATIVE) && !raw.company.get.equalsIgnoreCase(BANK_0748) && !raw.company.get.equalsIgnoreCase(BANK_748))
                      ) &&
                    (
                      isOptionDateValid(currentOptionFromEventParameter) &&
                        scriptsFromEventPar(i).creditAccount.isDefined &&
                        scriptsFromEventPar(i).debitAccount.isDefined &&
                        ValidationHelper.isDateScriptParameterValid(scriptsFromEventPar(i))
                      )

                if (!optionValid) errors.foreach(e => e(COMPANY_IN_RULES_NOT_VALID) = COMPANY_IN_RULES_NOT_VALID_TEXT)

                //tenta validar o script de novo caso possua zero a esquerda da empresa
                //mas, remove o zero antes de tentar enviando uma cópia do raw para validação
                if (!optionValid && (currentOptionFromIncomingEvent.startsWith(ZERO) || raw.company.get.startsWith(ZERO)))
                  optionValid = isScriptMovementAndRulesFromRawValid(eventDB, removeZeroFromCompany(raw, positionCompany), errors)
              }

              optionValid
            })
              .getOrElse(optionValid = isOptionValidFromMovementScript(scriptsFromEventPar, i, currentOptionFromIncomingEvent, currentOptionFromEventParameter)) //deve validar empresa, mas não é o momento de validar a empresa (j não é o indice de empresa no laço for)
          } else { //não deve validar empresa
            optionValid = isOptionValidFromMovementScript(scriptsFromEventPar, i, currentOptionFromIncomingEvent, currentOptionFromEventParameter)
          }
        }

        //se optionValid é false, então uma das opções é inválida
        //se optionValid é true, então todas as opções são válidas
        allOptionsValidNotFound = !optionValid
      }

      !allOptionsValidNotFound

    })
  }

  private def removeZeroFromCompany(raw: Raw, indexFromCompanyInsideRules: Int): Raw = {
    val maybeCompany = raw.company.filter(_.nonEmpty).filter(a => a.trim.nonEmpty).map(a => if (a.startsWith(ZERO)) a.substring(1, a.length) else a)

    val rulesTmp = raw.rules.map(rules => {
      val rulesCopy: Array[String] = new Array[String](rules.length)

      for (i <- rules.indices) if (i == indexFromCompanyInsideRules && rules(i).startsWith(ZERO)) rulesCopy(i) = rules(i).substring(1, rules(i).length) else rulesCopy(i) = rules(i)

      if (!rulesCopy.isEmpty) rulesCopy.toSeq else Seq()
    })

    val rawTmp = raw match {
      case movementRawTmp: MovementRaw =>
        MovementRaw(
          _id = movementRawTmp._id,
          _unique_index = movementRawTmp._unique_index,
          topicPostingTimestamp = movementRawTmp.topicPostingTimestamp,
          baseDate = movementRawTmp.baseDate,
          originalEventBaseDateForReversal = movementRawTmp.originalEventBaseDateForReversal,
          operationDate = movementRawTmp.operationDate,
          document = movementRawTmp.document,
          company = maybeCompany,
          reversal = movementRawTmp.reversal,
          dateReversalOcurred = movementRawTmp.dateReversalOcurred,
          accountingReferenceCode = movementRawTmp.accountingReferenceCode,
          productReferenceCode = movementRawTmp.productReferenceCode,
          transactionId = movementRawTmp.transactionId,
          transactionDetail = movementRawTmp.transactionDetail,
          system = movementRawTmp.system,
          creditUnit = movementRawTmp.creditUnit,
          debitUnit = movementRawTmp.debitUnit,
          rules = rulesTmp,
          values = movementRawTmp.values
        )

      case balanceRawTmp: BalanceRaw =>
        BalanceRaw(
          _id = balanceRawTmp._id,
          _unique_index = balanceRawTmp._unique_index,
          topicPostingTimestamp = balanceRawTmp.topicPostingTimestamp,
          baseDate = balanceRawTmp.baseDate,
          operationDate = balanceRawTmp.operationDate,
          document = balanceRawTmp.document,
          company = maybeCompany,
          accountingReferenceCode = balanceRawTmp.accountingReferenceCode,
          productReferenceCode = balanceRawTmp.productReferenceCode,
          transactionId = balanceRawTmp.transactionId,
          transactionDetail = balanceRawTmp.transactionDetail,
          system = balanceRawTmp.system,
          unit = balanceRawTmp.unit,
          rules = rulesTmp,
          value = balanceRawTmp.value
        )
    }
    rawTmp
  }

  private def isOptionValidFromBalanceScript(scriptsFromEventPar: Seq[BalanceScript], i: Int, currentOptionFromIncomingEvent: String, currentOptionFromEventParameter: RuleOption): Boolean = {
    isSameOption(currentOptionFromIncomingEvent, currentOptionFromEventParameter) &&
      isOptionDateValid(currentOptionFromEventParameter) &&
      scriptsFromEventPar(i).account.isDefined &&
      ValidationHelper.isDateScriptParameterValid(scriptsFromEventPar(i))
  }

  private def isOptionValidFromMovementScript(scriptsFromEventPar: Seq[MovementScript], i: Int, currentOptionFromIncomingEvent: String, currentOptionFromEventParameter: RuleOption): Boolean = {
    isSameOption(currentOptionFromIncomingEvent, currentOptionFromEventParameter) &&
      isOptionDateValid(currentOptionFromEventParameter) &&
      scriptsFromEventPar(i).creditAccount.isDefined &&
      scriptsFromEventPar(i).debitAccount.isDefined &&
      ValidationHelper.isDateScriptParameterValid(scriptsFromEventPar(i))
  }

  //verifica qual posição do rules contém a empresa, para depois validar o conteúdo do raw com a empresa permitida no script
  private def findCompanyPositionInParRules(eventDB: EventParameter): Option[Int] = {
    eventDB.rules
      .flatMap(rules => {
        var maybeCompanyPosition: Option[Int] = None
        var isCompanyPosition = false

        for {
          z <- rules.indices
          if !isCompanyPosition
        } {
          isCompanyPosition = rules(z).description.exists(desc => desc.equalsIgnoreCase(COMPANY_DESCRIPTION))

          if (isCompanyPosition) maybeCompanyPosition = Some(z)
        }

        maybeCompanyPosition
      })
  }

  def isCompanyFromMovementValid(eventDB: MovementEventParameter, raw: MovementRaw): Boolean = {
    eventDB.scripts
      .exists(scripts => scripts
        .exists(script => script.options
          .exists(options => options
            .exists(option => option.option
              .exists(optionDescription => raw.company
                .exists(c =>
                  //se o roteiro contábil é para o banco sicredi, então o produto tem que enviar 748 no campo empresa do payload
                  //se não, não precisa validar, pois vai ser sempre alguma cooperativa
                  if (optionDescription.equalsIgnoreCase(BANK_748) || optionDescription.equalsIgnoreCase(BANK_0748))
                    c.equalsIgnoreCase(optionDescription)
                  else
                    true
                ))))))
  }

  def isCompanyFromBalanceValid(eventDB: BalanceEventParameter, raw: BalanceRaw): Boolean = {
    eventDB.scripts
      .exists(scripts => scripts
        .exists(script => script.options
          .exists(options => options
            .exists(option => option.option
              .exists(optionDescription => raw.company
                .exists(c =>
                  //se o roteiro contábil é para o banco sicredi, então o produto tem que enviar 748 no campo empresa do payload
                  //se não, não precisa validar, pois vai ser sempre alguma cooperativa
                  if (optionDescription.equalsIgnoreCase(BANK_748) || optionDescription.equalsIgnoreCase(BANK_0748))
                    c.equalsIgnoreCase(optionDescription)
                  else
                    true
                ))))))
  }

}

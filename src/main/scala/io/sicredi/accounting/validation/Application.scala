package io.sicredi.accounting.validation

import io.sicredi.accounting.validation.client.{ClosureDateAPI, ClosureDateProperties}
import io.sicredi.accounting.validation.configuration._
import io.sicredi.accounting.validation.processor.FlinkProcessor
import okhttp3.OkHttpClient

object Application extends App {

  lazy val okHttpClient = new OkHttpClient

  lazy val consulConfiguration = Consul(okHttpClient)

  lazy val vaultConfiguration = Vault(
    VaultProperties(
      okHttpClient = okHttpClient,
      vaultAddress = consulConfiguration.storedConfigs.getString("vaultAddress"),
      vaultPort = consulConfiguration.storedConfigs.getInt("vaultPort"),
      vaultRole = consulConfiguration.storedConfigs.getString("vaultRole"),
      vaultToken = loadBootstrapConf().getString("vault.local-access-token")))

  lazy val mongoConfiguration = Mongo(
    MongoProperties(
      servers = consulConfiguration.storedConfigs.getString("mongoNodes").split(",").toSeq,
      authDBName = consulConfiguration.storedConfigs.getString("authenticationDB"),
      password = vaultConfiguration.getSecret(
        consulConfiguration.storedConfigs.getString("vaultMongoRootPath"),
        consulConfiguration.storedConfigs.getString("vaultMongoKey")),
      dataDBName = consulConfiguration.storedConfigs.getString("dataBaseName"),
      closureDateCollection = consulConfiguration.storedConfigs.getString("closureDateCollection"),
      systemParameterCollection = consulConfiguration.storedConfigs.getString("parameterSystemCollection"),
      movementEventParameterCollection = consulConfiguration.storedConfigs.getString("movementEventParameterCollection"),
      companyParameterCollection = consulConfiguration.storedConfigs.getString("companyParameterCollection"),
      balanceEventParameterCollection = consulConfiguration.storedConfigs.getString("balanceEventParameterCollection")))

  lazy val kafkaConfiguration = Kafka(
    KafkaProperties(
      servers = consulConfiguration.storedConfigs.getString("kafkaServer"),
      topicIn = consulConfiguration.storedConfigs.getString("flinkTopicIn"),
      topicOutSuccess = consulConfiguration.storedConfigs.getString("flinkTopicOut"),
      topicOutOutcome = consulConfiguration.storedConfigs.getString("flinkOutcomeTopic"),
      topicOutFailure = consulConfiguration.storedConfigs.getString("flinkErrorTopic"),
      restartAttempts = consulConfiguration.storedConfigs.getInt("restartAttempts"),
      delayBetweenAttempts = consulConfiguration.storedConfigs.getInt("delayBetweenAttempts"),
      checkpointInterval = consulConfiguration.storedConfigs.getInt("checkpointInterval"),
      minPauseBetweenCheckpoints = consulConfiguration.storedConfigs.getInt("minPauseBetweenCheckpoints"),
      maxConcurrentCheckpoints = consulConfiguration.storedConfigs.getInt("maxConcurrentCheckpoints"),
      groupId = consulConfiguration.storedConfigs.getString("flinkGroupId"),
      userName = consulConfiguration.storedConfigs.getString("kafkaUserName"),
      password = vaultConfiguration.getSecret(
        consulConfiguration.storedConfigs.getString("vaultKafkaRootPath"),
        consulConfiguration.storedConfigs.getString("vaultKafkaKey"))
    ),
    ClosureDateKafkaProperties(
      topicIn = consulConfiguration.storedConfigs.getString("closureDateTopicIn"),
      groupId = consulConfiguration.storedConfigs.getString("closureDateGroupId")
    )
  )

  lazy val closureDateAPI = ClosureDateAPI(
    ClosureDateProperties(
      url = consulConfiguration.storedConfigs.getString("closureDateAPI")
    ))

  okHttpClient.dispatcher.executorService.shutdown()
  okHttpClient.connectionPool.evictAll()

  FlinkProcessor.process(kafkaConfiguration, mongoConfiguration, closureDateAPI)

}

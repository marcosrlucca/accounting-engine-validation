package io.sicredi.accounting.validation.service

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.Raw.{BalanceRaw, RawProcess}
import io.sicredi.accounting.validation.service.Cache.BalanceCache
import scalacache.caffeine.CaffeineCache
import scalacache.modes.sync._

class BalanceCacheService extends StrictLogging {

  private val cache: CaffeineCache[Cache.BalanceCache] = Cache.customisedBalanceRawCache

  def process(process: RawProcess): (Boolean, Option[String], RawProcess) = {
    logger.debug("starting cache")

    val incomingRaw = process.payloadParsed.get.asInstanceOf[BalanceRaw]
    val uniqueIndex = incomingRaw._unique_index.get

    //posição 1: evento encontrado na cache
    //posição 2: mensagem de erro
    //posição 3: objeto RawProcess
    var result = Tuple3[Boolean, Option[String], RawProcess](false, None, process)

    if (cache.get(uniqueIndex).isDefined) {
      //evento original existe na cache

      logger.debug("original event already exists in cache")

      result = (true, Some("Event already exists"), process)
    } else {
      //evento original não existe na cache

      logger.debug("can add at cache")

      val balanceToCache = BalanceCache(incomingRaw._unique_index, incomingRaw.baseDate)

      cache.put(uniqueIndex)(balanceToCache, Some(cacheToCheckIfAlreadyExists))

      result = (false, None, process)
    }

    result
  }

}

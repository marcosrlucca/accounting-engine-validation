package io.sicredi.accounting.validation.repository

import io.sicredi.accounting.validation.configuration.Mongo
import reactivemongo.bson.{BSONDocument, document}
import scala.collection.mutable
import scala.concurrent.Future

abstract class IncomingRawRepository[T](mongoConfiguration: Mongo) extends Serializable {

  def findOneBy(incomingRaw: T): Future[Option[T]]

  def findAllBy(incomingRaw: T): Future[Option[mutable.Map[String, T]]]

  protected def getClauseWhere(uniqueIndex: String): BSONDocument = {
    document(
      "_unique_index" -> uniqueIndex,
      "_engine_error" -> BSONDocument("$exists" -> false)
    )
  }

}

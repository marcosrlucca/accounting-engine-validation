package io.sicredi.accounting.validation.domain

import play.api.libs.json._
import reactivemongo.bson.BSONDateTime

object EventParameter {

  implicit val readsBsonDateTime: Reads[BSONDateTime] = Json.reads[BSONDateTime]
  implicit val writesBsonDateTime: OWrites[BSONDateTime] = Json.writes[BSONDateTime]

  implicit val readsSystem: Reads[System] = Json.reads[System]
  implicit val writesSystem: OWrites[System] = Json.writes[System]

  implicit val readsValue: Reads[Value] = Json.reads[Value]
  implicit val writesValue: OWrites[Value] = Json.writes[Value]

  implicit val readsRuleOption: Reads[RuleOption] = Json.reads[RuleOption]
  implicit val writesRuleOption: OWrites[RuleOption] = Json.writes[RuleOption]

  implicit val readsRule: Reads[Rule] = Json.reads[Rule]
  implicit val writesRule: OWrites[Rule] = Json.writes[Rule]

  implicit val readsMovementScript: Reads[MovementScript] = Json.reads[MovementScript]
  implicit val writesMovementScript: OWrites[MovementScript] = Json.writes[MovementScript]

  implicit val readsBalanceScript: Reads[BalanceScript] = Json.reads[BalanceScript]
  implicit val writesBalanceScript: OWrites[BalanceScript] = Json.writes[BalanceScript]

  implicit val readsMovementEventParameter: Reads[MovementEventParameter] = Json.reads[MovementEventParameter]
  implicit val writesMovementEventParameter: OWrites[MovementEventParameter] = Json.writes[MovementEventParameter]

  implicit val readsBalanceEventParameter: Reads[BalanceEventParameter] = Json.reads[BalanceEventParameter]
  implicit val writesBalanceEventParameter: OWrites[BalanceEventParameter] = Json.writes[BalanceEventParameter]

  implicit val formatEventParameter: Format[EventParameter] = new Format[EventParameter] {

    override def reads(json: JsValue): JsResult[EventParameter] = {
      def from(name: String, data: JsObject): JsResult[EventParameter] = name match {
        case "MovementEventParameter" => Json.fromJson[MovementEventParameter](data)(readsMovementEventParameter)
        case "BalanceEventParameter" => Json.fromJson[BalanceEventParameter](data)(readsBalanceEventParameter)
        case _ => JsError(s"Unknown class '$name'")
      }

      for {
        name <- (json \ "class").validate[String]
        data <- (json \ "data").validate[JsObject]
        result <- from(name, data)
      } yield result
    }

    override def writes(o: EventParameter): JsValue = {
      val (prod: Product, sub) = o match {
        case b: MovementEventParameter => (b, Json.toJson(b)(writesMovementEventParameter))
        case b: BalanceEventParameter => (b, Json.toJson(b)(writesBalanceEventParameter))
      }
      JsObject(Seq("class" -> JsString(prod.productPrefix), "data" -> sub))
    }

  }

  case class RuleOption(option: Option[String] = None,
                        startValidity: Option[BSONDateTime],
                        endValidity: Option[BSONDateTime])

  case class Rule(description: Option[String],
                  startValidity: Option[BSONDateTime] = None,
                  endValidity: Option[BSONDateTime] = None,
                  options: Option[Seq[RuleOption]] = None)

  case class Value(startValidity: Option[BSONDateTime] = None,
                   endValidity: Option[BSONDateTime] = None)

  case class MovementScript(debitAccount: Option[String],
                            creditAccount: Option[String],
                            sapCreditTransaction: Option[String],
                            sapDebitTransaction: Option[String],
                            startValidity: Option[BSONDateTime],
                            endValidity: Option[BSONDateTime],
                            options: Option[Seq[RuleOption]])

  case class BalanceScript(account: Option[String],
                           startValidity: Option[BSONDateTime],
                           endValidity: Option[BSONDateTime],
                           options: Option[Seq[RuleOption]])

  case class System(name: Option[String] = None,
                    sapCode: Option[String] = None,
                    startValidity: Option[BSONDateTime] = None,
                    endValidity: Option[BSONDateTime] = None)

  case class MovementEventParameter(code: Option[String] = None,
                                    productCode: Option[String] = None,
                                    startValidity: Option[BSONDateTime] = None,
                                    endValidity: Option[BSONDateTime] = None,
                                    system: Option[System] = None,
                                    rules: Option[Seq[Rule]] = None,
                                    values: Option[Seq[Value]] = None,
                                    scripts: Option[Seq[MovementScript]] = None)
    extends EventParameter

  case class BalanceEventParameter(code: Option[String] = None,
                                   productCode: Option[String] = None,
                                   startValidity: Option[BSONDateTime] = None,
                                   endValidity: Option[BSONDateTime] = None,
                                   system: Option[System] = None,
                                   nature: Option[String] = None,
                                   documentRequired: Option[Boolean] = None,
                                   rules: Option[Seq[Rule]] = None,
                                   scripts: Option[Seq[BalanceScript]] = None)
    extends EventParameter

  trait EventParameter {
    def code: Option[String]

    def productCode: Option[String]

    def startValidity: Option[BSONDateTime]

    def endValidity: Option[BSONDateTime]

    def system: Option[System]

    def rules: Option[Seq[Rule]]
  }

}
package io.sicredi.accounting.validation.configuration

import java.nio.charset.StandardCharsets
import java.util.Properties
import io.sicredi.accounting.validation.domain.Raw.Event
import org.apache.flink.api.common.restartstrategy.RestartStrategies
import org.apache.flink.api.common.serialization.{DeserializationSchema, SimpleStringSchema}
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.watermark.Watermark
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer011, FlinkKafkaProducer011}

case class ClosureDateKafkaProperties(topicIn: String, groupId: String)

case class KafkaProperties(servers: String, topicIn: String, topicOutSuccess: String, topicOutOutcome: String, topicOutFailure: String, restartAttempts: Int,
                           delayBetweenAttempts: Int, checkpointInterval: Int, minPauseBetweenCheckpoints: Int, maxConcurrentCheckpoints: Int,
                           groupId: String, userName: String, password: String)

case class Kafka(kafkaProperties: KafkaProperties, closureDateProperties: ClosureDateKafkaProperties) {

  lazy val kafkaServers: String = System.getenv(CONSUL_SERVER) match {
    case _: String => kafkaProperties.servers
    case null => loadBootstrapConf().getString(KafkaServerLocalAccess)
  }

  def getProducerToError: FlinkKafkaProducer011[String] = {
    new FlinkKafkaProducer011[String](
      kafkaProperties.topicOutFailure,
      new SimpleStringSchema,
      getKafkaProducerProps)
  }

  def getProducerToOutcome: FlinkKafkaProducer011[String] = {
    new FlinkKafkaProducer011[String](
      kafkaProperties.topicOutOutcome,
      new SimpleStringSchema,
      getKafkaProducerProps)
  }

  def getProducerToProcessing: FlinkKafkaProducer011[String] = {
    new FlinkKafkaProducer011[String](
      kafkaProperties.topicOutSuccess,
      new SimpleStringSchema,
      getKafkaProducerProps)
  }

  def getClosureDateConsumer: FlinkKafkaConsumer011[String] = {
    val myConsumer = new FlinkKafkaConsumer011(
      closureDateProperties.topicIn,
      new SimpleStringSchema,
      getClosureDateKafkaConsumerProps)

    myConsumer.setStartFromGroupOffsets()

    myConsumer
  }

  def getConsumer: FlinkKafkaConsumer011[Event] = {
    val myConsumer = new FlinkKafkaConsumer011[Event](
      kafkaProperties.topicIn,
      new MySchema,
      getKafkaConsumerProps)

    myConsumer.assignTimestampsAndWatermarks(new MyTimestampAndWatermark)

    myConsumer.setStartFromGroupOffsets()
    //    myConsumer.setStartFromTimestamp(ZonedDateTime.now().minusDays(1).toInstant.toEpochMilli)

    myConsumer
  }

  def getEnvironment: StreamExecutionEnvironment = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    env.getConfig.setRestartStrategy(RestartStrategies.fixedDelayRestart(
      kafkaProperties.restartAttempts,
      kafkaProperties.delayBetweenAttempts))

    env.enableCheckpointing(kafkaProperties.checkpointInterval)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.getCheckpointConfig.setMaxConcurrentCheckpoints(kafkaProperties.maxConcurrentCheckpoints)
    env.getCheckpointConfig.setMinPauseBetweenCheckpoints(kafkaProperties.minPauseBetweenCheckpoints)

    env
  }

  private def getClosureDateKafkaConsumerProps = {
    val properties: Properties = commonProperties
    properties.setProperty("group.id", closureDateProperties.groupId)
    properties
  }

  private def getKafkaConsumerProps = {
    val properties: Properties = commonProperties
    properties.setProperty("group.id", kafkaProperties.groupId)
    properties
  }

  private def getKafkaProducerProps = {
    commonProperties
  }

  private def commonProperties = {
    val properties = new Properties()
    properties.put("bootstrap.servers", kafkaServers)
    properties.put("sasl.jaas.config",
      s"""org.apache.kafka.common.security.scram.ScramLoginModule required username="${
        kafkaProperties.userName
      }" password="${kafkaProperties.password}";""")
    properties.put("security.protocol", "SASL_PLAINTEXT")
    properties.put("sasl.mechanism", "SCRAM-SHA-256")
    properties
  }

}

class MyTimestampAndWatermark extends AssignerWithPunctuatedWatermarks[Event] {
  override def checkAndGetNextWatermark(lastElement: Event, extractedTimestamp: Long): Watermark = new Watermark(extractedTimestamp - 10)

  override def extractTimestamp(element: Event, previousElementTimestamp: Long): Long = {
    element.timestamp = previousElementTimestamp

    previousElementTimestamp
  }
}

class MySchema extends DeserializationSchema[Event] {

  override def deserialize(message: Array[Byte]): Event = {
    if (message == null)
      null
    else
      Event(new String(message, StandardCharsets.UTF_8), 0L)
  }

  override def isEndOfStream(nextElement: Event): Boolean = false

  override def getProducedType: TypeInformation[Event] = TypeInformation.of(classOf[Event])

}

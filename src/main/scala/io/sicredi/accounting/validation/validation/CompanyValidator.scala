package io.sicredi.accounting.validation.validation

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.CompanyParameter
import io.sicredi.accounting.validation.domain.Raw.{BalanceRaw, MovementRaw, RawProcess, RawProcessFailure}
import io.sicredi.accounting.validation.service.CompanyParameterService
import scala.util.{Failure, Success}
import org.apache.flink.streaming.api.scala.async.{AsyncFunction, ResultFuture}
import scala.concurrent.ExecutionContext.Implicits.global

class CompanyValidator(service: CompanyParameterService) extends AsyncFunction[RawProcess, Either[Some[RawProcessFailure], Some[RawProcess]]] with StrictLogging {

  val ZERO = "0"

  override def asyncInvoke(incomingEvent: RawProcess, resultFuture: ResultFuture[Either[Some[RawProcessFailure], Some[RawProcess]]]): Unit = {
    logger.debug("starting company validator")

    service.getAllCompanies onComplete {
      case Success(companies) =>
        val companyResult = isCompanyCorrect(incomingEvent, companies)
        val unitResult = isUnitCorrect(incomingEvent, companies)

        if (companyResult && unitResult) {
          logger.debug("company validator result is right")
          resultFuture.complete(Iterable(Right(Some(incomingEvent))))
        } else if (!companyResult) {
          logger.debug("company is invalid")
          resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = Some("Company is invalid"), failuredRaw = Some(incomingEvent.payloadParsed.get), failuredPayload = incomingEvent.payload, topicPostingTimestamp = incomingEvent.topicPostingTimestamp)))))
        } else {
          logger.debug("units are invalid")
          resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = Some("Units are invalid"), failuredRaw = Some(incomingEvent.payloadParsed.get), failuredPayload = incomingEvent.payload, topicPostingTimestamp = incomingEvent.topicPostingTimestamp)))))
        }

      case Failure(exception) =>
        logger.debug("no companies found")
        resultFuture.completeExceptionally(exception)
    }
  }

  private def isCompanyCorrect(incomingEvent: RawProcess, companies: Seq[CompanyParameter.Company]) = {
    val companyRaw = incomingEvent.payloadParsed.get.company.get

    companies
      .exists(company => company.code.exists(code => companyRaw.equalsIgnoreCase(code) || companyRaw.equalsIgnoreCase(addZero(code))))
  }

  private def isUnitCorrect(incomingEvent: RawProcess, companies: Seq[CompanyParameter.Company]) = {
    val companyRaw = incomingEvent.payloadParsed.get.company.get

    companies
      .filter(company => company.code.exists(code => companyRaw.equalsIgnoreCase(code) || companyRaw.equalsIgnoreCase(addZero(code))))
      .exists(company => incomingEvent.payloadParsed.get match {
        case movement: MovementRaw =>
          company.units
            .exists(units => (units.contains(movement.debitUnit.get) || addZero(units).contains(movement.debitUnit.get)) &&
              (units.contains(movement.creditUnit.get) || addZero(units).contains(movement.creditUnit.get)))

        case balance: BalanceRaw =>
          company.units.exists(units => units.contains(balance.unit.get) || addZero(units).contains(balance.unit.get))
      })
  }

  private def addZero(units: Seq[String]) = {
    units
      .map(unit => {
        var unitAux = unit

        while (unitAux.length < 4)
          unitAux = ZERO + unitAux

        unitAux
      })
  }

  private def addZero(code: String) = {
    var codeAux = code

    while (codeAux.length < 4)
      codeAux = ZERO + codeAux

    codeAux
  }

}

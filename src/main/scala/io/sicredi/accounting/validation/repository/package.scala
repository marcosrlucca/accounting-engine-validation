package io.sicredi.accounting.validation

import java.time.ZonedDateTime
import io.sicredi.accounting.validation.domain.ClosureDateParameter.ClosureDate
import io.sicredi.accounting.validation.domain.CompanyParameter.Company
import io.sicredi.accounting.validation.domain.EventParameter._
import io.sicredi.accounting.validation.domain.Raw.{Balance, BalanceRaw, Movement, MovementRaw, Raw}
import io.sicredi.accounting.validation.domain.SystemParameter.SystemParameterView
import reactivemongo.bson.{BSONDocumentReader, BSONDocumentWriter, Macros}
import scala.util.{Failure, Success, Try}

package object repository {

  val EMPTY = ""

  val NOT_PARSED = "EVENT_NOT_PARSED"

  val NOT_VALIDATED = "EVENT_NOT_VALIDATED"

  val BALANCE = "BALANCE"

  val MOVEMENT = "MOVEMENT"

  val TRACE_LOGGING = "TRACE_LOGGING"

  val REVERSAL_WAITING_COLLECTION = "reversal_waiting_for_original_event"

  implicit def systemReader: BSONDocumentReader[System] = Macros.reader[System]

  implicit def systemWriter: BSONDocumentWriter[System] = Macros.writer[System]

  implicit def valueReader: BSONDocumentReader[Value] = Macros.reader[Value]

  implicit def valueWriter: BSONDocumentWriter[Value] = Macros.writer[Value]

  implicit def ruleOptionReader: BSONDocumentReader[RuleOption] = Macros.reader[RuleOption]

  implicit def ruleOptionWriter: BSONDocumentWriter[RuleOption] = Macros.writer[RuleOption]

  implicit def ruleReader: BSONDocumentReader[Rule] = Macros.reader[Rule]

  implicit def ruleWriter: BSONDocumentWriter[Rule] = Macros.writer[Rule]

  implicit def movementScriptReader: BSONDocumentReader[MovementScript] = Macros.reader[MovementScript]

  implicit def movementScriptWriter: BSONDocumentWriter[MovementScript] = Macros.writer[MovementScript]

  implicit def balanceScriptReader: BSONDocumentReader[BalanceScript] = Macros.reader[BalanceScript]

  implicit def balanceScriptWriter: BSONDocumentWriter[BalanceScript] = Macros.writer[BalanceScript]

  implicit def movementEventParameterReader: BSONDocumentReader[MovementEventParameter] = Macros.reader[MovementEventParameter]

  implicit def movementEventParameterWriter: BSONDocumentWriter[MovementEventParameter] = Macros.writer[MovementEventParameter]

  implicit def balanceEventParameterReader: BSONDocumentReader[BalanceEventParameter] = Macros.reader[BalanceEventParameter]

  implicit def balanceEventParameterWriter: BSONDocumentWriter[BalanceEventParameter] = Macros.writer[BalanceEventParameter]

  implicit def closureDateReader: BSONDocumentReader[ClosureDate] = Macros.reader[ClosureDate]

  implicit def closureDateWriter: BSONDocumentWriter[ClosureDate] = Macros.writer[ClosureDate]

  implicit def systemParameterReader: BSONDocumentReader[SystemParameterView] = Macros.reader[SystemParameterView]

  implicit def movementRawReader: BSONDocumentReader[MovementRaw] = Macros.reader[MovementRaw]

  implicit def movementRawWriter: BSONDocumentWriter[MovementRaw] = Macros.writer[MovementRaw]

  implicit def balanceRawReader: BSONDocumentReader[BalanceRaw] = Macros.reader[BalanceRaw]

  implicit def balanceRawWriter: BSONDocumentWriter[BalanceRaw] = Macros.writer[BalanceRaw]

  implicit def movementReader: BSONDocumentReader[Movement] = Macros.reader[Movement]

  implicit def movementWriter: BSONDocumentWriter[Movement] = Macros.writer[Movement]

  implicit def balanceReader: BSONDocumentReader[Balance] = Macros.reader[Balance]

  implicit def balanceWriter: BSONDocumentWriter[Balance] = Macros.writer[Balance]

  implicit def companyReader: BSONDocumentReader[Company] = Macros.reader[Company]

  implicit def companyWriter: BSONDocumentWriter[Company] = Macros.writer[Company]

  def getCollectionName(raw: Raw): Option[String] =
    raw match {
      case balanceRaw: BalanceRaw =>
        val maybeDate =
          Try(ZonedDateTime.parse(balanceRaw.baseDate.get)) match {
            case Success(bDate) => Some(bDate.toLocalDate)
            case Failure(_) => None
          }

        Some(BALANCE + "_" + maybeDate.get.getMonth.name() + "_" + maybeDate.get.getYear)

      case movementRaw: MovementRaw =>
        val maybeDate =
          if (movementRaw.reversal.get)
            Try(ZonedDateTime.parse(movementRaw.originalEventBaseDateForReversal.get)) match {
              case Success(bDate) => Some(bDate)
              case Failure(_) => None
            }
          else
            Try(ZonedDateTime.parse(movementRaw.baseDate.get)) match {
              case Success(bDate) => Some(bDate)
              case Failure(_) => None
            }

        Some(MOVEMENT + "_" + maybeDate.get.getMonth.name + "_" + maybeDate.get.getYear)

      case _ => None
    }

}

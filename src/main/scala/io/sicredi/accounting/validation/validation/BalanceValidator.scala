package io.sicredi.accounting.validation.validation

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.EventParameter.{BalanceEventParameter, EventParameter}
import io.sicredi.accounting.validation.domain.Raw.{BalanceRaw, RawProcess, RawProcessFailure}
import io.sicredi.accounting.validation.helper.{COMPANY_DESCRIPTION, ValidationHelper}
import io.sicredi.accounting.validation.service.BalanceEventParameterService
import org.apache.flink.streaming.api.scala.async.{AsyncFunction, ResultFuture}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class BalanceValidator(service: BalanceEventParameterService) extends RawValidator with AsyncFunction[RawProcess, Either[Some[RawProcessFailure], Some[RawProcess]]] with StrictLogging {

  override def asyncInvoke(incomingEvent: RawProcess, resultFuture: ResultFuture[Either[Some[RawProcessFailure], Some[RawProcess]]]): Unit = {
    logger.debug("starting balance validator")

    service.getAll onComplete {
      case Success(eventParameters) =>
        val rulesFromIncomingEvent = incomingEvent.payloadParsed.get.rules.get

        val incomingEventValidated = eventParameters
          .map(eventPar => isSameEventCodeOrProductCode(incomingEvent, eventPar))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isSameSystem(incomingEvent, resultValidation))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isRulesDefined(resultValidation))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isSameSizeRules(rulesFromIncomingEvent, resultValidation))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isValueWithTwoDecimalPlaces(incomingEvent, resultValidation))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isDateValid(incomingEvent, resultValidation))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isOptionsPresent(resultValidation))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isScriptsPresent(resultValidation))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isOptionsValid(incomingEvent.payloadParsed.get.asInstanceOf[BalanceRaw], resultValidation))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isDocumentRequired(incomingEvent.payloadParsed.get.asInstanceOf[BalanceRaw], resultValidation))
          .filter(resultValidation => resultValidation.isRight)
          .map(resultValidation => isScriptsFromBalanceAndRulesFromRawValid(incomingEvent.payloadParsed.get.asInstanceOf[BalanceRaw], resultValidation))
          .exists(resultValidationEventPar => resultValidationEventPar.isRight)

        if (incomingEventValidated) {
          logger.debug("balance validator result is right")
          resultFuture.complete(Iterable(Right(Some(incomingEvent))))
        } else {
          logger.debug("balance validator result is left")
          resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = Some(errors.get mkString ","), failuredRaw = Some(incomingEvent.payloadParsed.get), failuredPayload = incomingEvent.payload, topicPostingTimestamp = incomingEvent.topicPostingTimestamp)))))
        }

      case Failure(exception) =>
        logger.error(exception.getLocalizedMessage)
        resultFuture.completeExceptionally(exception)
    }
  }

  override def isValueWithTwoDecimalPlaces(incomingEvent: RawProcess, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    incomingEvent.payloadParsed
      .filter(raw => raw.isInstanceOf[BalanceRaw])
      .map(raw => raw.asInstanceOf[BalanceRaw])
      .map(balanceRaw => {
        errors.foreach(e => e.clear())
        if (isTwoDecimalPlaces(balanceRaw.value.get)) {
          errors.foreach(e => e(VALUE_TWO_DECIMAL_PLACES) = VALUE_TWO_DECIMAL_PLACES_TEXT)
          Left(VALUE_TWO_DECIMAL_PLACES_TEXT)
        } else {
          Right(resultValidationEventPar.right.get.asInstanceOf[BalanceEventParameter])
        }
      })
      .getOrElse(resultValidationEventPar)
  }

  override def isScriptsPresent(resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    errors.foreach(e => e.clear())
    val eventPar = resultValidationEventPar.right.get.asInstanceOf[BalanceEventParameter]

    val validated = eventPar.scripts.get.exists(roadmap => roadmap.options.isDefined && roadmap.options.get.nonEmpty && roadmap.options.get.exists(opt => opt.option.isDefined))

    if (!validated) {
      errors.foreach(e => e(SCRIPTS_NOT_EXISTS) = SCRIPTS_NOT_EXISTS_TEXT)
      Left(SCRIPTS_NOT_EXISTS_TEXT)
    } else {
      Right(eventPar)
    }
  }

  override def isScriptsPresent(eventPar: EventParameter): Either[String, EventParameter] = {
    val balanceParameter = eventPar.asInstanceOf[BalanceEventParameter]
    val validated = balanceParameter.scripts.get.exists(roadmap => roadmap.options.isDefined && roadmap.options.get.nonEmpty && roadmap.options.get.exists(opt => opt.option.isDefined))

    if (!validated) {
      errors.foreach(e => e(SCRIPTS_NOT_EXISTS) = SCRIPTS_NOT_EXISTS_TEXT)
      Left(SCRIPTS_NOT_EXISTS_TEXT)
    } else {
      Right(eventPar)
    }
  }

  private def isCompanyValid(raw: BalanceRaw, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    val eventDB = resultValidationEventPar.right.get.asInstanceOf[BalanceEventParameter]

    val hasCompany = eventDB.rules
      .exists(rules => rules
        .exists(rule => rule.description
          .exists(desc => desc.equalsIgnoreCase(COMPANY_DESCRIPTION))))

    if (hasCompany) {
      val companyIsValid = ValidationHelper.isCompanyFromBalanceValid(eventDB, raw)

      if (companyIsValid) {
        resultValidationEventPar
      } else {
        errors.foreach(e => e(COMPANY_NOT_VALID) = COMPANY_NOT_VALID_TEXT)
        Left(COMPANY_NOT_VALID_TEXT)
      }
    } else {
      resultValidationEventPar
    }
  }

  private def isScriptsFromBalanceAndRulesFromRawValid(raw: BalanceRaw, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    val scriptsIsValid = ValidationHelper.isScriptBalanceAndRulesFromRawValid(resultValidationEventPar.right.get.asInstanceOf[BalanceEventParameter], raw, errors)

    if (scriptsIsValid) {
      resultValidationEventPar
    } else {
      errors.foreach(e => e(SCRIPTS_NOT_VALID) = SCRIPTS_NOT_VALID_TEXT)
      Left(SCRIPTS_NOT_VALID_TEXT)
    }
  }

  private def isDocumentRequired(raw: BalanceRaw, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    val parameter: BalanceEventParameter = resultValidationEventPar.right.get.asInstanceOf[BalanceEventParameter]
    val isDocumentRequired: Boolean = parameter.documentRequired.get

    if (isDocumentRequired) {
      /**
        * Se o parâmetro do evento exige que seja enviado o CPF e o evento chega SEM o CPF, então um erro será gerado no JIRA
        */
      if (raw.document.isDefined && !raw.document.get.isEmpty) {
        resultValidationEventPar
      } else {
        errors.foreach(e => e(DOCUMENT_REQUIRED_LBL) = DOCUMENT_REQUIRED_TXT)
        Left(DOCUMENT_REQUIRED_TXT)
      }
    } else {
      /**
        * Se o parâmetro do evento estiver configurado para não permitir o envio do CPF, então será emitido um erro no JIRA
        * caso o evento chegue com document informado.
        */
      if (raw.document.isEmpty || raw.document.get.isEmpty) {
        resultValidationEventPar
      } else {
        errors.foreach(e => e(DOCUMENT_IS_NOT_ALLOWED_LBL) = DOCUMENT_IS_NOT_ALLOWED_TXT)
        Left(DOCUMENT_IS_NOT_ALLOWED_TXT)
      }
    }
  }
}

package io.sicredi.accounting.validation.validation

import java.time.ZonedDateTime
import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.Raw.{BalanceRaw, MovementRaw, Raw, RawProcess, RawProcessFailure}
import scala.util.{Failure, Success, Try}

object MandatoryFieldsValidator extends StrictLogging {

  private def validateMandatory(errors: scala.collection.mutable.Map[String, String], incomingEvent: Raw): Unit = {
    if (incomingEvent.baseDate.isEmpty || incomingEvent.baseDate.get.trim.isEmpty)
      errors(BASE_DATE_LBL) = BASE_DATE_TXT

    if (incomingEvent.company.isEmpty || incomingEvent.company.get.trim.isEmpty)
      errors(COMPANY_LBL) = COMPANY_TXT

    if ((incomingEvent.accountingReferenceCode.isEmpty || incomingEvent.accountingReferenceCode.get.trim.isEmpty) && (incomingEvent.productReferenceCode.isEmpty || incomingEvent.productReferenceCode.get.trim.isEmpty))
      errors(REFERENCE_CODE_LBL) = REFERENCE_CODE_TXT
    else if (incomingEvent.accountingReferenceCode.isDefined && incomingEvent.productReferenceCode.isDefined)
      errors(PRODUCT_OR_ACCOUNTING_REFERENCE_CODE_LBL) = PRODUCT_OR_ACCOUNTING_REFERENCE_CODE_TXT

    if (incomingEvent.transactionId.isEmpty || incomingEvent.transactionId.get.trim.isEmpty)
      errors(TRANSATION_ID_LBL) = TRANSATION_ID_TXT

    if (incomingEvent.system.isEmpty || incomingEvent.system.get.trim.isEmpty)
      errors(SYSTEM_NAME_LBL) = SYSTEM_NAME_TXT

    if (incomingEvent.rules.isEmpty || incomingEvent.rules.get.exists(a => a.trim.isEmpty))
      errors(RULES_LBL) = RULES_TXT

    incomingEvent match {
      case raw: MovementRaw =>
        if (raw.debitUnit.isEmpty || raw.debitUnit.get.trim.isEmpty)
          errors(DEBIT_UNIT_LBL) = DEBIT_UNIT_TXT

        if (raw.creditUnit.isEmpty || raw.creditUnit.get.trim.isEmpty)
          errors(CREDIT_UNIT_LBL) = CREDIT_UNIT_TXT

        if (raw.values.isEmpty)
          errors(VALUES_LBL) = VALUES_TXT

        if (raw.reversal.isEmpty)
          errors(REVERSAL_LBL) = REVERSAL_TXT

        if (raw.reversal.isDefined && raw.reversal.get && (raw.originalEventBaseDateForReversal.isEmpty || raw.originalEventBaseDateForReversal.get.trim.isEmpty))
          errors(ORIGINAL_EVENT_BASE_DATE_NOT_EMPTY_LBL) = ORIGINAL_EVENT_BASE_DATE_NOT_EMPTY_TXT

        if (raw.reversal.isDefined && !raw.reversal.get && raw.originalEventBaseDateForReversal.isDefined)
          errors(ORIGINAL_EVENT_BASE_DATE_EMPTY_LBL) = ORIGINAL_EVENT_BASE_DATE_EMPTY_TXT

      case raw: BalanceRaw =>
        if (raw.value.isEmpty)
          errors(VALUES_LBL) = VALUES_TXT

        if (raw.unit.isEmpty || raw.unit.get.trim.isEmpty)
          errors(UNIT_LBL) = UNIT_TXT
    }
  }

  private def validateSizeAndType(errors: scala.collection.mutable.Map[String, String], incomingEvent: Raw): Unit = {
    if (!incomingEvent.company.exists(c => c.length == 4))
      errors(COMPANY_SIZE_LBL) = COMPANY_SIZE_TXT

    incomingEvent.baseDate
      .foreach(baseDate =>
        Try(
          ZonedDateTime.parse(baseDate)
        ) match {
          case Success(_) =>

          case Failure(_) =>
            errors(BASE_DATE_CONVERSION_EXC_LBL) = BASE_DATE_CONVERSION_EXC_TXT
        }
      )

    incomingEvent.operationDate
      .foreach(operationDate =>
        Try(
          ZonedDateTime.parse(operationDate)
        ) match {
          case Success(_) =>
          case Failure(exception) =>
            errors(OPERATION_DATE_CONVERSION_EXC_LBL) = OPERATION_DATE_CONVERSION_EXC_TXT
        }
      )

    incomingEvent match {
      case raw: MovementRaw =>
        if (!raw.debitUnit.exists(s => s.length == 4))
          errors(SIZE_DEBIT_UNIT_LBL) = SIZE_DEBIT_UNIT_TXT

        if (!raw.creditUnit.exists(s => s.length == 4))
          errors(SIZE_CREDIT_UNIT_LBL) = SIZE_CREDIT_UNIT_TXT

        if (!raw.reversal.exists(s => s || !s))
          errors(REVERSAL_BOOL_LBL) = REVERSAL_BOOL_TXT

        if (raw.transactionId.exists(t => t.length > 50))
          errors(TRANSACTION_ID_LENGTH_LBL) = TRANSACTION_ID_LENGTH_TXT

        if (raw.transactionDetail.exists(t => t.length > 50))
          errors(TRANSACTION_DETAIL_LENGTH_LBL) = TRANSACTION_DETAIL_LENGTH_TXT

        raw.originalEventBaseDateForReversal
          .foreach(originalEventBaseDateForReversal =>
            Try(
              ZonedDateTime.parse(originalEventBaseDateForReversal)
            ) match {
              case Success(_) =>

              case Failure(_) =>
                errors(ORIGINAL_EVENT_BASE_DATE_CONVERSION_EXC_LBL) = ORIGINAL_EVENT_BASE_DATE_CONVERSION_EXC_TXT
            }
          )

      case raw: BalanceRaw =>
        if (!raw.unit.exists(s => s.length == 4))
          errors(SIZE_UNIT_LBL) = SIZE_UNIT_TXT
    }

    incomingEvent.document match {
      case Some(doc) =>
        if (doc.length > 14)
          errors(DOCUMENT_LBL) = DOCUMENT_TXT

      case None =>
    }
  }

  def validateSizeAndTypeAndMandatory(incomingEvent: RawProcess): Either[Some[RawProcessFailure], Some[RawProcess]] = {
    logger.debug("starting mandatory validaton")

    val errors = scala.collection.mutable.Map[String, String]()

    logger.debug("gonna to validate mandatory")

    validateMandatory(errors, incomingEvent.payloadParsed.get)

    logger.debug("gonna to validate size and type")

    validateSizeAndType(errors, incomingEvent.payloadParsed.get)

    if (errors.isEmpty) {
      logger.debug("mandatory fields validator result is right")
      Right(Some(incomingEvent))
    } else {
      logger.debug("mandatory fields validator result is left")
      Left(Some(RawProcessFailure(rawProcessError = Some(errors mkString ","), failuredRaw = Some(incomingEvent.payloadParsed.get), failuredPayload = incomingEvent.payload, topicPostingTimestamp = incomingEvent.topicPostingTimestamp)))
    }
  }

}

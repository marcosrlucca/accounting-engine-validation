package io.sicredi.accounting.validation.repository

import io.sicredi.accounting.validation.domain.EventParameter.EventParameter
import scala.concurrent.Future

trait EventParameterRepository {

  def findAll(): Future[Seq[EventParameter]]

}

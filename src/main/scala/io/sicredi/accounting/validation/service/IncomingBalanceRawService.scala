package io.sicredi.accounting.validation.service

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.Raw.{RawProcess, RawProcessFailure}
import io.sicredi.accounting.validation.repository.IncomingBalanceRepository
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.{implicitConversions, postfixOps}
import io.sicredi.accounting.validation.domain.Raw.BalanceRaw
import org.apache.flink.streaming.api.scala.async.{AsyncFunction, ResultFuture}
import scala.util.{Failure, Success}

class IncomingBalanceRawService(incomingEventRepository: IncomingBalanceRepository) extends AsyncFunction[(Boolean, Option[String], RawProcess), Either[Some[RawProcessFailure], Some[RawProcess]]] with StrictLogging {

  override def asyncInvoke(cachedRawProcess: (Boolean, Option[String], RawProcess), resultFuture: ResultFuture[Either[Some[RawProcessFailure], Some[RawProcess]]]): Unit = {

    logger.debug("starting incoming balance raw service")

    val foundEventInCache = cachedRawProcess._1
    val errorMessageFromCache = cachedRawProcess._2
    val rawProcess = cachedRawProcess._3

    if (foundEventInCache) { //se encontrou evento na cache e evento é inválido
      logger.debug("found in cache and invalid event")

      resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = errorMessageFromCache, failuredRaw = rawProcess.payloadParsed, failuredPayload = rawProcess.payload, topicPostingTimestamp = rawProcess.topicPostingTimestamp)))))
    } else { //se não encontrou evento na cache, então valida pelo banco de dados
      logger.debug("event not found in cache")

      val incomingRaw = rawProcess.payloadParsed.get.asInstanceOf[BalanceRaw]

      val resultOfEventAlreadyExists =
        for {
          eventDB <- incomingEventRepository.findOneBy(incomingRaw)

          res <- eventDB match {
            case Some(_) =>
              Future {
                Left("Event already exists")
              }

            case _ =>
              Future {
                Right(rawProcess)
              }
          }
        } yield res

      resultOfEventAlreadyExists onComplete {
        case Success(maybeEvent) =>
          maybeEvent match {
            case Right(_) =>
              logger.debug("incoming balance raw service result is right")
              resultFuture.complete(Iterable(Right(Some(rawProcess))))

            case Left(e) =>
              logger.debug("incoming balance raw service result is left")
              resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = Some(e), failuredRaw = rawProcess.payloadParsed, failuredPayload = rawProcess.payload, topicPostingTimestamp = rawProcess.topicPostingTimestamp)))))
          }

        case Failure(e) =>
          logger.error(e.getLocalizedMessage)
          resultFuture.completeExceptionally(e)
      }
    }

  }

}
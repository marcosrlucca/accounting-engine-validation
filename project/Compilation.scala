import sbt.Keys.{libraryDependencies, _}
import sbt.{Compile, _}
import sbtassembly.AssemblyKeys._
import sbtassembly.{MergeStrategy, PathList}

object Compilation {

  lazy val apiName = "accounting-engine-validation"
  lazy val suffix = ".jar"

  lazy val buildSettings = Seq(
    name := apiName,
    organization := "io.sicredi.accounting",
    version := "2.9.0"
  )

  val settings = Seq(
    ThisBuild / scalaVersion := "2.11.12",

    libraryDependencies ++= Dependencies.all,

    // force scala-library to be removed from pom
    autoScalaLibrary := false,

    assembly / mainClass := Some("io.sicredi.accounting.validation.Application"),

    credentials += Credentials(Path.userHome / ".sbt" / ".credentials"),

    // publish to nexus
    publishTo := {
      val suffix = if (isSnapshot.value) "snapshots" else "releases"
      Some("sicredi-nexus" at s"https://nexus.sicredi.in/repository/$suffix")
    },


    // exclude Scala library from assembly
    assembly / assemblyOption := (assembly / assemblyOption).value.copy(includeScala = false),

    // include merge strategy for dependencies containing repeatable configuration files within meta-inf
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case x => MergeStrategy.first
    },

    // create artifact during compilation phase
    artifact in(Compile, assembly) := {
      val art = (artifact in(Compile, assembly)).value
      art.withClassifier(Some("assembly"))
    },

    // add assembled artifact to publish tasks
    addArtifact(artifact in(Compile, assembly), assembly),

    // make run command include the provided dependencies
    Compile / run := Defaults.runTask(
      Compile / fullClasspath,
      Compile / run / mainClass,
      Compile / run / runner
    ).evaluated,

    // stays inside the sbt console when we press "ctrl-c" while a Flink programme executes with "run" or "runMain"
    Compile / run / fork := true,
    Global / cancelable := true

  )
}

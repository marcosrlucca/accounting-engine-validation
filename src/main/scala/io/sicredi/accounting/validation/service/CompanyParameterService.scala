package io.sicredi.accounting.validation.service

import io.sicredi.accounting.validation.domain.CompanyParameter.Company
import io.sicredi.accounting.validation.repository.CompanyParameterRepository
import scalacache.Cache
import scalacache.modes.sync._
import scala.concurrent.Future
import scala.language.{implicitConversions, postfixOps}

class CompanyParameterService(companyRepository: CompanyParameterRepository) extends Serializable {

  private lazy val companies: Cache[Future[Seq[Company]]] = Cache.companyParameterCache.cache

  def getAllCompanies: Future[Seq[Company]] = {
    val maybeCompanies = companies
      .get(AllCompaniesLabel)
      .getOrElse(companies.cachingForMemoize(AllCompaniesLabel)(Some(globalCacheDuration))(companyRepository.findAll()))

    maybeCompanies
  }

}

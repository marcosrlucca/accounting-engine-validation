package io.sicredi.accounting.validation.validation

import java.time.ZonedDateTime
import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.Raw.{RawProcess, RawProcessFailure}
import io.sicredi.accounting.validation.service.ClosureDateService
import org.apache.flink.streaming.api.scala.async.{AsyncFunction, ResultFuture}
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

class ClosureDateValidator(service: ClosureDateService) extends AsyncFunction[RawProcess, Either[Some[RawProcessFailure], Some[RawProcess]]] with StrictLogging {

  override def asyncInvoke(incomingEvent: RawProcess, resultFuture: ResultFuture[Either[Some[RawProcessFailure], Some[RawProcess]]]): Unit = {
    logger.debug("starting closure date validator")

    service.getLastClosureDateByCompany(incomingEvent.payloadParsed.get.company.get) onComplete {

      case Success(maybeClosureDateParameter) =>
        if (maybeClosureDateParameter.isEmpty) {
          resultFuture.complete(Iterable(Right(Some(incomingEvent))))
        } else {
          val closureDateParameter = maybeClosureDateParameter.get
          Try(ZonedDateTime.parse(incomingEvent.payloadParsed.get.baseDate.get)) match {
            case Success(baseDate) =>
              if (closureDateParameter.getMonthValue.isDefined && baseDate.getMonthValue <= closureDateParameter.getMonthValue.get) {
                logger.debug("closure date validator result is left")
                resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = Some("The base date can not be equal or lower than closure date"), failuredRaw = Some(incomingEvent.payloadParsed.get), failuredPayload = incomingEvent.payload, topicPostingTimestamp = incomingEvent.topicPostingTimestamp)))))
              } else {
                resultFuture.complete(Iterable(Right(Some(incomingEvent))))
              }

            case Failure(e) =>
              logger.error(e.getLocalizedMessage)
              resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = Some("Error on parsing incoming base date: " + e), failuredRaw = Some(incomingEvent.payloadParsed.get), failuredPayload = incomingEvent.payload, topicPostingTimestamp = incomingEvent.topicPostingTimestamp)))))
          }
        }

      case Failure(exception) =>
        logger.error(exception.getLocalizedMessage)
        resultFuture.completeExceptionally(exception)

    }

  }

}
package io.sicredi.accounting.validation.service

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.Raw.{MovementRaw, RawProcess}
import scalacache.caffeine.CaffeineCache
import scalacache.modes.sync._
import io.sicredi.accounting.validation.service.Cache.MovementCache

class MovementCacheService extends StrictLogging {

  private val cache: CaffeineCache[MovementCache] = Cache.customisedMovementRawCache

  def process(process: RawProcess): (Boolean, Boolean, Option[String], RawProcess) = {
    logger.debug("starting cache")

    val incomingRaw: MovementRaw = process.payloadParsed.get.asInstanceOf[MovementRaw]
    val uniqueIndex = incomingRaw._unique_index.get

    /*posição 1: evento validado com sucesso pela cache
    posição 2: evento encontrado na cache
    posição 3: mensagem de erro
    posição 4: objeto RawProcess*/
    var result = Tuple4[Boolean, Boolean, Option[String], RawProcess](false, false, None, process)

    /*Cria 2 flags, uma de um evento que não é do tipo estorno e uma do tipo estorno.
    Essas flags ajudam a identificar se existe o evento em cache.
    O evento em cache pode ser do tipo não-estorno que vai usar a flag originalEventFlag, como também pode ser do tipo
    estorno aguardando pelo evento original, que vai usar a flag reversalWaitingOriginalEventFlag.*/
    val originalEventFlag = FLAG_ORIGINAL_EVENT + uniqueIndex
    val reversalWaitingOriginalEventFlag = FLAG_WAITING_FOR_THE_ORIGINAL + uniqueIndex

    if (cache.get(originalEventFlag).isDefined) {
      //1ª opção: evento original existe na cache

      logger.debug("original event exists in cache")

      val movementFromCache: MovementCache = cache.get(originalEventFlag).get

      //incoming event é do tipo estorno
      if (incomingRaw.reversal.get && movementFromCache.dateReversalOcurred.isEmpty) {
        //evento original ainda não foi estornado

        //altera a data do estorno do evento original, para sinalizar que ele já foi estornado
        movementFromCache.dateReversalOcurred = incomingRaw.baseDate

        cache.put(originalEventFlag)(movementFromCache, Some(cacheToCheckIfAlreadyExists))

        result = (true, true, None, process)
      } else if (incomingRaw.reversal.get && movementFromCache.dateReversalOcurred.isDefined) {
        //evento original já foi estornado

        result = (false, true, Some("Event already reversed"), process)
      } else {
        //não é evento de estorno, então é erro, porque evento já existe em cache

        result = (false, true, Some("Event already exists"), process)
      }
    } else if (cache.get(originalEventFlag).isEmpty && cache.get(reversalWaitingOriginalEventFlag).isDefined) {
      //2ª opção: o evento original não existe na cache, porém o estorno que aguarda pelo evento original existe na cache

      logger.debug("original event does not exist in cache, but reversal waiting for original exists")

      if (incomingRaw.reversal.get) {
        //é evento de estorno, então é erro, porque estorno já existe em cache

        result = (false, true, Some("Reversal already exists"), process)
      } else {
        //incoming event não é estorno, então remove o estorno da cache e adiciona o evento original em cache, porém, o evento original deve ser adicionado estornado

        //recupera o estorno da cache
        val reversalEventWaitingFor = cache.get(reversalWaitingOriginalEventFlag).get

        //atualiza a data de estorno do evento original com a data do estorno que está em cache
        val mCache = MovementCache(uniqueIndex = incomingRaw._unique_index, baseDate = incomingRaw.baseDate, dateReversalOcurred = reversalEventWaitingFor.baseDate)

        //adiciona o evento original na cache
        cache.put(originalEventFlag)(mCache, Some(cacheToCheckIfAlreadyExists))

        //apaga o evento de estorno que estava esperando pelo original da cache
        cache.remove(reversalWaitingOriginalEventFlag)

        result = (true, true, None, process)
      }
    } else {
      //3ª opção: cache está vazia

      logger.debug("cache is empty")

      if (incomingRaw.reversal.get) {
        //incoming event é estorno, então adiciona este estorno em cache, para ele aguardar pelo evento original

        val mCache = MovementCache(uniqueIndex = incomingRaw._unique_index, baseDate = incomingRaw.baseDate, dateReversalOcurred = None)

        cache.put(reversalWaitingOriginalEventFlag)(mCache, Some(cacheToCheckIfAlreadyExists))
      } else {
        //incoming event não é estorno, então adiciona este evento em cache

        val mCache = MovementCache(uniqueIndex = incomingRaw._unique_index, baseDate = incomingRaw.baseDate, dateReversalOcurred = None)

        cache.put(originalEventFlag)(mCache, Some(cacheToCheckIfAlreadyExists))
      }

      result = (true, false, None, process)
    }

    result
  }

}

package io.sicredi.accounting.validation

package object validation {

  val EVENT_PAR_NOT_FOUND = "event_par_not_found"

  val SYSTEM_INVALID = "system_invalid"
  val SYSTEM_INVALID_TEXT = "System not found in event parameters"

  val EVENT_CODE_INVALID = "reference_code_invalid"
  val EVENT_CODE_INVALID_TEXT = "No Parameters found for this Reference Code"

  val EVENT_PAR_NOT_FOUND_TEXT = "Event Parameter not found"

  val RULE_NOT_DEFINED = "rule_not_defined"
  val RULE_NOT_DEFINED_TEXT = "Event Parameter should have Rules"

  val RULE_DIFFERENT_SIZE = "rule_different_size"
  val RULE_DIFFERENT_SIZE_TEXT = "The Rules have different size"

  val VALUE_NOT_DEFINED = "value_not_defined"
  val VALUE_NOT_DEFINED_TEXT = "Event Parameter should have Values"

  val VALUE_EQUAL_ZERO = "value_equal_zero"
  val VALUE_EQUAL_ZERO_TEXT = "Incoming Event should have at least one value different than zero"

  val VALUE_LESS_ZERO = "value_less_zero"
  val VALUE_LESS_ZERO_TEXT = "Incoming Event should have values greater than zero"

  val VALUE_TWO_DECIMAL_PLACES = "value_two_decimal_places"
  val VALUE_TWO_DECIMAL_PLACES_TEXT = "Incoming Event should have values with two decimal places"

  val VALUE_DIFFERENT_SIZE = "value_different_size"
  val VALUE_DIFFERENT_SIZE_TEXT = "The Values have different size"

  val DATE_NOT_VALID = "date_not_valid"
  val DATE_NOT_VALID_TEXT = "The Date Event Or Date System is not valid"

  val OPTION_NOT_EXISTS = "option_not_exists"
  val OPTION_NOT_EXISTS_TEXT = "The Parameter Options do not exist"

  val SCRIPTS_NOT_EXISTS = "scripts_not_exists"
  val SCRIPTS_NOT_EXISTS_TEXT = "The Scripts do not exist"

  val SCRIPTS_NOT_VALID = "scripts_not_valid"
  val SCRIPTS_NOT_VALID_TEXT = "The Scripts are not valid"

  val COMPANY_NOT_VALID = "company_not_valid"
  val COMPANY_NOT_VALID_TEXT = "The Company does not comply with the parameter"

  val COMPANY_IN_RULES_NOT_VALID = "company_in_rules_not_valid"
  val COMPANY_IN_RULES_NOT_VALID_TEXT = "The Company in rules is wrong"

  val OPTION_NOT_VALID = "option_not_valid"
  val OPTION_NOT_VALID_TEXT = "The Options are not valid"

  val BASE_DATE_LBL = "baseDate"
  val BASE_DATE_TXT = "Base date should not be empty"

  val COMPANY_LBL = "company"
  val COMPANY_TXT = "Company should not be empty"

  val COMPANY_SIZE_LBL = "companySize"
  val COMPANY_SIZE_TXT = "Company should be of size 4"

  val REFERENCE_CODE_LBL = "referenceCode"
  val REFERENCE_CODE_TXT = "Reference Code should not be empty"

  val PRODUCT_OR_ACCOUNTING_REFERENCE_CODE_LBL = "productOrAccountingReferenceCode"
  val PRODUCT_OR_ACCOUNTING_REFERENCE_CODE_TXT = "Only one Reference Code must be filled in"

  val TRANSATION_ID_LBL = "transactionId"
  val TRANSATION_ID_TXT = "Transaction Id should not be empty"

  val SYSTEM_NAME_LBL = "system"
  val SYSTEM_NAME_TXT = "System should not be empty"

  val RULES_LBL = "rule"
  val RULES_TXT = "Rules should not be empty"

  val DEBIT_UNIT_LBL = "debitUnit"
  val DEBIT_UNIT_TXT = "Debit Unit should not be empty"

  val CREDIT_UNIT_LBL = "creditUnit"
  val CREDIT_UNIT_TXT = "Credit Unit should not be empty"

  val VALUES_LBL = "values"
  val VALUES_TXT = "Values should not be empty"

  val VALUE_LBL = "value"
  val VALUE_TXT = "Value should not be empty"

  val UNIT_LBL = "unit"
  val UNIT_TXT = "Unit should not be empty"

  val REVERSAL_LBL = "reversal"
  val REVERSAL_TXT = "Reversal should not be empty"

  val REVERSAL_BOOL_LBL = "reversalBoolean"
  val REVERSAL_BOOL_TXT = "Reversal should be a boolean"

  val ORIGINAL_EVENT_BASE_DATE_NOT_EMPTY_LBL = "originalEventBaseDateNotEmptyForReversal"
  val ORIGINAL_EVENT_BASE_DATE_NOT_EMPTY_TXT = "Original Event Base Date should not be empty"

  val ORIGINAL_EVENT_BASE_DATE_EMPTY_LBL = "originalEventBaseDateEmptyForReversal"
  val ORIGINAL_EVENT_BASE_DATE_EMPTY_TXT = "Original Event Base Date should be empty"

  val BASE_DATE_CONVERSION_EXC_LBL = "baseDateConversionException"
  val BASE_DATE_CONVERSION_EXC_TXT = "Could not parse base date"

  val ORIGINAL_EVENT_BASE_DATE_CONVERSION_EXC_LBL = "originalEventBaseDateConversionException"
  val ORIGINAL_EVENT_BASE_DATE_CONVERSION_EXC_TXT = "Could not parse original event base date"

  val OPERATION_DATE_CONVERSION_EXC_LBL = "operationDateConversionException"
  val OPERATION_DATE_CONVERSION_EXC_TXT = "Could not parse operation date"

  val SIZE_DEBIT_UNIT_LBL = "sizeDebitUnit"
  val SIZE_DEBIT_UNIT_TXT = "Unit Debit should be of size 4"

  val SIZE_CREDIT_UNIT_LBL = "sizeCreditUnit"
  val SIZE_CREDIT_UNIT_TXT = "Credit Unit should be of size 4"

  val SIZE_UNIT_LBL = "sizeUnit"
  val SIZE_UNIT_TXT = "Unit should be of size 4"

  val DOCUMENT_LBL = "document"
  val DOCUMENT_TXT = "Maximum of 14 characters for document"

  val DOCUMENT_REQUIRED_LBL = "document_required"
  val DOCUMENT_REQUIRED_TXT = "CPF/CNPJ is required for this event"

  val DOCUMENT_IS_NOT_ALLOWED_LBL = "document_is_not_allowed"
  val DOCUMENT_IS_NOT_ALLOWED_TXT = "This event does not allow CPF/CNPJ to be sent. Please contact Digital's accounting team."

  val TRANSACTION_DETAIL_LENGTH_LBL = "transaction_detail"
  val TRANSACTION_DETAIL_LENGTH_TXT = "Maximum of 50 characters for transaction detail"

  val TRANSACTION_ID_LENGTH_LBL = "transaction_id"
  val TRANSACTION_ID_LENGTH_TXT = "Maximum of 50 characters for transaction id"

}
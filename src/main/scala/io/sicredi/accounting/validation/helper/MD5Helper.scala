package io.sicredi.accounting.validation.helper

import java.math.BigInteger
import java.security.MessageDigest
import io.sicredi.accounting.validation.domain.Raw.{Raw, RawProcessFailure}

object MD5Helper {

  def md5HashString(s: String): String = {
    val md = MessageDigest.getInstance("MD5")
    val digest = md.digest(s.getBytes)
    val bigInt = new BigInteger(1, digest)
    val hashedString = bigInt.toString(16)

    hashedString
  }

  def createHashId(incomingRaw: Raw): Option[String] = {
    Some(
      MD5Helper.md5HashString(incomingRaw.keyToUniqueIndex)
    )
  }

  def createHashId(failure: RawProcessFailure): Option[String] = {
    Some(
      MD5Helper.md5HashString(failure.keyToUniqueIndex)
    )
  }

}

package io.sicredi.accounting.validation.service

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.Raw.{MovementRaw, RawProcess, RawProcessFailure}
import io.sicredi.accounting.validation.repository.IncomingMovementRepository
import org.apache.flink.streaming.api.scala.async.{AsyncFunction, ResultFuture}
import scala.concurrent.Future
import scala.language.{implicitConversions, postfixOps}
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

class IncomingMovementRawService(repository: IncomingMovementRepository) extends AsyncFunction[(Boolean, Boolean, Option[String], RawProcess), Either[Some[RawProcessFailure], Some[RawProcess]]] with StrictLogging {

  override def asyncInvoke(cachedRawProcess: (Boolean, Boolean, Option[String], RawProcess), resultFuture: ResultFuture[Either[Some[RawProcessFailure], Some[RawProcess]]]): Unit = {

    logger.debug("starting incoming movement raw service")

    val eventSuccessByCache = cachedRawProcess._1
    val foundEventInCache = cachedRawProcess._2
    val errorMessageFromCache = cachedRawProcess._3
    val rawProcess = cachedRawProcess._4

    if (foundEventInCache && !eventSuccessByCache) { //se encontrou evento na cache e evento é inválido
      logger.debug("found in cache and invalid event")

      resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = errorMessageFromCache, failuredRaw = rawProcess.payloadParsed, failuredPayload = rawProcess.payload, topicPostingTimestamp = rawProcess.topicPostingTimestamp)))))
    } else if (foundEventInCache && eventSuccessByCache) { //se encontrou evento na cache e evento é válido
      logger.debug("found in cache and valid event")

      resultFuture.complete(Iterable(Right(Some(rawProcess))))
    } else { //se não encontrou evento na cache, então valida pelo banco de dados
      logger.debug("event not found in cache")

      val incomingRaw = rawProcess.payloadParsed.get.asInstanceOf[MovementRaw]

      val reversal = incomingRaw.reversal.get

      val resultOfEventAlreadyExists =
        for {
          //procura pelo evento original
          originalEventDB <- repository.findOneBy(incomingRaw)

          //procura pelo estorno
          reversalEventDB <- repository.findOneReversalBy(incomingRaw)

          res <- originalEventDB match {
            case Some(eventDB: MovementRaw) =>
              //encontrou o evento original

              if (reversal && eventDB.dateReversalOcurred.isEmpty) {
                //pode estornar

                Future {
                  Right(rawProcess)
                }
              } else if (reversal && eventDB.dateReversalOcurred.isDefined) {
                //evento já estornado

                Future {
                  Left("Event already reversed")
                }
              } else {
                //não é estorno, então evento já existe

                Future {
                  Left("Event already exists")
                }
              }

            case _ =>
              //não encontrou o evento original

              reversalEventDB match {
                case Some(_) =>
                  if (reversal) {
                    Future {
                      Left("Reversal waiting for the original event already exists")
                    }
                  } else {
                    //deixa salvar o lançamento original, pois será feita a consistência dele posteriormente com o estorno que o está aguardando
                    Future {
                      Right(rawProcess)
                    }
                  }

                case _ =>
                  /*deixa salvar também o estorno no mongodb, para aguardar pelo evento original
                  se não for estorno, também salva no mongodb o evento original*/

                  Future {
                    Right(rawProcess)
                  }
              }
          }
        } yield res

      resultOfEventAlreadyExists onComplete {
        case Success(maybeEvent) =>
          maybeEvent match {
            case Right(_) =>
              logger.debug("incoming movement raw service result is right")
              resultFuture.complete(Iterable(Right(Some(rawProcess))))

            case Left(e) =>
              logger.debug("incoming movement raw service result is left")
              resultFuture.complete(Iterable(Left(Some(RawProcessFailure(rawProcessError = Some(e), failuredRaw = rawProcess.payloadParsed, failuredPayload = rawProcess.payload, topicPostingTimestamp = rawProcess.topicPostingTimestamp)))))
          }

        case Failure(e) =>
          logger.error(e.getLocalizedMessage)
          resultFuture.completeExceptionally(e)
      }

    }
  }
}
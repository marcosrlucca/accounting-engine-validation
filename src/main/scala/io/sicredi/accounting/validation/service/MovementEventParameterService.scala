package io.sicredi.accounting.validation.service

import io.sicredi.accounting.validation.domain.EventParameter.MovementEventParameter
import io.sicredi.accounting.validation.repository.MovementEventParameterRepository
import scalacache.Cache
import scalacache.modes.sync._
import scala.concurrent.Future
import scala.language.{implicitConversions, postfixOps}

class MovementEventParameterService(movementEventRepository: MovementEventParameterRepository) extends EventParameterService with Serializable {

  private lazy val movementEventParameters: Cache[Future[Seq[MovementEventParameter]]] = Cache.movementEventParameter.cache

  override def getAll: Future[Seq[MovementEventParameter]] = {
    val maybeEventParametersView = movementEventParameters
      .get(MovementEventParametersLabel)
      .getOrElse(movementEventParameters.cachingForMemoize(MovementEventParametersLabel)(Some(globalCacheDuration))(movementEventRepository.findAll()))

    maybeEventParametersView
  }

}

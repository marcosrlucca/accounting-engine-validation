package io.sicredi.accounting.validation.domain

import java.time.ZonedDateTime
import io.sicredi.accounting.validation.helper.DateConversionHelper
import io.sicredi.accounting.validation.repository.EMPTY
import play.api.libs.json._
import reactivemongo.bson.{BSONDateTime, BSONObjectID}
import scala.util.{Failure, Success, Try}

object Raw {

  implicit val writesBSONDateTime: OWrites[BSONDateTime] = Json.writes[BSONDateTime]
  implicit val readsBSONDateTime: Reads[BSONDateTime] = Json.reads[BSONDateTime]

  implicit val writesBSONObjectID: OWrites[BSONObjectID] = Json.writes[BSONObjectID]
  implicit val readsBSONObjectID: Reads[BSONObjectID] = Json.reads[BSONObjectID]

  implicit val writesBalance: OWrites[Balance] = Json.writes[Balance]
  implicit val readsBalance: Reads[Balance] = Json.reads[Balance]

  implicit val writesMovement: OWrites[Movement] = Json.writes[Movement]
  implicit val readsMovement: Reads[Movement] = Json.reads[Movement]

  implicit val formatGenericMovement: Format[GenericMovement] = new Format[GenericMovement] {
    override def reads(json: JsValue): JsResult[GenericMovement] = {
      def from(name: String, data: JsObject): JsResult[GenericMovement] = name match {
        case "Movement" => Json.fromJson[Movement](data)(readsMovement)
        case "Balance" => Json.fromJson[Balance](data)(readsBalance)
        case _ => JsError(s"Unknown class '$name'")
      }

      for {
        name <- (json \ "class").validate[String]
        data <- (json \ "data").validate[JsObject]
        result <- from(name, data)
      } yield result
    }

    override def writes(o: GenericMovement): JsValue = {
      val (prod: Product, sub) = o match {
        case b: Movement => (b, Json.toJson(b)(writesMovement))
        case b: Balance => (b, Json.toJson(b)(writesBalance))
      }
      JsObject(Seq("class" -> JsString(prod.productPrefix), "data" -> sub))
    }
  }

  implicit val writesBalanceRaw: OWrites[BalanceRaw] = Json.writes[BalanceRaw]
  implicit val readsBalanceRaw: Reads[BalanceRaw] = Json.reads[BalanceRaw]

  implicit val writesMovementRaw: OWrites[MovementRaw] = Json.writes[MovementRaw]
  implicit val readsMovementRaw: Reads[MovementRaw] = Json.reads[MovementRaw]

  implicit val formatRaw: Format[Raw] = new Format[Raw] {
    override def reads(json: JsValue): JsResult[Raw] = {
      def from(name: String, data: JsObject): JsResult[Raw] = name match {
        case "MovementRaw" => Json.fromJson[MovementRaw](data)(readsMovementRaw)
        case "BalanceRaw" => Json.fromJson[BalanceRaw](data)(readsBalanceRaw)
        case _ => JsError(s"Unknown class '$name'")
      }

      for {
        name <- (json \ "class").validate[String]
        data <- (json \ "data").validate[JsObject]
        result <- from(name, data)
      } yield result
    }

    override def writes(o: Raw): JsValue = {
      val (prod: Product, sub) = o match {
        case b: MovementRaw => (b, Json.toJson(b)(writesMovementRaw))
        case b: BalanceRaw => (b, Json.toJson(b)(writesBalanceRaw))
      }
      JsObject(Seq("class" -> JsString(prod.productPrefix), "data" -> sub))
    }
  }

  implicit val writesRawProcessFailure: OWrites[RawProcessFailure] = Json.writes[RawProcessFailure]
  implicit val readsRawProcessFailure: Reads[RawProcessFailure] = Json.reads[RawProcessFailure]

  implicit val writesRawProcessOutcome: OWrites[RawProcessOutcome] = Json.writes[RawProcessOutcome]
  implicit val readsRawProcessOutcome: Reads[RawProcessOutcome] = Json.reads[RawProcessOutcome]

  implicit val writesRawProcess: OWrites[RawProcess] = Json.writes[RawProcess]
  implicit val readsRawProcess: Reads[RawProcess] = Json.reads[RawProcess]

  case class Balance(var _id: Option[BSONObjectID],
                     var _unique_index: Option[String],
                     var _engine_error: Option[String] = None,
                     var topicPostingTimestamp: Option[Long],
                     baseDate: Option[BSONDateTime],
                     operationDate: Option[BSONDateTime],
                     document: Option[String],
                     company: Option[String],
                     accountingReferenceCode: Option[String],
                     productReferenceCode: Option[String],
                     transactionId: Option[String],
                     transactionDetail: Option[String],
                     system: Option[String],
                     unit: Option[String],
                     rules: Option[Seq[String]],
                     value: Option[BigDecimal])
    extends GenericMovement

  case class Movement(var _id: Option[BSONObjectID],
                      var _unique_index: Option[String],
                      var _engine_error: Option[String] = None,
                      var topicPostingTimestamp: Option[Long],
                      baseDate: Option[BSONDateTime],
                      originalEventBaseDateForReversal: Option[BSONDateTime],
                      operationDate: Option[BSONDateTime],
                      var dateReversalOcurred: Option[BSONDateTime],
                      document: Option[String],
                      company: Option[String],
                      reversal: Option[Boolean],
                      accountingReferenceCode: Option[String],
                      productReferenceCode: Option[String],
                      transactionId: Option[String],
                      transactionDetail: Option[String],
                      system: Option[String],
                      creditUnit: Option[String],
                      debitUnit: Option[String],
                      rules: Option[Seq[String]],
                      values: Option[Seq[BigDecimal]])
    extends GenericMovement

  trait GenericMovement {
    var _id: Option[BSONObjectID]

    var _unique_index: Option[String]

    var _engine_error: Option[String]

    var topicPostingTimestamp: Option[Long]

    def baseDate: Option[BSONDateTime]

    def operationDate: Option[BSONDateTime]

    def document: Option[String]

    def company: Option[String]

    def accountingReferenceCode: Option[String]

    def productReferenceCode: Option[String]

    def transactionId: Option[String]

    def transactionDetail: Option[String]

    def system: Option[String]

    def rules: Option[Seq[String]]
  }

  case class BalanceRaw(var _id: Option[BSONObjectID],
                        var _unique_index: Option[String],
                        var topicPostingTimestamp: Option[Long],
                        baseDate: Option[String],
                        operationDate: Option[String],
                        var document: Option[String],
                        company: Option[String],
                        accountingReferenceCode: Option[String],
                        productReferenceCode: Option[String],
                        transactionId: Option[String],
                        transactionDetail: Option[String],
                        productDetail: Option[String] = None,
                        system: Option[String],
                        unit: Option[String],
                        rules: Option[Seq[String]],
                        value: Option[BigDecimal])
    extends Raw {
    override def keyToUniqueIndex: String = {
      baseDate.getOrElse("") +
        company.getOrElse("") +
        accountingReferenceCode.getOrElse("") +
        productReferenceCode.getOrElse("") +
        system.getOrElse("") +
        transactionId.getOrElse("") +
        rules.get.toString() +
        value.get.toString()
    }
  }

  case class MovementRaw(var _id: Option[BSONObjectID],
                         var _unique_index: Option[String],
                         var topicPostingTimestamp: Option[Long],
                         baseDate: Option[String],
                         originalEventBaseDateForReversal: Option[String],
                         operationDate: Option[String],
                         document: Option[String],
                         company: Option[String],
                         reversal: Option[Boolean],
                         var dateReversalOcurred: Option[String],
                         accountingReferenceCode: Option[String],
                         productReferenceCode: Option[String],
                         transactionId: Option[String],
                         transactionDetail: Option[String],
                         productDetail: Option[String] = None,
                         system: Option[String],
                         creditUnit: Option[String],
                         debitUnit: Option[String],
                         rules: Option[Seq[String]],
                         values: Option[Seq[BigDecimal]])
    extends Raw {
    override def keyToUniqueIndex: String = {
      //Caso seja estorno, terá apenas a propriedade originalEventBaseDateForReversal
      val maybeDate: Option[ZonedDateTime] = if (reversal.get)
        Try(ZonedDateTime.parse(originalEventBaseDateForReversal.get)) match {
          case Success(bDate) => Some(bDate)
          case Failure(_) => None
        }
      else
        Try(ZonedDateTime.parse(baseDate.get)) match {
          case Success(bDate) => Some(bDate)
          case Failure(_) => None
        }

      val baseDateStr: Option[String] = DateConversionHelper.parseToDateString(maybeDate)

      baseDateStr +
        document.getOrElse(EMPTY) +
        company.getOrElse(EMPTY) +
        accountingReferenceCode.getOrElse(EMPTY) +
        productReferenceCode.getOrElse(EMPTY) +
        system.getOrElse(EMPTY) +
        transactionId.getOrElse(EMPTY) +
        transactionDetail.getOrElse(EMPTY) +
        creditUnit.getOrElse(EMPTY) +
        debitUnit.getOrElse(EMPTY) +
        rules.getOrElse(EMPTY) +
        values.getOrElse(EMPTY)

    }
  }

  trait Raw {
    var _id: Option[BSONObjectID]

    var _unique_index: Option[String]

    var topicPostingTimestamp: Option[Long]

    def baseDate: Option[String]

    def operationDate: Option[String]

    def document: Option[String]

    def company: Option[String]

    def accountingReferenceCode: Option[String]

    def productReferenceCode: Option[String]

    def transactionId: Option[String]

    def transactionDetail: Option[String]

    def productDetail: Option[String]

    def system: Option[String]

    def rules: Option[Seq[String]]

    //attributes that will compose _unique_index
    def keyToUniqueIndex: String
  }

  case class RawProcessOutcome(system: Option[String],
                               transactionId: Option[String],
                               transactionDetail: Option[String],
                               topicPostingTimestamp: Option[String],
                               validated: Boolean = false,
                               errorDescription: Option[String] = None,
                               payload: Option[String])

  case class RawProcessFailure(rawProcessError: Option[String],
                               var uniqueEventIdDB: Option[String] = None,
                               var collectionName: Option[String] = None,
                               var topicPostingTimestamp: Option[Long],
                               failuredRaw: Option[Raw] = None,
                               failuredPayload: Option[String],
                               applicationName: String = APPLICATION_NAME) {
    def keyToUniqueIndex: String = {
      rawProcessError.getOrElse("") +
        uniqueEventIdDB.getOrElse("") +
        topicPostingTimestamp.getOrElse("") +
        failuredRaw.getOrElse("") +
        failuredPayload.getOrElse("")
    }
  }

  case class RawProcess(payloadParsed: Option[Raw],
                        var uniqueEventIdDB: Option[String] = None,
                        var collectionName: Option[String] = None,
                        var topicPostingTimestamp: Option[Long],
                        payload: Option[String])

  case class Event(message: String, var timestamp: Long)

}

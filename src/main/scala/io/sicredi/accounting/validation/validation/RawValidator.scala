package io.sicredi.accounting.validation.validation

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.EventParameter.EventParameter
import io.sicredi.accounting.validation.domain.Raw.{Raw, RawProcess}
import io.sicredi.accounting.validation.helper.ValidationHelper
import scala.collection.mutable

abstract class RawValidator extends StrictLogging {

  protected val errors: Some[mutable.Map[String, String]] = Some(scala.collection.mutable.Map[String, String]())

  def isScriptsPresent(resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter]

  def isValueWithTwoDecimalPlaces(incomingEvent: RawProcess, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter]

  def isScriptsPresent(eventPar: EventParameter): Either[String, EventParameter]

  def isOptionsValid(raw: Raw, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    if (!ValidationHelper.isRulesFromIncomingEventValid(raw, resultValidationEventPar.right.get)) {
      errors.foreach(e => e(OPTION_NOT_VALID) = OPTION_NOT_VALID_TEXT)
      Left(OPTION_NOT_VALID_TEXT)
    } else {
      resultValidationEventPar
    }
  }

  def isOptionsPresent(resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    val eventPar = resultValidationEventPar.right.get

    val validated = eventPar.rules.get.exists(rule => rule.options.isDefined && rule.options.get.nonEmpty && rule.options.get.exists(opt => opt.option.isDefined))

    if (!validated) {
      errors.foreach(e => e(OPTION_NOT_EXISTS) = OPTION_NOT_EXISTS_TEXT)
      Left(OPTION_NOT_EXISTS_TEXT)
    } else {
      Right(eventPar)
    }
  }

  def isDateValid(incomingEvent: RawProcess, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    val eventPar = resultValidationEventPar.right.get

    val validated = ValidationHelper.isDateEventParameterValid(incomingEvent.payloadParsed.get.baseDate.get, eventPar) && ValidationHelper.isDateSystemParameterValid(eventPar, incomingEvent.payloadParsed.get)

    if (!validated) {
      errors.foreach(e => e(DATE_NOT_VALID) = DATE_NOT_VALID_TEXT)
      Left(DATE_NOT_VALID_TEXT)
    } else {
      Right(eventPar)
    }
  }

  def isRulesDefined(resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    val eventPar = resultValidationEventPar.right.get

    val validated = eventPar.rules.isDefined

    if (!validated) {
      errors.foreach(e => e(RULE_NOT_DEFINED) = RULE_NOT_DEFINED_TEXT)
      Left(RULE_NOT_DEFINED_TEXT)
    } else {
      Right(eventPar)
    }
  }

  def isSameSystem(incomingEvent: RawProcess, resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    val eventPar = resultValidationEventPar.right.get

    val validated =
      eventPar.system.isDefined &&
        eventPar.system.exists(s => s.name.isDefined && s.name.get.equalsIgnoreCase(incomingEvent.payloadParsed.get.system.get))

    if (!validated) {
      errors.foreach(e => e(SYSTEM_INVALID) = SYSTEM_INVALID_TEXT)
      Left(SYSTEM_INVALID_TEXT)
    } else {
      Right(eventPar)
    }
  }

  def isSameEventCodeOrProductCode(incomingEvent: RawProcess, eventPar: EventParameter): Either[String, EventParameter] = {
    cleanErrors

    val payload = incomingEvent.payloadParsed.get

    val validated =
      payload
        .accountingReferenceCode.map(incomingCode => eventPar.code.exists(parameterCode => incomingCode.equalsIgnoreCase(parameterCode)))
        .getOrElse(payload.productReferenceCode.exists(incomingCode => eventPar.productCode.exists(parameterCode => incomingCode.equalsIgnoreCase(parameterCode))))

    if (!validated) {
      errors.foreach(e => e(EVENT_CODE_INVALID) = EVENT_CODE_INVALID_TEXT)
      Left(EVENT_CODE_INVALID_TEXT)
    } else {
      Right(eventPar)
    }
  }

  def isTwoDecimalPlaces(v: BigDecimal): Boolean = {
    val valueParsedToString = v.toString
    val splitter = valueParsedToString.split("\\.")

    !(splitter.length == 2 && splitter(1).length <= 2)
  }

  def isSameSizeRules(rulesFromIncomingEvent: Seq[String], resultValidationEventPar: Either[String, EventParameter]): Either[String, EventParameter] = {
    cleanErrors

    val eventPar = resultValidationEventPar.right.get

    val validated = ValidationHelper.isSameSizeRules(rulesFromIncomingEvent, eventPar)

    if (!validated) {
      errors.foreach(e => e(RULE_DIFFERENT_SIZE) = RULE_DIFFERENT_SIZE_TEXT)
      Left(RULE_DIFFERENT_SIZE_TEXT)
    } else {
      Right(eventPar)
    }
  }

  protected def cleanErrors = errors.foreach(e => e.clear())

}

package io.sicredi.accounting.validation.repository

import io.sicredi.accounting.validation.configuration.Mongo
import io.sicredi.accounting.validation.domain.EventParameter.BalanceEventParameter
import reactivemongo.api.Cursor
import reactivemongo.bson.document
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class BalanceEventParameterRepository(mongoConfiguration: Mongo) extends EventParameterRepository with Serializable {

  override def findAll(): Future[Seq[BalanceEventParameter]] = {

    val query = document()

    Try(mongoConfiguration.getBalanceEventParameterCollection.flatMap(_.find(query)
      .cursor[BalanceEventParameter]()
      .collect[Seq](-1, Cursor.FailOnError[Seq[BalanceEventParameter]]()))) match {
      case Success(rs) => rs
      case Failure(_) => Future(Seq.empty)
    }

  }

}

package io.sicredi.accounting.validation.processor

object MessagesBucket {

  def payloadLessRules: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"reversal": true,
        |"accountingReferenceCode": "20-10-001",
        |"transactionDetail": "D791097",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadLessSystem: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"reversal": true,
        |"eventCode": "20-10-001",
        |"originId": "D791097",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadLessTransactionId: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"reversal": true,
        |"eventCode": "20-10-001",
        |"originId": "D791097",
        |"system": "RISCO",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadLessOriginId: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"reversal": true,
        |"eventCode": "20-10-001",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadLessEventId: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"reversal": true,
        |"eventCode": "20-10-001",
        |"originId": "D791097",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadLessEventCode: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"reversal": true,
        |"originId": "D791097",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadLessCompany: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"reversal": true,
        |"eventCode": "20-10-001",
        |"originId": "D791097",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadLessBaseDate: String =
    s"""|{
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"reversal": true,
        |"eventCode": "20-10-001",
        |"originId": "D791097",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadMovementOk: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"reversal": false,
        |"eventCode": "20-10-001",
        |"originId": "D791097",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadMovementError: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"reversal": false,
        |"eventCode": "20-10-001",
        |"originId": D791097,
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"creditUnit": "0116",
        |"debitUnit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"values": [
        |100
        |]
        |}""".stripMargin

  def payloadBalanceOk: String =
    s"""|{
        |"baseDate": "2018-11-30 00:00:00+00:00",
        |"operationDate": "2019-02-08T12:14:01.540185+00:00",
        |"document": "53125746000",
        |"company": "0106",
        |"eventCode": "20-10-001",
        |"originId": "D791097",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"unit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"value": 100
        |}""".stripMargin

  def payloadBalanceError: String =
    s"""|{
        |"baseDate": "error",
        |"operationDate": "null",
        |"document": "",
        |"company": 0106,
        |"eventCode": "20-10-001",
        |"originId": "D791097",
        |"transactionId": "5c48caf00154540b46b839b1",
        |"system": "RISCO",
        |"unit": "0116",
        |"rules": [
        |"999",
        |"1304",
        |"LIMITE_DISPONIVEL_CH",
        |"D",
        |"N"
        |],
        |"value": 100
        |}""".stripMargin

}

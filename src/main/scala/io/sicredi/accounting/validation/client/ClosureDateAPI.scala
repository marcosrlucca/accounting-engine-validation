package io.sicredi.accounting.validation.client

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.domain.ClosureDateParameter._
import cats.effect.{ContextShift, IO}
import org.http4s.{MediaType, Status, Uri}
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.dsl.io.GET
import org.http4s.client.dsl.io._
import org.http4s.headers.Accept
import play.api.libs.json.Json
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.{implicitConversions, postfixOps}
import scala.concurrent.Future

case class ClosureDateProperties(url: String)

case class ClosureDateAPI(properties: ClosureDateProperties) extends Serializable with StrictLogging {

  private val connectTimeOut = 60000
  private val readTimeOut = 60000
  private val method = "GET"

  @throws(classOf[java.io.IOException])
  @throws(classOf[java.net.SocketTimeoutException])
  def getAllClosureDate: Future[Either[String, Seq[ClosureDate]]] = {
    callExternalAPI(Uri.fromString(properties.url).right.get).map {
      case Right(json) => Right(Json.parse(json).as[Array[ClosureDate]].toSeq)

      case Left(error) => Left(error)
    }
  }

  private def callExternalAPI(uri: Uri): Future[Either[String, String]] = {
    import scala.concurrent.ExecutionContext.global
    implicit val cs: ContextShift[IO] = IO.contextShift(global)

    val request = GET(uri, Accept(MediaType.application.json))

    BlazeClientBuilder[IO](global).resource.use { client =>
      client.fetch(request) {
        case Status.Successful(r) => r.attemptAs[String].leftMap(_.message).value
        case r => r.as[String]
          .map(b => Left(s"Request $request failed with status ${r.status.code} and body $b"))
      }
    }.unsafeToFuture()
  }

}

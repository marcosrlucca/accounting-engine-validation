package io.sicredi.accounting.validation.helper

import java.time.ZonedDateTime

import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers, PrivateMethodTester}
import reactivemongo.bson.BSONDateTime

class DateConversionHelperSpec extends FlatSpec with Matchers with MockFactory with PrivateMethodTester {

  "The parseToZonedDateTime method" should "return a ZonedDateTime" in {
    val zdt = ZonedDateTime.now
    val millisFromZdt = zdt.toInstant.toEpochMilli

    val bsonDateTime = Some(BSONDateTime(millisFromZdt))

    val result = DateConversionHelper.parseToZonedDateTime(bsonDateTime)

    assert(result.isDefined)
    assert(result.exists(r => r.toInstant.toEpochMilli == millisFromZdt))
  }

  "The parseToBSONDateTime" should "return a BSONDateTime" in {
    val zdt = ZonedDateTime.now
    val millisFromZdt = zdt.toInstant.toEpochMilli
    val time = Some(zdt)

    val result = DateConversionHelper.parseToBSONDateTime(time)

    assert(result.isDefined)
    assert(result.exists(r => r.value == millisFromZdt))
  }

  "The parseToDateString" should "return a Option[String] formmated 'yyyy-MM-dd'" in {
    val zdt = ZonedDateTime.now
    val millisFromZdt = zdt.toInstant.toEpochMilli
    val time = Some(zdt)

    val result = DateConversionHelper.parseToDateString(time)

    assert(result.isDefined)
    assert(result.exists(r => r.length == 10))
  }

  "The parseToDateString" should "return a Option.empty" in {
    var time: Option[ZonedDateTime] = Option.empty
    val result = DateConversionHelper.parseToDateString(time)

    time = null
    val resultTestNull = DateConversionHelper.parseToDateString(time)

    assert(result.isEmpty)
    assert(resultTestNull.isEmpty)
  }
}

package io.sicredi.accounting.validation

package object helper {

  val ZERO = "0"

  val COMPANY_DESCRIPTION = "EMPRESA"
  val ANY_CODE = "*"
  val BANK_748 = "748"
  val BANK_0748 = "0748"
  val ANY_AND_ONLY_COOPERATIVE = "999"

}

package io.sicredi.accounting.validation.domain

import play.api.libs.json.{Json, Reads, Writes}

object ClosureDateParameter {

  implicit val readsClosureDate: Reads[ClosureDate] = Json.reads[ClosureDate]
  implicit val writesClosureDate: Writes[ClosureDate] = Json.writes[ClosureDate]

  case class ClosureDate(company: Option[String], period: Option[String]) extends Ordered[ClosureDate] {

    override def compare(that: ClosureDate): Int = {
      if (that.period.isEmpty || this.period.isEmpty) -1
      else if (that.period.get == this.period.get) 0
      else if (that.period.get > this.period.get) 1
      else -1
    }

    def getMonthValue: Option[Int] = {
      period
        .map(p => p.substring(p.length - 2, p.length)) //takes the last two elements representing the month
        .map(_.toInt)
    }

  }

}

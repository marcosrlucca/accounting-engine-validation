package io.sicredi.accounting.validation.repository

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.configuration.Mongo
import io.sicredi.accounting.validation.domain.ClosureDateParameter.ClosureDate
import reactivemongo.api.Cursor
import reactivemongo.bson.document
import reactivemongo.api.commands.{MultiBulkWriteResult, WriteResult}
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

class ClosureDateRepository(mongoConfiguration: Mongo) extends StrictLogging with Serializable {

  def findAll(): Future[Seq[ClosureDate]] = {
    val query = document()

    Try(mongoConfiguration.getClosureDateCollection
      .flatMap(_.find(query)
        .cursor[ClosureDate]()
        .collect[Seq](-1, Cursor.FailOnError[Seq[ClosureDate]]()))) match {
      case Success(rs) => rs
      case Failure(_) => Future(Seq.empty)
    }
  }

  def deleteAll(): Future[WriteResult] = {
    logger.debug("deleting all rows from closure_date collection")

    val query = document()

    mongoConfiguration.getClosureDateCollection
      .flatMap(_.remove(query))
  }

  def insert(closureDate: Seq[ClosureDate]): Future[MultiBulkWriteResult] = {
    logger.debug("starting insert closure date")

    mongoConfiguration.getClosureDateCollection
      .flatMap(_.insert[ClosureDate](ordered = false)
        .many(closureDate))
  }

}

package io.sicredi.accounting.validation.domain

import io.sicredi.accounting.validation.domain.Raw.{MovementRaw, Raw}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers, PrivateMethodTester}

class RawSpec extends FlatSpec with Matchers with MockFactory with PrivateMethodTester {
  "The MovementRaw.keyToUniqueIndex with operationDate" should "return a String to generate _unique_index" in {
    val raw: Raw = MovementRaw(
      baseDate = Some("2019-07-25T09:50:58.000-03:00"),
      operationDate = Some("2019-07-26T09:50:58.000-03:00"),
      document = Some("0"),
      company = Some("0101"),
      reversal = Some(false),
      accountingReferenceCode = Some("10-10-002"),
      transactionId = Some("72386414786128"),
      transactionDetail = Some("72386414786128"),
      system = Some("CAIXA"),
      creditUnit = Some("0009"),
      debitUnit = Some("0009"),
      rules = Option(
        Seq("999", "9")
      ),
      values = Option(
        Seq(
          6710.89
        )
      ),
      _id = Option.empty,
      _unique_index = Option.empty,
      topicPostingTimestamp = Option.empty,
      originalEventBaseDateForReversal = Option.empty,
      dateReversalOcurred = Option.empty,
      productReferenceCode = Option.empty
    )

    val _unique_index = raw.keyToUniqueIndex
    val result: String = "Some(2019-07-25)0010110-10-002CAIXA723864147861287238641478612800090009List(999, 9)List(6710.89)"

    assert(result.equals(_unique_index))
  }

  //Movimento de ESTORNO
  "The MovementRaw.keyToUniqueIndex with operationDate and reversal" should "return a String to generate _unique_index" in {
    val raw: Raw = MovementRaw(
      originalEventBaseDateForReversal = Some("2019-07-25T09:50:58.000-03:00"),
      dateReversalOcurred = Some("2019-07-27T09:50:58.000-03:00"),
      operationDate = Some("2019-07-26T09:50:58.000-03:00"),
      document = Some("0"),
      company = Some("0101"),
      reversal = Some(true),
      accountingReferenceCode = Some("10-10-002"),
      transactionId = Some("72386414786128"),
      transactionDetail = Some("72386414786128"),
      system = Some("CAIXA"),
      creditUnit = Some("0009"),
      debitUnit = Some("0009"),
      rules = Option(
        Seq("999", "9")
      ),
      values = Option(
        Seq(
          6710.89
        )
      ),
      baseDate = Option.empty,
      _id = Option.empty,
      _unique_index = Option.empty,
      topicPostingTimestamp = Option.empty,
      productReferenceCode = Option.empty
    )

    val _unique_index = raw.keyToUniqueIndex
    val result: String = "Some(2019-07-25)0010110-10-002CAIXA723864147861287238641478612800090009List(999, 9)List(6710.89)"

    assert(result.equals(_unique_index))
  }
}

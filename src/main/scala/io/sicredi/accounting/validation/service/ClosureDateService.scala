package io.sicredi.accounting.validation.service

import com.typesafe.scalalogging.StrictLogging
import io.sicredi.accounting.validation.client.ClosureDateAPI
import io.sicredi.accounting.validation.domain.ClosureDateParameter.ClosureDate
import io.sicredi.accounting.validation.repository.ClosureDateRepository
import reactivemongo.api.commands.MultiBulkWriteResult
import scalacache.{Cache, Id}
import scalacache.modes.sync._
import scala.concurrent.Future
import scala.language.{implicitConversions, postfixOps}
import scala.concurrent.ExecutionContext.Implicits.global

class ClosureDateService(repository: ClosureDateRepository, closureDateAPI: ClosureDateAPI) extends Serializable with StrictLogging {

  private lazy val cache: Cache[Future[Seq[ClosureDate]]] = Cache.closureDate.cache

  def getAllClosureDate: Future[Seq[ClosureDate]] = {
    val result = cache
      .get(ClosureDateLabel)
      .getOrElse(
        cache.cachingForMemoize(ClosureDateLabel)
        (Some(globalCacheDuration))
        (repository.findAll()))

    result
  }

  def getLastClosureDateByCompany(companyFromRaw: String): Future[Option[ClosureDate]] = {
    getAllClosureDate
      .map(allClosureDate => {
        val allClosureDateByCompany: Seq[ClosureDate] = allClosureDate
          .filter(closureDate => closureDate.company
            .exists(company => company.equalsIgnoreCase(companyFromRaw)))

        allClosureDateByCompany.sorted.headOption
      })
  }

  def clearCache(): Id[Any] = {
    if (cache != null) cache.removeAll()
  }

  def insertAll(closureDates: Seq[ClosureDate]): Future[MultiBulkWriteResult] = {
    for {
      deleted <- repository.deleteAll()
      result <- repository.insert(closureDates)
    } yield result
  }

}
